package com.zafin.rpe.test.tools;

import com.zafin.rpe.test.tools.generator.CitibanamexConfig;
import com.zafin.rpe.test.tools.generator.CitibanamexDataGenerationConfig;
import com.zafin.rpe.test.tools.generator.PtDataGenerationConfig;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.zafin.rpe.test.tools.generator.CitibanamexConfigSmall;
import com.zafin.rpe.test.tools.generator.PerformanceDataGenerator;
import com.zafin.rpe.test.tools.generator.CIBCJointConfig;
import com.zafin.rpe.test.tools.generator.CIBCPerformanceDataGenerationConfig;
import com.zafin.rpe.test.tools.generator.SmartDataGenerator;
import com.zafin.rpe.test.tools.generator.SnowflakeDataGenerationConfig;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {
	
	@Override
	public void run(String... args) throws Exception {
		//To invoke the test data exporter
		/*if (args.length > 0) {
			String testDataFile = args[0];
			E2ETestDataExporter e2eTestDataExporter = new E2ETestDataExporter();
			e2eTestDataExporter.exportData(testDataFile);
		}*/
		
		//To generate Citibanamex performance data
		/*if (args.length > 1) {
			String folderPath = args[1];
			SmartDataGenerator dataGenerator = new SmartDataGenerator();
			dataGenerator.generateData(CitibanamexConfigSmall.getDataGenerationSpecification(), folderPath);
		}*/
		
		//To generate CIBC performance data
		if (args.length > 0) {
			String folderPath = args[0];
			PerformanceDataGenerator dataGenerator = new PerformanceDataGenerator();
			dataGenerator.generateData(PtDataGenerationConfig.getDataGenerationSpecification(), folderPath);
		}
		
	}
	
}
