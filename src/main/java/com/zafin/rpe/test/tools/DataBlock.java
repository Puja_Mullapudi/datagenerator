package com.zafin.rpe.test.tools;

public class DataBlock {

	private String dataBlockName;
	private int startRowNum; //Inclusive
	private int endRowNum; //Inclusive
	
	public String getDataBlockName() {
		return dataBlockName;
	}
	public void setDataBlockName(String dataBlockName) {
		this.dataBlockName = dataBlockName;
	}
	public int getStartRowNum() {
		return startRowNum;
	}
	public void setStartRowNum(int startRowNum) {
		this.startRowNum = startRowNum;
	}
	public int getEndRowNum() {
		return endRowNum;
	}
	public void setEndRowNum(int endRowNum) {
		this.endRowNum = endRowNum;
	}
	
	
}
