package com.zafin.rpe.test.tools;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class E2ETestDataExporter {

	private static final String TRIGGER_FILE_EXTENSION = ".TRG";
	private static final String START_DAY_END = "START_DAY_END";
	private static final String DAT_FILE_EXTENSION = ".DAT";
	private static final String UNDERSCORE = "_";
	private static final String DELIMITER = "|";
	private static final String END = "END";
	private static final String QUOTE = "'";
	private static final String METADATA = "Metadata";
	private static final String RUN_ORDER = "RunOrder";
	private static final Logger logger = LoggerFactory.getLogger(CommandLineAppStartupRunner.class);
	private static final String NEW_LINE = System.lineSeparator();
	private static final String THREE_DOTS = "...";
	
	public void exportData(String testDataFile) {
		try {
			File file = new File(testDataFile);
			if(!file.exists()) {
				throw new TestingToolsRuntimeException("File " + testDataFile + " does not exist. Please ensure that the right path is provided");
			}
			Workbook workbook = WorkbookFactory.create(file);
			if(workbook != null) {
				List<FileMetadata> fileMetadataList = readMetadataSheet(workbook);
				List<String> dayEndDateList = readRunOrderSheet(workbook);
				String dataFolderPath = setupDataFolder(file);
				generateFiles(dataFolderPath, dayEndDateList, fileMetadataList, workbook);
			}
		} catch (Exception e) {
			System.out.println("*****************************************EXCEPTION*****************************************");
			logger.error(e.getMessage());
			System.out.println("*****************************************EXCEPTION*****************************************");
			e.printStackTrace();
		}
	}
	
	private String setupDataFolder(File file) {
		File dataFolder = null;
		try {
			File parentFile = file.getParentFile();
			if(parentFile == null) { //If full path is not fount, assume current directory
				parentFile = new File(".");
			}
			if(parentFile.canWrite()) {
				//Delete existing data folder
				String pathToDataFolder = parentFile.getAbsolutePath() + "/" + "data";
				dataFolder = new File(pathToDataFolder);
				if(dataFolder.exists()) {
					logger.info("Data folder '"+ pathToDataFolder + "' exists, deleting");
					FileUtils.deleteDirectory(dataFolder);
					logger.info("Data folder deleted successfully");
					//Creating an empty data folder
					dataFolder.mkdir();
					logger.info("Created new data folder successfully");
				} else {
					logger.info("Data folder '"+ pathToDataFolder + "' does not exists, creating one");
					dataFolder.mkdir();
					logger.info("Data folder created successfully");
				}
			} else {
				throw new TestingToolsRuntimeException("Cannot write to directory: " + QUOTE + parentFile.getAbsolutePath() + QUOTE);
			}
		} catch (Exception e) {
			throw new TestingToolsRuntimeException(e);
		}
		return dataFolder.getAbsolutePath();
	}


	private void generateFiles(String dataFolderPath, List<String> dayEndDateList, List<FileMetadata> fileMetadataList, Workbook workbook) {
		for (Iterator<String> iterator = dayEndDateList.iterator(); iterator.hasNext();) {
			String dayEndDate = (String) iterator.next();
			File dayEndDateFolder = new File(dataFolderPath + "/" + dayEndDate);
			dayEndDateFolder.mkdir();
			logger.info("Created folder '" + dayEndDateFolder.getAbsolutePath() + "' successfully");
			
			logger.info("Generating files for day: '" + dayEndDate + QUOTE + THREE_DOTS);
			Sheet sheet = workbook.getSheet(dayEndDate);
			if(sheet == null) {
				throw new TestingToolsRuntimeException("Could not find sheet for day '" + dayEndDate + QUOTE);
			}
			generateFilesForDay(dayEndDateFolder.getAbsolutePath(), dayEndDate, fileMetadataList, sheet);
		}
	}
	
	
	private void generateFilesForDay(String dayEndDateFolderPath, String dayEndDate, List<FileMetadata> fileMetadataList, Sheet sheet) {
		File inputsFolder = new File(dayEndDateFolderPath + "/inputs");
		inputsFolder.mkdir();
		logger.info("Created folder '" + inputsFolder.getAbsolutePath() + "' successfully");
		
		File outputsFolder = new File(dayEndDateFolderPath + "/outputs");
		outputsFolder.mkdir();
		logger.info("Created folder '" + outputsFolder.getAbsolutePath() + "' successfully");
		
		File resultsFolder = new File(dayEndDateFolderPath + "/results");
		resultsFolder.mkdir();
		logger.info("Created folder '" + resultsFolder.getAbsolutePath() + "' successfully");
		
		File verifyFolder = new File(dayEndDateFolderPath + "/verify");
		verifyFolder.mkdir();
		logger.info("Created folder '" + verifyFolder.getAbsolutePath() + "' successfully");
		
		//Generate Input files
		generateInputFiles(inputsFolder.getAbsolutePath(), dayEndDate, fileMetadataList, sheet);
		
		//Generate Verify files
		generateVerifyFiles(verifyFolder.getAbsolutePath(), dayEndDate, fileMetadataList, sheet);
	}
	
	private void generateVerifyFiles(String verifyFolderPath, String dayEndDate, List<FileMetadata> fileMetadataList,
			Sheet sheet) {
		for (Iterator<FileMetadata> iterator = fileMetadataList.iterator(); iterator.hasNext();) {
			FileMetadata fileMetadata = (FileMetadata) iterator.next();
			if(fileMetadata.getFileType() == FileMetadataFileType.OUTPUT) {
				generateVerifyFiles(verifyFolderPath, dayEndDate, fileMetadata, sheet);
			}
		}
	}

	private void generateVerifyFiles(String verifyFolderPath, String dayEndDate, FileMetadata fileMetadata,
			Sheet sheet) {
		try {
			DataBlock dataBlock = findDataBlock(fileMetadata.getDataBlockName(),sheet);
			Map<String, List<Row>> testCases = getTestCaseIds(dataBlock, sheet);
			Set<String> testCaseIds = testCases.keySet();
			for (String testCaseId : testCaseIds) {
				String fileName = verifyFolderPath + "/" + testCaseId + "." + fileMetadata.getFilePrefix() + UNDERSCORE + dayEndDate + DAT_FILE_EXTENSION;
				File inputFile = new File(fileName);
				inputFile.createNewFile();
				List<Row> rowsMatchingTestCase = testCases.get(testCaseId);
				String contentForFile = getContentFromRows(rowsMatchingTestCase, fileMetadata.getColumnCount()+1, sheet);
				Files.write(Paths.get(inputFile.getAbsolutePath()), contentForFile.getBytes());
				logger.info("Generated file: " + QUOTE + fileName + QUOTE);
			}
		} catch (Exception e) {
			throw new TestingToolsRuntimeException(e);
		}
	}
	
	private String getContentFromRows(List<Row> rowsMatchingTestCase, int columnCount, Sheet sheet) {
		String content = "";
		int i=0;
		for (Row row : rowsMatchingTestCase) {
			for (int j = 1; j < columnCount; j++) {
				content += row.getCell(j).getStringCellValue();
				if(j < columnCount - 1) {
					content += DELIMITER;
				}
			}
			if(i < rowsMatchingTestCase.size() - 1) {
				content += NEW_LINE;
			}
			i++;
		}
		return content;
	}
	
	private Map<String, List<Row>> getTestCaseIds(DataBlock dataBlock, Sheet sheet) {
		Map<String, List<Row>> testCaseIds = new HashMap<String, List<Row>>();
		for (int i = dataBlock.getStartRowNum(); i < dataBlock.getEndRowNum(); i++) {
			Row row = sheet.getRow(i);
			if(row != null && row.getCell(0) != null) {
				String testCaseId = row.getCell(0).getStringCellValue();
				if(!testCaseIds.keySet().contains(testCaseId)) {
					List<Row> rows = new ArrayList<Row>();
					rows.add(row);
					testCaseIds.put(testCaseId, rows);
				} else {
					List<Row> rows = testCaseIds.get(testCaseId);
					rows.add(row);
				}
			}
		}
		return testCaseIds;
	}

	private void generateInputFiles(String inputsFolderPath, String dayEndDate, List<FileMetadata> fileMetadataList, Sheet sheet) {
		for (Iterator<FileMetadata> iterator = fileMetadataList.iterator(); iterator.hasNext();) {
			FileMetadata fileMetadata = (FileMetadata) iterator.next();
			if(fileMetadata.getFileType() == FileMetadataFileType.INPUT) {
				generateInputFile(inputsFolderPath, dayEndDate, fileMetadata, sheet);
			}
		}
		//Generate the trigger file
		String triggerFileName = inputsFolderPath + "/" + START_DAY_END + UNDERSCORE + dayEndDate + TRIGGER_FILE_EXTENSION;
		try {
			new File(triggerFileName).createNewFile();
		} catch (IOException e) {
			throw new TestingToolsRuntimeException(e);
		}
	}
	
	private void generateInputFile(String inputsFolderPath, String dayEndDate, FileMetadata fileMetadata, Sheet sheet) {
		try {
			String fileName = inputsFolderPath + "/" + fileMetadata.getFilePrefix() + UNDERSCORE + dayEndDate + DAT_FILE_EXTENSION;
			File inputFile = new File(fileName);
			inputFile.createNewFile();
			String contentForFile = getContentForFile(fileMetadata.getDataBlockName(), dayEndDate, fileMetadata.getColumnCount(), sheet);
			Files.write(Paths.get(inputFile.getAbsolutePath()), contentForFile.getBytes());
			logger.info("Generated file: " + QUOTE + fileName + QUOTE);
		} catch (Exception e) {
			throw new TestingToolsRuntimeException(e);
		}
	}
	
	private String getContentForFile(String dataBlockName, String dayEndDate, int columnCount, Sheet sheet) {
		DataBlock dataBlock = findDataBlock(dataBlockName,sheet);
		int totalRowCount = dataBlock.getEndRowNum() - dataBlock.getStartRowNum();
		String content = "#|" + dayEndDate + "|" + totalRowCount + "\n";
		for (int i = dataBlock.getStartRowNum(); i < dataBlock.getEndRowNum(); i++) {
			Row row = sheet.getRow(i);
			for (int j = 0; j < columnCount; j++) {
				content += row.getCell(j).getStringCellValue();
				if(j < columnCount - 1) {
					content += DELIMITER;
				}
			}
			if(i < dataBlock.getEndRowNum() - 1) {
				content += NEW_LINE;
			}
		}
		return content;
	}
	
	private DataBlock findDataBlock(String dataBlockName, Sheet sheet) {
		int lastRowNum = sheet.getLastRowNum();
		DataBlock dataBlock = new DataBlock();
		dataBlock.setDataBlockName(dataBlockName);
		int startRowNum = getRowNumberForRowWithDataBlockName(dataBlockName,sheet);
		dataBlock.setStartRowNum(startRowNum + 2);
		for (int i = startRowNum; i < lastRowNum+1; i++) {
			Row row = sheet.getRow(i);
			if(row != null && row.getCell(1) != null &&
					row.getCell(1).getStringCellValue().equals(END)) {
				dataBlock.setEndRowNum(row.getRowNum());
				break;
			}
		}
		return dataBlock;
	}
	
	private int getRowNumberForRowWithDataBlockName(String dataBlockName, Sheet sheet) {
		int lastRowNum = sheet.getLastRowNum();
		for (int i = 0; i < lastRowNum+1; i++) {
			Row row = sheet.getRow(i);
			if(row != null && row.getCell(0) != null && row.getCell(0).getStringCellValue().equals(dataBlockName)) {
				return row.getRowNum();
			}
		}
		throw new TestingToolsRuntimeException("Could not find data block for " + dataBlockName + " within sheet " + sheet.getSheetName());
	}
	
	private List<FileMetadata> readMetadataSheet(Workbook workbook) throws Exception {
		List<FileMetadata> fileMetadataList = new ArrayList<FileMetadata>();
		logger.info("Reading Metadata Sheet: " + THREE_DOTS);
		Sheet sheet = workbook.getSheet(METADATA);
		if(sheet == null) {
			throw new TestingToolsRuntimeException("Could not find sheet for name '" + METADATA + QUOTE);
		}
		int lastRowNum = sheet.getLastRowNum();
		for (int i = 1; i < lastRowNum+1; i++) {
			Row row = sheet.getRow(i);
			FileMetadata fileMetadata = new FileMetadata();
			fileMetadata.setDataBlockName(row.getCell(0).getStringCellValue());
			fileMetadata.setFilePrefix(row.getCell(1).getStringCellValue());
			fileMetadata.setColumnCount(ExcelUtility.getCellDataAsInt(row,2));
			fileMetadata.setFileType(FileMetadataFileType.valueOf(row.getCell(3).getStringCellValue()));
			fileMetadataList.add(fileMetadata);
			if(logger.isDebugEnabled()) {
				logMetadata(fileMetadata);
			}
		}
		logger.info("Reading Metadata Sheet Successful");
		return fileMetadataList;
	}
	
	private List<String> readRunOrderSheet(Workbook workbook) {
		List<String> dayEndDateList = new ArrayList<String>();
		logger.info("Reading RunOrder Sheet: " + THREE_DOTS);
		Sheet sheet = workbook.getSheet(RUN_ORDER);
		if(sheet == null) {
			throw new TestingToolsRuntimeException("Could not find sheet for name '" + RUN_ORDER + QUOTE);
		}
		if(sheet != null) {
			int lastRowNum = sheet.getLastRowNum();
			for (int i = 0; i < lastRowNum+1; i++) {
				Row row = sheet.getRow(i);
				String dayEndDate = row.getCell(0).getStringCellValue();
				dayEndDateList.add(dayEndDate);
				logger.info("Day End: {}", dayEndDate);
			}
		}
		return dayEndDateList;
	}
	
	private void logMetadata(FileMetadata fileMetadata) {
		logger.info("FileMetadata {}, {}, {}, {}", fileMetadata.getDataBlockName(), fileMetadata.getFilePrefix(), fileMetadata.getColumnCount(), fileMetadata.getFileType());
	}
	
}
