package com.zafin.rpe.test.tools;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

public class ExcelUtility {

	public static int getCellDataAsInt(Row row, int cellIndex) throws Exception {
		CellType cellType = row.getCell(cellIndex).getCellTypeEnum();
		if(cellType == CellType.NUMERIC) {
			return Double.valueOf(row.getCell(cellIndex).getNumericCellValue()).intValue();
		} else if(cellType == CellType.STRING) {
			return Double.valueOf(row.getCell(cellIndex).getStringCellValue()).intValue();
		} else {
			throw new Exception("Cannot convert cell value into int");
		}
	}
	
	public static String getCellDataAsString(Row row, int cellIndex) throws Exception {
		CellType cellType = row.getCell(cellIndex).getCellTypeEnum();
		if(cellType == CellType.NUMERIC) {
			return "" + Double.valueOf(row.getCell(cellIndex).getNumericCellValue()).intValue();
		} else if(cellType == CellType.STRING) {
			return row.getCell(cellIndex).getStringCellValue();
		} else {
			throw new Exception("Cannot convert cell value into String");
		}
	}
	
}
