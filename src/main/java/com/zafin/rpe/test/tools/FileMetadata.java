package com.zafin.rpe.test.tools;

public class FileMetadata {

	private String dataBlockName;
	private String filePrefix;
	private int columnCount;
	private FileMetadataFileType fileType;
	
	public String getDataBlockName() {
		return dataBlockName;
	}
	public void setDataBlockName(String dataBlockName) {
		this.dataBlockName = dataBlockName;
	}
	public String getFilePrefix() {
		return filePrefix;
	}
	public void setFilePrefix(String filePrefix) {
		this.filePrefix = filePrefix;
	}
	public int getColumnCount() {
		return columnCount;
	}
	public void setColumnCount(int columnCount) {
		this.columnCount = columnCount;
	}
	public FileMetadataFileType getFileType() {
		return fileType;
	}
	public void setFileType(FileMetadataFileType fileType) {
		this.fileType = fileType;
	}
	
	
	
}
