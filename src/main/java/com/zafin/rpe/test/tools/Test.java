package com.zafin.rpe.test.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Test {

	private static final Logger logger = LoggerFactory.getLogger(Test.class);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//setupDataFolder(new File("."));
		//String filePath = "/Users/Jenston/Test/testDataExporter/ACCOUNT_BALANCE.DAT";
		//updateFileCountInHeader(new File(filePath), 44444);
		
		String testCaseFileName = "TC038.FEE_20180124.DAT";
		String outputFileName = getOutputFileName(testCaseFileName);
		String testCaseId = getTestCaseId(testCaseFileName);
		System.out.println("OutputFileName: " + outputFileName + ", TestCaseId: " + testCaseId);
	}
	
	private static String getOutputFileName(String fileName) {
		List<String> splitStrings = splitStringUsingDelimiter(fileName, ".");
		return splitStrings.get(1);
	}

	private static String getTestCaseId(String fileName) {
		List<String> splitStrings = splitStringUsingDelimiter(fileName, ".");
		return splitStrings.get(0);
	}
	
	public static List<String> splitStringUsingDelimiter(String str, String delimiter) {
		List<String> dataValues = new ArrayList<String>();
		Scanner sc = new Scanner(str);
		sc.useDelimiter("[" + delimiter + "]");

		while (sc.hasNext()) {
			String value = sc.next();
			dataValues.add(value);
		}
		sc.close();
		return dataValues;
	}
	
	private static void updateFileCountInHeader(File file, long count) {
		try {
			RandomAccessFile raf = new RandomAccessFile(file, "rw");
			raf.seek(11);
			String countString = ""+count+"\n";
			byte[] byteArray = countString.getBytes();
			raf.write(byteArray, 0, countString.length());
			raf.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String setupDataFolder(File file) {
		File dataFolder = null;
		try {
			File parentFile = file.getParentFile();
			if(parentFile == null) { //If full path is not fount, assume current directory
				parentFile = new File(".");
			}
			if(parentFile.canWrite()) {
				//Delete existing data folder
				String pathToDataFolder = parentFile.getAbsolutePath() + "/" + "data";
				dataFolder = new File(pathToDataFolder);
				if(dataFolder.exists()) {
					logger.info("Data folder '"+ pathToDataFolder + "' exists, deleting");
					FileUtils.deleteDirectory(dataFolder);
					logger.info("Data folder deleted successfully");
					//Creating an empty data folder
					dataFolder.mkdir();
					logger.info("Created new data folder successfully");
				} else {
					logger.info("Data folder '"+ pathToDataFolder + "' does not exists, creating one");
					dataFolder.mkdir();
					logger.info("Data folder created successfully");
				}
			} else {
				throw new TestingToolsRuntimeException("Cannot write to directory: '" + parentFile.getAbsolutePath() + "'");
			}
		} catch (Exception e) {
			throw new TestingToolsRuntimeException(e);
		}
		return dataFolder.getAbsolutePath();
	}
	
	private void testChunking(long totalCustomerCount, long batchSize) {
		int numOfChunks = (int)Math.ceil((double)totalCustomerCount / batchSize);
        for(int i = 0; i < numOfChunks; ++i) {
            long start = i * batchSize;
            long length = Math.min(totalCustomerCount - start, batchSize);
            System.out.println(""+length);
        }
	}
	
	private List<Long> getCustomerCountBatches(long totalCustomerCount, int batchSize) {
		List<Long> batches = new ArrayList<>();
		if(totalCustomerCount <= batchSize) {
			batches.add(new Long(batchSize));
		} else {
			int numOfChunks = (int)Math.ceil((double)totalCustomerCount / batchSize);
	        for(int i = 0; i < numOfChunks; ++i) {
	            int start = i * batchSize;
	            long length = Math.min(totalCustomerCount - start, batchSize);
	            System.out.println(""+length);
	        }
		}
		return batches;
	}
	
	private void testHeaderInsert(String fileName, String tempFileName) {
		BufferedReader reader = null;
		BufferedWriter writer = null;
		try {
		    reader = new BufferedReader(new FileReader(fileName));
		    writer = new BufferedWriter(new FileWriter(tempFileName));
		    writer.write("#|20180101|234" + "\r\n");
		    String tmp;
		    while ((tmp = reader.readLine()) != null) {
		    	writer.write(tmp + "\r\n");
		    }
		} catch (Exception e) {
		    e.printStackTrace();
		} finally {
			try {
				reader.close();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
