package com.zafin.rpe.test.tools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestingToolsApp {

	public static void main(String[] args) {
		SpringApplication.run(TestingToolsApp.class, args);
	}
}
