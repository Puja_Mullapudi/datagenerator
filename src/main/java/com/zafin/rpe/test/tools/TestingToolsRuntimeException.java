package com.zafin.rpe.test.tools;

public class TestingToolsRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public TestingToolsRuntimeException(String message) {
		super(message);
	}

	public TestingToolsRuntimeException(Exception e) {
		super(e);
	}

}
