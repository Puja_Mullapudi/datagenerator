package com.zafin.rpe.test.tools.generator;

public class AccountData {

	private String accountProfileName;
	private String accountNumber;
	private EntityType entityType;

	private String accountStatus = "ACTIVE";

	private String inGoodStanding;
	
	private AccountsPTDGSpecification accountsPTDGSpecification;
	
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public EntityType getEntityType() {
		return entityType;
	}
	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}
	public AccountsPTDGSpecification getAccountsPTDGSpecification() {
		return accountsPTDGSpecification;
	}
	public void setAccountsPTDGSpecification(AccountsPTDGSpecification accountsPTDGSpecification) {
		this.accountsPTDGSpecification = accountsPTDGSpecification;
	}
	public String getAccountProfileName() {
		return accountProfileName;
	}
	public void setAccountProfileName(String accountProfileName) {
		this.accountProfileName = accountProfileName;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public String getInGoodStanding() {
		return inGoodStanding;
	}

	public void setInGoodStanding(String inGoodStanding) {
		this.inGoodStanding = inGoodStanding;
	}

}
