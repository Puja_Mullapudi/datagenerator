package com.zafin.rpe.test.tools.generator;

public class AccountRelationshipData {

	private String role;
	private String accountNumber;
	private String customerId;
	
	public AccountRelationshipData(String role, String accountNumber, String customerId) {
		this.role = role;
		this.accountNumber = accountNumber;
		this.customerId = customerId; 
	}
	public String getRole() {
		return role;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public String getCustomerId() {
		return customerId;
	}
	
}
