package com.zafin.rpe.test.tools.generator;

import java.util.ArrayList;
import java.util.List;

public class AccountsPTDGSpecification {

	//Number of accounts are ignored in a customer graph setup
	private int numberOfAccounts = 1;
	
	private String productCode;
	private double closingDayBalance;
	private double averageBalance;
	private String accountCohort;
	private String accountOpenDate;
	private EntityType entityType;
	private String payrollAccount = "N";
	private String cycleEndDate;
	private String customCategory;
	private String accountProfileName;
	private String relationshipRole;
	private String regionCode;
	private String accountStatus = "ACTIVE";
	private String inGoodStanding;
	private String billingPlan;

    private List<AccountFeaturePTDGSpecification> accountFeatureSpecifications = new ArrayList<>();
	private List<TransactionsPTDGSpecification> transactionPTDGSpecifications = new ArrayList<>();
	private List<ManualExceptionPTDGSpecification> manualExceptionPTDGSpecifications = new ArrayList<>();
	private List<FeePTDGSpecification> feePTDGSpecifications = new ArrayList<>();
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public double getClosingDayBalance() {
		return closingDayBalance;
	}
	public void setClosingDayBalance(double closingDayBalance) {
		this.closingDayBalance = closingDayBalance;
	}
	public double getAverageBalance() {
		return averageBalance;
	}
	public void setAverageBalance(double averageBalance) {
		this.averageBalance = averageBalance;
	}
	public String getAccountCohort() {
		return accountCohort;
	}
	public void setAccountCohort(String accountCohort) {
		this.accountCohort = accountCohort;
	}
	public String getAccountOpenDate() {
		return accountOpenDate;
	}
	public void setAccountOpenDate(String accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}
	public List<AccountFeaturePTDGSpecification> getAccountFeatureSpecifications() {
		return accountFeatureSpecifications;
	}
	public void setAccountFeatureSpecifications(List<AccountFeaturePTDGSpecification> accountFeatureSpecifications) {
		this.accountFeatureSpecifications = accountFeatureSpecifications;
	}
	public EntityType getEntityType() {
		return entityType;
	}
	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}
	public List<TransactionsPTDGSpecification> getTransactionPTDGSpecifications() {
		return transactionPTDGSpecifications;
	}
	public void setTransactionPTDGSpecifications(List<TransactionsPTDGSpecification> transactionPTDGSpecifications) {
		this.transactionPTDGSpecifications = transactionPTDGSpecifications;
	}
	public String getPayrollAccount() {
		return payrollAccount;
	}
	public void setPayrollAccount(String payrollAccount) {
		this.payrollAccount = payrollAccount;
	}
	public String getCycleEndDate() {
		return cycleEndDate;
	}
	public void setCycleEndDate(String cycleEndDate) {
		this.cycleEndDate = cycleEndDate;
	}
	public String getCustomCategory() {
		return customCategory;
	}
	public void setCustomCategory(String customCategory) {
		this.customCategory = customCategory;
	}
	public int getNumberOfAccounts() {
		return numberOfAccounts;
	}
	public void setNumberOfAccounts(int numberOfAccounts) {
		this.numberOfAccounts = numberOfAccounts;
	}
	public List<ManualExceptionPTDGSpecification> getManualExceptionPTDGSpecifications() {
		return manualExceptionPTDGSpecifications;
	}
	public void setManualExceptionPTDGSpecifications(List<ManualExceptionPTDGSpecification> manualExceptionPTDGSpecifications) {
		this.manualExceptionPTDGSpecifications = manualExceptionPTDGSpecifications;
	}
	public String getAccountProfileName() {
		return accountProfileName;
	}
	public void setAccountProfileName(String accountProfileName) {
		this.accountProfileName = accountProfileName;
	}
	public String getRelationshipRole() {
		return relationshipRole;
	}
	public void setRelationshipRole(String relationshipRole) {
		this.relationshipRole = relationshipRole;
	}
	public String getRegionCode() {
		return regionCode;
	}
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	public List<FeePTDGSpecification> getFeePTDGSpecifications() {
		return feePTDGSpecifications;
	}
	public void setFeePTDGSpecifications(List<FeePTDGSpecification> feePTDGSpecifications) {
		this.feePTDGSpecifications = feePTDGSpecifications;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public String getInGoodStanding() {
		return inGoodStanding;
	}

	public void setInGoodStanding(String inGoodStanding) {
		this.inGoodStanding = inGoodStanding;
    }

    public String getBillingPlan() {
        return billingPlan;
    }

    public void setBillingPlan(String billingPlan) {
        this.billingPlan = billingPlan;
    }
	

}
