package com.zafin.rpe.test.tools.generator;

/**
 * 
 * Customer Graph 1
 *  3 Customers, 4 accounts, 7 relationships
 * Customer Graph 2
 *  5 Customers, 6 accounts, 12 relationships
 * Customer Graph 3
 *  4 Customers, 4 accounts, 7 relationships
 * Customer Graph 4
 *  2 Customers, 6 accounts, 8 relationships
 * Customer Graph 5
 *  2 Customers, 7 accounts, 8 relationships
 * Customer Graph 6
 *  2 Customers, 1 Joint SPA, 19 INV per customer
 * Customer Graph 7
 *  3 Customers, 1 Joint SPA, 19 INV per customer
 * Customer Graph 8
 *  1 Customer, 1 Sole SPA, 19 INV per customer, 2 Credit Cards
 * Customer Graph 9
 *  1 Customer, 2 Sole SPA, 18 INV per customer, 2 Credit Cards
 * RG - Customer Graph 10
 *  2 Customers, 1 Joint SPAs, 2 Credit Cards
 * RG - Customer Graph 11
 *  2 Customers, 1 Joint SPA, 2 Credit Cards
 * RG - Customer Graph 12
 *  2 Customers, 1 Joint SPAs, 2 Sole SPAs, 2 Credit Cards
 * RG - Customer Graph 13
 *  3 Customers, 3 Joint SPAs, 3 Credit Cards
 * Day Zero:
 *  Customer Graph 1: 1K
 *  Customer Graph 2: 1K
 *  Customer Graph 3: 1K
 *  Customer Graph 4: 1K
 *  Customer Graph 5: 1K
 *  Customer Graph 6: 47K
 *  Customer Graph 7: 50K
 *  Customer Graph 8: 33K
 *  Customer Graph 9: 33K 
 *  RG - Customer Graph 10: 100K
 * Day 2-30:
 *  Customer Graph 9: 80
 *  RG - Customer Graph 11: 40
 *  RG - Customer Graph 12: 60
 *  RG - Customer Graph 13: 100
 * Day 31:
 *   --
 *
 */
public class CIBCJointConfig {

	private static final String DEFAULT_ACCOUNT_OPEN_DATE = "20190101";
	private static final String PERMANENT_WAIVER_REASON_CODE = "PFW";
	private static final String PRODUCT_CODE_B0 = "B0";
	private static final String PRODUCT_CODE_INVT = "INVT";
	private static final String SEGMENT_GOLD = "GOLD";
	private static final String SEGMENT_SILVER = "SILVER";
	private static final String SEGMENT_BLANK = "";
	private static final String EMP_STATUS_STAFF = "STAFF";
	private static final String EMP_STATUS_BLANK = "";
	private static final String MANUAL_EXCEPTION_FEE_ITEM_CODE = "1";
	private static final String PRODUCT_CODE_CC_P1 = "VGARO";
	private static final String PRODUCT_CODE_CC_P2 = "VPDIV";
	private static final String PRODUCT_CODE_CC_P3 = "VGLIS";
	private static long multiplicationFactor = 1000; //1000
	
	public static PTDGSpecification getDataGenerationSpecification() {
		PTDGSpecification specification = new PTDGSpecification();
		specification.setCustomPTDGSpecification(getCustomPTDGSpecification());
		specification.setSystemDate("01-JAN-2019");
		return specification;
	}
	
	private static CustomPTDGSpecification getCustomPTDGSpecification() {
		CustomPTDGSpecification customPTDGSpecification = new CustomPTDGSpecification();
		
		//16/1 Customer, 27/51 Accounts, 42/66 relationships
		
		//Day Zero: 01 Jan 2019
	    DayEndSpecification dayEndSpecification = new DayEndSpecification("20190101", true, 0, 0);
	    //3 Customers, 4 accounts, 7 relationships
	    dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph1(1*multiplicationFactor));//MODIFY
	    //5 Customers, 6 accounts, 12 relationships
	    dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph2(1*multiplicationFactor));//MODIFY
	    //4 Customers, 4 accounts, 7 relationships
	    dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph3(1*multiplicationFactor));//MODIFY
	    //2 Customers, 6 accounts, 8 relationships
	    dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph4(1*multiplicationFactor));//MODIFY
	    //2 Customers, 7 accounts, 8 relationships
	    dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph5(1*multiplicationFactor));//MODIFY
	    
	    //2 Customers, 1 Joint SPA, 19 INV per customer
	    dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph6(47*multiplicationFactor));//MODIFY
	    //3 Customers, 1 Joint SPA, 19 INV per customer
	    dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph7(50*multiplicationFactor));//MODIFY	    
	    //1 Customer, 1 Sole SPA, 19 INV per customer, 2 Credit Cards
	    dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph8(33*multiplicationFactor));//MODIFY	    
	    //1 Customer, 2 Sole SPA, 18 INV per customer, 2 Credit Cards
	    dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph9(33*multiplicationFactor));//MODIFY
	    
	    //2 Customers, 1 Joint SPAs, 2 Credit Cards
	    dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createRebateGroupCustomerGraph10(100*multiplicationFactor));//MODIFY
	    customPTDGSpecification.getDayEndSpecifications().add(dayEndSpecification);
	    
	    //Intermediate days - 3 Million account updates, 2560 Customer, 4650 SPA Accounts, 1440 INV accounts, 4480 Credit Cards
	    int dayEndDate = 20190101;
	    for (int i = 0; i < 29; i++) {
	    	dayEndDate = dayEndDate + 1;
	    	
	    	//Intermediate day with full balance update
		    DayEndSpecification intermediateDayEndSpecification = new DayEndSpecification(""+dayEndDate, false, 0, 3000000);
		    //1 Customer, 2 Sole SPA, 18 INV per customer, 2 Credit Cards, 2 rebate groups
		    //80 Customer, 160 SPA accounts, 1440 INV accounts, 160 credit cards, 160 rebate groups
		    intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph9(80));
	    	//2 Customers, 1 Joint SPA, 2 Credit Cards, 1 rebate groups
		    //80 Customer, 40 SPA accounts, 80 credit cards, 40 rebate groups
		    intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createRebateGroupCustomerGraph11(40));//40
		    //2 Customers, 1 Joint SPAs, 2 Sole SPAs, 2 Credit Cards - 2 rebate groups (100)
		    //100 Customer, 150 SPA accounts, 100 credit cards, 100 rebate groups
		    intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createRebateGroupCustomerGraph12(50));//50
		    //3 Customers, 3 Joint SPAs, 3 Credit Cards, 3 rebate groups (300)
		    //300 Customer, 300 SPA accounts, 300 credit cards, 300 rebate groups
		    intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createRebateGroupCustomerGraph13(100));//100
		    //1 Customer, 2 Sole SPA, 2 Credit Cards, 2 rebate groups
		    //2000 Customer, 4000 SPA accounts, 4000 credit cards, 4000 rebate groups
		    intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createRebateGroupCustomerGraph14(2000));//2000
		    customPTDGSpecification.getDayEndSpecifications().add(intermediateDayEndSpecification);
		}
	    
	    //Month End: 31 Jan 2019
	    DayEndSpecification monthEndSpecification = new DayEndSpecification("20190131", false, 0, 0);
	    customPTDGSpecification.getDayEndSpecifications().add(monthEndSpecification);
	    
		return customPTDGSpecification;
	}
	
	private static AccountsPTDGSpecification createAccountsPTDGSpecification(int numberOfAccounts, String accountProfileName, EntityType entityType, 
			String productCode, double closingDayBalance, String accountOpenDate) {
		AccountsPTDGSpecification accountsPTDGSpecification = new AccountsPTDGSpecification();
		accountsPTDGSpecification.setNumberOfAccounts(numberOfAccounts);
		accountsPTDGSpecification.setAccountCohort("");
		accountsPTDGSpecification.setAccountOpenDate(accountOpenDate);
		accountsPTDGSpecification.setAverageBalance(0.0);
		accountsPTDGSpecification.setClosingDayBalance(closingDayBalance);
		accountsPTDGSpecification.setProductCode(productCode);
		accountsPTDGSpecification.setEntityType(entityType);
		accountsPTDGSpecification.setPayrollAccount("N");
		accountsPTDGSpecification.setCycleEndDate("");
		accountsPTDGSpecification.setCustomCategory("");
		accountsPTDGSpecification.setAccountProfileName(accountProfileName);
		return accountsPTDGSpecification;
	}
	
	private static AccountsPTDGSpecification createAccountsPTDGSpecification(int numberOfAccounts, String accountProfileName, EntityType entityType, String productCode, double closingDayBalance) {
		return createAccountsPTDGSpecification(numberOfAccounts, accountProfileName, entityType, productCode, closingDayBalance, DEFAULT_ACCOUNT_OPEN_DATE);
	}
	
	private static CustomerPTDGSpecification createCustomerPTDGSpecification(long numberOfCustomers, String customerProfileName, String customerSegment, String employeeStatus) {
		CustomerPTDGSpecification customerPTDGSpecification = new CustomerPTDGSpecification();
		customerPTDGSpecification.setCustomerSegment(customerSegment);
		customerPTDGSpecification.setCustomerOpenDate("20170101");
		customerPTDGSpecification.setBranchCode("");
		customerPTDGSpecification.setCustomerCohortCategoryCode("");
		customerPTDGSpecification.setCustomerCohortCode("");
		customerPTDGSpecification.setCustomerStatus("ACTIVE");
		customerPTDGSpecification.setCustomerBirthDate("19820829");
		customerPTDGSpecification.setCustomerEmployeeStatus(employeeStatus);
		customerPTDGSpecification.setNumberOfCustomers(numberOfCustomers);
		customerPTDGSpecification.setCustomerProfileName(customerProfileName);
		return customerPTDGSpecification;
	}
	
	private static ManualExceptionPTDGSpecification createManualExceptionPTDGSpecification(String startDate, String endDate, String feeOrReward, String feeItemCode, String reason) {
		ManualExceptionPTDGSpecification manualExceptionPTDGSpecification = new ManualExceptionPTDGSpecification();
		manualExceptionPTDGSpecification.setEndDate(endDate);
		manualExceptionPTDGSpecification.setFeeItemCode(feeItemCode);
		manualExceptionPTDGSpecification.setFeeOrReward(feeOrReward);
		manualExceptionPTDGSpecification.setReason(reason);
		manualExceptionPTDGSpecification.setStartDate(startDate);
		return manualExceptionPTDGSpecification;
	}
	
	//3 Customers, 4 accounts, 7 relationships
	private static CustomerGraphPTDGSpecification createCustomerGraph1(long numberOfCustomerGraphs) {
		
		CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
		jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);
		
		//Create customer profiles
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1", SEGMENT_BLANK, EMP_STATUS_BLANK));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C2", SEGMENT_GOLD, EMP_STATUS_BLANK));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C3", SEGMENT_BLANK, EMP_STATUS_STAFF));
		
		//Create account profiles - Number of accounts are ignored in a customer graph setup
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a1", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a2", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a3", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "sec_a1", EntityType.TIME_DEPOSIT_ACCOUNT, PRODUCT_CODE_INVT, 100000));
	    
		//Add the customer account relationships
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a1", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a1", "PRIMARY");
		
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a2", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "dep_a2", "PRIMARY");
		
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "dep_a3", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a3", "PRIMARY");
		
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "sec_a1", "SOLE");
		
		return jointCustomerPTDGSpecification;
	}
	
	//5 Customers, 6 accounts, 12 relationships
	private static CustomerGraphPTDGSpecification createCustomerGraph2(long numberOfCustomerGraphs) {
		
		CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
		jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);
		
		//Create customer profiles
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1", SEGMENT_BLANK, EMP_STATUS_BLANK));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C2", SEGMENT_SILVER, EMP_STATUS_BLANK));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C3", SEGMENT_BLANK, EMP_STATUS_BLANK));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C4", SEGMENT_BLANK, EMP_STATUS_BLANK));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C5", SEGMENT_BLANK, EMP_STATUS_STAFF));
		
		//Create account profiles - Number of accounts are ignored in a customer graph setup
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a1", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a2", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a3", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a4", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a5", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 8000));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "sec_a1", EntityType.TIME_DEPOSIT_ACCOUNT, PRODUCT_CODE_INVT, 100000));
	    
		//Add the customer account relationships
		//cspec1 & cspec2 joint on aspec1 & aspec2
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a1", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a1", "PRIMARY");
		
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a2", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a2", "PRIMARY");
		
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a3", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a3", "PRIMARY");
		
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a4", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "dep_a4", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C4", "dep_a4", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C5", "dep_a4", "PRIMARY");
		
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "sec_a1", "SOLE");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C5", "dep_a5", "SOLE");
		
		return jointCustomerPTDGSpecification;
	}
	
	//4 Customers, 4 accounts, 7 relationships
	private static CustomerGraphPTDGSpecification createCustomerGraph3(long numberOfCustomerGraphs) {
		
		CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
		jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);
		
		//Create customer profiles
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1", SEGMENT_BLANK, EMP_STATUS_STAFF));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C2", SEGMENT_SILVER, EMP_STATUS_BLANK));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C3", SEGMENT_BLANK, EMP_STATUS_BLANK));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C4", SEGMENT_BLANK, EMP_STATUS_BLANK));
		
		//Create account profiles - Number of accounts are ignored in a customer graph setup
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a1", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a2", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a3", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "sec_a1", EntityType.TIME_DEPOSIT_ACCOUNT, PRODUCT_CODE_INVT, 100000));
	    
		//Add the customer account relationships
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a1", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a1", "PRIMARY");
		
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a2", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "dep_a2", "PRIMARY");
		
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a3", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C4", "dep_a3", "PRIMARY");
		
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C4", "sec_a1", "SOLE");
		
		return jointCustomerPTDGSpecification;
	}
	
	//2 Customers, 6 accounts, 8 relationships
	private static CustomerGraphPTDGSpecification createCustomerGraph4(long numberOfCustomerGraphs) {
		
		CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
		jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);
		
		//Create customer profiles
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1", SEGMENT_BLANK, EMP_STATUS_BLANK));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C2", SEGMENT_BLANK, EMP_STATUS_BLANK));
		
		//Create account profiles - Number of accounts are ignored in a customer graph setup
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a1", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a2", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		
		//Add manual exception for dep_a3
		AccountsPTDGSpecification dep_a3 = createAccountsPTDGSpecification(1, "dep_a3", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500);
		dep_a3.getManualExceptionPTDGSpecifications().add(createManualExceptionPTDGSpecification("20180101","20201231","F", MANUAL_EXCEPTION_FEE_ITEM_CODE,PERMANENT_WAIVER_REASON_CODE));
		jointCustomerPTDGSpecification.getAccountProfiles().add(dep_a3);
		
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a4", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 8000));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a5", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 8000));

		//Add manual exception for dep_a6
		AccountsPTDGSpecification dep_a6 = createAccountsPTDGSpecification(1, "dep_a6", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500);
		dep_a6.getManualExceptionPTDGSpecifications().add(createManualExceptionPTDGSpecification("20180101","20201231","F", MANUAL_EXCEPTION_FEE_ITEM_CODE,PERMANENT_WAIVER_REASON_CODE));
		jointCustomerPTDGSpecification.getAccountProfiles().add(dep_a6);
		
		//Add the customer account relationships
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a1", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a1", "PRIMARY");
		
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a2", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a2", "PRIMARY");
		
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a3", "SOLE");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a4", "SOLE");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a5", "SOLE");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a6", "SOLE");
		
		return jointCustomerPTDGSpecification;
	}
	
	//2 Customers, 7 accounts, 8 relationships
	private static CustomerGraphPTDGSpecification createCustomerGraph5(long numberOfCustomerGraphs) {
		
		CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
		jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);
		
		//Create customer profiles
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1", SEGMENT_BLANK, EMP_STATUS_BLANK));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C2", SEGMENT_BLANK, EMP_STATUS_BLANK));
		
		//Create account profiles - Number of accounts are ignored in a customer graph setup
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a1", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 8000));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a2", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 8000));
		
		//Add manual exception for dep_a3
		AccountsPTDGSpecification dep_a3 = createAccountsPTDGSpecification(1, "dep_a3", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500);
		dep_a3.getManualExceptionPTDGSpecifications().add(createManualExceptionPTDGSpecification("20180101","20201231","F", MANUAL_EXCEPTION_FEE_ITEM_CODE,PERMANENT_WAIVER_REASON_CODE));
		jointCustomerPTDGSpecification.getAccountProfiles().add(dep_a3);
		
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a4", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		
		//Add manual exception for dep_a5
		AccountsPTDGSpecification dep_a5 = createAccountsPTDGSpecification(1, "dep_a5", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500);
		dep_a5.getManualExceptionPTDGSpecifications().add(createManualExceptionPTDGSpecification("20180101","20201231","F", MANUAL_EXCEPTION_FEE_ITEM_CODE,PERMANENT_WAIVER_REASON_CODE));
		jointCustomerPTDGSpecification.getAccountProfiles().add(dep_a5);
		
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a6", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 8000));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a7", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 8000));
	    
		//Add the customer account relationships
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a4", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a4", "PRIMARY");
		
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a1", "SOLE");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a2", "SOLE");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a3", "SOLE");
		
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a5", "SOLE");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a6", "SOLE");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a7", "SOLE");
		
		return jointCustomerPTDGSpecification;
	}
	
	//1 Customer, 24 
	private static CustomerPTDGSpecification createCustomerPTDGSpecification1(long numberOfCustomers) {
		CustomerPTDGSpecification customerPTDGSpecification = createCustomerPTDGSpecification(numberOfCustomers, "", SEGMENT_BLANK, EMP_STATUS_BLANK);
		
		//10 [ Product Code : B0 ] - Accounts open on 1st November
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(1, "", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1000, "20181101"));
	       
	    //14 [ Product Code : GIC ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(14, "", EntityType.TIME_DEPOSIT_ACCOUNT, PRODUCT_CODE_INVT, 1000));
	    	    
		return customerPTDGSpecification;
	}
	
	//2 Customers, 1 Joint SPA, 19 INV per customer
	private static CustomerGraphPTDGSpecification createCustomerGraph6(long numberOfCustomerGraphs) {
		CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
		jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);
		
		//Create customer profiles
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1", SEGMENT_BLANK, EMP_STATUS_BLANK));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C2", SEGMENT_BLANK, EMP_STATUS_BLANK));
		
		//Create account profiles - Number of accounts are ignored in a customer graph setup
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "joint_dep_a1", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "joint_dep_a1", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "joint_dep_a1", "PRIMARY");
		
		for (int i = 1; i < 20; i++) {
			jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "sec_1a"+i, EntityType.TIME_DEPOSIT_ACCOUNT, PRODUCT_CODE_INVT, 100000));
			jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "sec_1a"+i, "SOLE");
		}

		for (int i = 1; i < 20; i++) {
			jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "sec_2a"+i, EntityType.TIME_DEPOSIT_ACCOUNT, PRODUCT_CODE_INVT, 100000));
			jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "sec_2a"+i, "SOLE");
		}
		
		return jointCustomerPTDGSpecification;
	}
	
	//3 Customers, 1 Joint SPA, 19 INV per customer
	private static CustomerGraphPTDGSpecification createCustomerGraph7(long numberOfCustomerGraphs) {
		CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
		jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);
		
		//Create customer profiles
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1", SEGMENT_BLANK, EMP_STATUS_BLANK));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C2", SEGMENT_BLANK, EMP_STATUS_BLANK));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C3", SEGMENT_BLANK, EMP_STATUS_BLANK));
		
		//Create account profiles - Number of accounts are ignored in a customer graph setup
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "joint_dep_a1", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "joint_dep_a1", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "joint_dep_a1", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "joint_dep_a1", "PRIMARY");
		
		for (int i = 1; i < 20; i++) {
			jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "sec_1a"+i, EntityType.TIME_DEPOSIT_ACCOUNT, PRODUCT_CODE_INVT, 100000));
			jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "sec_1a"+i, "SOLE");
		}
		
		for (int i = 1; i < 20; i++) {
			jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "sec_2a"+i, EntityType.TIME_DEPOSIT_ACCOUNT, PRODUCT_CODE_INVT, 100000));
			jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "sec_2a"+i, "SOLE");
		}
		
		for (int i = 1; i < 20; i++) {
			jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "sec_3a"+i, EntityType.TIME_DEPOSIT_ACCOUNT, PRODUCT_CODE_INVT, 100000));
			jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "sec_3a"+i, "SOLE");
		}
		
		return jointCustomerPTDGSpecification;
	}
	
	

	//1 Customer, 1 Sole SPA, 19 INV per customer, 2 Credit Cards
	private static CustomerGraphPTDGSpecification createCustomerGraph8(long numberOfCustomerGraphs) {
		CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
		jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);
		
		//Create customer profiles
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1", SEGMENT_BLANK, EMP_STATUS_STAFF));
		
		//Create account profiles - Number of accounts are ignored in a customer graph setup
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a1", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a1", "SOLE");
		
		for (int i = 1; i < 20; i++) {
			jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "sec_1a"+i, EntityType.TIME_DEPOSIT_ACCOUNT, PRODUCT_CODE_INVT, 100000));
			jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "sec_1a"+i, "SOLE");
		}
		
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "cc_p1", EntityType.CREDIT_CARD_ACCOUNT, PRODUCT_CODE_CC_P1, 0));
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "cc_p1", "SOLE");
		
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "cc_p2", EntityType.CREDIT_CARD_ACCOUNT, PRODUCT_CODE_CC_P1, 0));
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "cc_p2", "SOLE");
		
		return jointCustomerPTDGSpecification;
	}

	//1 Customer, 2 Sole SPA, 18 INV per customer, 2 Credit Cards
	private static CustomerGraphPTDGSpecification createCustomerGraph9(long numberOfCustomerGraphs) {
		CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
		jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);
		
		//Create customer profiles
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1", SEGMENT_BLANK, EMP_STATUS_STAFF));
		
		//Create account profiles - Number of accounts are ignored in a customer graph setup
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a1", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a1", "SOLE");
		
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a2", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a2", "SOLE");
		
		for (int i = 1; i < 19; i++) {
			jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "sec_1a"+i, EntityType.TIME_DEPOSIT_ACCOUNT, PRODUCT_CODE_INVT, 100000));
			jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "sec_1a"+i, "SOLE");
		}
		
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "cc_p1", EntityType.CREDIT_CARD_ACCOUNT, PRODUCT_CODE_CC_P1, 0));
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "cc_p1", "SOLE");
		
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "cc_p2", EntityType.CREDIT_CARD_ACCOUNT, PRODUCT_CODE_CC_P1, 0));
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "cc_p2", "SOLE");
		
		return jointCustomerPTDGSpecification;
	}

	
	//Rebate Group Usecase - 1 Customer, 1 Sole account, 2 Credit Cards
	private static CustomerGraphPTDGSpecification createRebateGroupCustomerGraph10(long numberOfCustomerGraphs) {
		
		CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
		jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);
		
		//Create customer profiles
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1", SEGMENT_BLANK, EMP_STATUS_BLANK));
		
		//Create account profiles - Number of accounts are ignored in a customer graph setup
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a1", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 0));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "cc_p1", EntityType.CREDIT_CARD_ACCOUNT, PRODUCT_CODE_CC_P1, 0));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "cc_p2", EntityType.CREDIT_CARD_ACCOUNT, PRODUCT_CODE_CC_P2, 0));
		
		//Add the customer account relationships
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a1", "SOLE");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "cc_p1", "SOLE");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "cc_p2", "SOLE");
		
		return jointCustomerPTDGSpecification;
	}
	
	//Rebate Group Usecase - 2 Customers, 1 Joint account, 2 Credit Cards
	private static CustomerGraphPTDGSpecification createRebateGroupCustomerGraph11(long numberOfCustomerGraphs) {
		
		CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
		jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);
		
		//Create customer profiles
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1", SEGMENT_BLANK, EMP_STATUS_BLANK));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C2", SEGMENT_BLANK, EMP_STATUS_BLANK));
		
		//Create account profiles - Number of accounts are ignored in a customer graph setup
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a1", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 0));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "cc_p1", EntityType.CREDIT_CARD_ACCOUNT, PRODUCT_CODE_CC_P1, 0));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "cc_p2", EntityType.CREDIT_CARD_ACCOUNT, PRODUCT_CODE_CC_P2, 0));
		
		//Add the customer account relationships
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a1", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a1", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "cc_p1", "SOLE");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "cc_p2", "SOLE");
		
		return jointCustomerPTDGSpecification;
	}

	//Rebate Group Usecase - 2 Customers, 1 Joint debit account, 2 sole debit cards, 2 Credit Cards
	private static CustomerGraphPTDGSpecification createRebateGroupCustomerGraph12(long numberOfCustomerGraphs) {
		
		CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
		jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);
		
		//Create customer profiles
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1", SEGMENT_BLANK, EMP_STATUS_BLANK));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C2", SEGMENT_BLANK, EMP_STATUS_BLANK));
		
		//Create account profiles - Number of accounts are ignored in a customer graph setup
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a1", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 0));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "cc_p1", EntityType.CREDIT_CARD_ACCOUNT, PRODUCT_CODE_CC_P1, 0));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "cc_p2", EntityType.CREDIT_CARD_ACCOUNT, PRODUCT_CODE_CC_P2, 0));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a2", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 0));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a3", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 0));
		
		//Add the customer account relationships
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a1", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a1", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "cc_p1", "SOLE");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a2", "SOLE");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "cc_p2", "SOLE");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a3", "SOLE");
		
		return jointCustomerPTDGSpecification;
	}

	//Rebate Group Usecase - 3 Customers, 3 Joint debit accounts, 3 Credit Cards
	private static CustomerGraphPTDGSpecification createRebateGroupCustomerGraph13(long numberOfCustomerGraphs) {
		
		CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
		jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);
		
		//Create customer profiles
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1", SEGMENT_BLANK, EMP_STATUS_BLANK));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C2", SEGMENT_BLANK, EMP_STATUS_BLANK));
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C3", SEGMENT_BLANK, EMP_STATUS_BLANK));
		
		//Create account profiles - Number of accounts are ignored in a customer graph setup
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a1", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 0));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a2", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 0));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a3", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 0));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "cc_p1", EntityType.CREDIT_CARD_ACCOUNT, PRODUCT_CODE_CC_P1, 0));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "cc_p2", EntityType.CREDIT_CARD_ACCOUNT, PRODUCT_CODE_CC_P2, 0));
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "cc_p3", EntityType.CREDIT_CARD_ACCOUNT, PRODUCT_CODE_CC_P3, 0));
		
		//Add the customer account relationships
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a1", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a1", "PRIMARY");
		
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a2", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "dep_a2", "PRIMARY");
		
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "dep_a3", "PRIMARY");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "dep_a3", "PRIMARY");
		
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "cc_p1", "SOLE");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "cc_p2", "SOLE");
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "cc_p3", "SOLE");
		
		return jointCustomerPTDGSpecification;
	}
	
	//1 Customer, 2 Sole SPA, 2 Credit Cards
	private static CustomerGraphPTDGSpecification createRebateGroupCustomerGraph14(long numberOfCustomerGraphs) {
		CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
		jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);
		
		//Create customer profiles
		jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1", SEGMENT_BLANK, EMP_STATUS_STAFF));
		
		//Create account profiles - Number of accounts are ignored in a customer graph setup
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a1", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a1", "SOLE");
		
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "dep_a2", EntityType.DEPOSIT_ACCOUNT, PRODUCT_CODE_B0, 1500));
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "dep_a2", "SOLE");
		
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "cc_p1", EntityType.CREDIT_CARD_ACCOUNT, PRODUCT_CODE_CC_P1, 0));
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "cc_p1", "SOLE");
		
		jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "cc_p2", EntityType.CREDIT_CARD_ACCOUNT, PRODUCT_CODE_CC_P1, 0));
		jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "cc_p2", "SOLE");
		
		return jointCustomerPTDGSpecification;
	}
	
}
