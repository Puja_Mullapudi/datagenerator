package com.zafin.rpe.test.tools.generator;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Customers & Account Profiles
    * All accounts have Calendar account cycle
    * Customer Profile 1: 35K of Customer have one SPA Accounts that has a manual exception for ‘Permanent Waiver’ (10%)
        * Accounts (24)
            * Deposit Account -  5 [ Product Code : DEP ]
            * IPS GIC - 14 [ Product Code : GIC ]
            * SPA - 1 [ Product Code : SPA ]
            * Mutual Funds : 4 [ Product Code : MF ]
    * Customer Profile 2: 70K of Customer have one SPA Accounts have minimum balance of 6K (20%)
        * Accounts (24)
            * Deposit Account -  5 [ Product Code : DEP ]
            * IPS GIC - 14 [ Product Code : GIC ]
            * SPA - 1 [ Product Code : SPA ]
            * Mutual Funds : 4 [ Product Code : MF ]
    * 105K of Customers have 4 SPA Accounts each
        * Accounts (27)
            * Deposit Account -  5 [ Product Code : DEP ]
            * IPS GIC - 14 [ Product Code : GIC ]
            * SPA - 4 [ Product Code : SPA ]
            * Mutual Funds : 4 [ Product Code : MF
        * Customer Profile 3: 70K of Customer has a customer segment of ‘WoodGundy’ (20%)
            * Each of the accounts have 1K balance each
        * Customer Profile 4: 35K of Customers get 100K (10%)
            * All SPA accounts have less than 6K, no permanent waiver, are not wood gundy customers
            * Each of the accounts have 5K balance each
    * Customer Profile 5: 35K of Customers gets grace period
        * Accounts (24)
            * Deposit Account -  5 [ Product Code : DEP ]
            * IPS GIC - 14 [ Product Code : GIC ]
            * SPA - 1 [ Product Code : SPA ]
            * Mutual Funds : 4 [ Product Code : MF ]
        * No permanent waivers, not wood gundy customers
        * Each of the accounts have 1K balance each
    * Customer Profile 6: 105K of Customers get no waiver
        * Accounts (24)
            * Deposit Account -  5 [ Product Code : DEP ]
            * IPS GIC - 14 [ Product Code : GIC ]
            * SPA - 1 [ Product Code : SPA ]
            * Mutual Funds : 4 [ Product Code : MF ]
        * No permanent waivers, not wood gundy customers
        * Each of the accounts have 1K balance each
 *
 */
public class CIBCPerformanceDataGenerationConfig {

	private static final String PERMANENT_WAIVER_REASON_CODE = "PFW";
	private static final String MUTUAL_FUND_PRODUCT_CODE = "MF";
	private static final String TERM_DEPOSIT_PRODUCT_CODE = "GIC";
	private static final String DEPOSIT_PRODUCT_CODE = "DEP";
	private static final String SPA_PRODUCT_CODE = "SPA";
	private static final String WOOD_GUNDY_SEGMENT = "WG";
	private static final String MANUAL_EXCEPTION_FEE_ITEM_CODE = "MMF";
	private static final String NON_WOOD_GUNDY_SEGMENT = "CPIC";
	private static long multiplicationFactor = 35000; //35000
	
	public static PTDGSpecification getDataGenerationSpecification() {
		PTDGSpecification specification = new PTDGSpecification();
		specification.setCustomPTDGSpecification(getCustomPTDGSpecification());
		return specification;
	}
	
	private static CustomPTDGSpecification getCustomPTDGSpecification() {
		CustomPTDGSpecification customPTDGSpecification = new CustomPTDGSpecification();
		
		//Day Zero: 01 Nov 2018
	    DayEndSpecification dayEndSpecification = new DayEndSpecification("20181101", true, 0, 0);
	    dayEndSpecification.getCustomerPTDGSpecifications().add(createCustomerPTDGSpecification1(1 * multiplicationFactor));//MODIFY
	    dayEndSpecification.getCustomerPTDGSpecifications().add(createCustomerPTDGSpecification2(2 * multiplicationFactor));//MODIFY
	    dayEndSpecification.getCustomerPTDGSpecifications().add(createCustomerPTDGSpecification3(2 * multiplicationFactor));//MODIFY
	    dayEndSpecification.getCustomerPTDGSpecifications().add(createCustomerPTDGSpecification4(1 * multiplicationFactor));//MODIFY
	    dayEndSpecification.getCustomerPTDGSpecifications().add(createCustomerPTDGSpecification5(1 * multiplicationFactor));//MODIFY
	    dayEndSpecification.getCustomerPTDGSpecifications().add(createCustomerPTDGSpecification6(3 * multiplicationFactor));//MODIFY
	    customPTDGSpecification.getDayEndSpecifications().add(dayEndSpecification);
	    
	    int dayEndDate = 20181101;
	    long numberOfAccountsHavingBalanceUpdates = 243 * multiplicationFactor ;//243//8505000
	    for (int i = 0; i < 25; i++) {
	    	dayEndDate = dayEndDate + 1;
	    	//Intermediate day with full balance update
		    DayEndSpecification intermediateDayEndSpecification = new DayEndSpecification(""+dayEndDate, false, 0, numberOfAccountsHavingBalanceUpdates);
		    customPTDGSpecification.getDayEndSpecifications().add(intermediateDayEndSpecification);
		}
	    
		//Normal Day: 26 Nov 2018
	    DayEndSpecification normalDayEndSpecification = new DayEndSpecification("20181126", false, 0, numberOfAccountsHavingBalanceUpdates);
	    customPTDGSpecification.getDayEndSpecifications().add(normalDayEndSpecification);
		
		//Month End: 30 Nov 2018
	    DayEndSpecification monthEndSpecification = new DayEndSpecification("20181130", false, 0, 0);
	    customPTDGSpecification.getDayEndSpecifications().add(monthEndSpecification);
		
		return customPTDGSpecification;
	}
	
	private static AccountsPTDGSpecification createAccountsPTDGSpecification(int numberOfAccounts, EntityType entityType, 
			String productCode, double closingDayBalance, String accountOpenDate) {
		AccountsPTDGSpecification accountsPTDGSpecification = new AccountsPTDGSpecification();
		accountsPTDGSpecification.setNumberOfAccounts(numberOfAccounts);
		accountsPTDGSpecification.setAccountCohort("");
		accountsPTDGSpecification.setAccountOpenDate(accountOpenDate);
		accountsPTDGSpecification.setAverageBalance(0.0);
		accountsPTDGSpecification.setClosingDayBalance(closingDayBalance);
		accountsPTDGSpecification.setProductCode(productCode);
		accountsPTDGSpecification.setEntityType(entityType);
		accountsPTDGSpecification.setPayrollAccount("N");
		accountsPTDGSpecification.setCycleEndDate("");
		accountsPTDGSpecification.setCustomCategory("");
		return accountsPTDGSpecification;
	}
	
	private static AccountsPTDGSpecification createAccountsPTDGSpecification(int numberOfAccounts, EntityType entityType, String productCode, double closingDayBalance) {
		return createAccountsPTDGSpecification(numberOfAccounts, entityType, productCode, closingDayBalance, "20180101");
	}
	
	private static CustomerPTDGSpecification createCustomerPTDGSpecification(long numberOfCustomers, String customerSegment) {
		CustomerPTDGSpecification customerPTDGSpecification = new CustomerPTDGSpecification();
		customerPTDGSpecification.setCustomerSegment(customerSegment);
		customerPTDGSpecification.setCustomerOpenDate("20170101");
		customerPTDGSpecification.setBranchCode("");
		customerPTDGSpecification.setCustomerCohortCategoryCode("");
		customerPTDGSpecification.setCustomerCohortCode("");
		customerPTDGSpecification.setCustomerStatus("ACTIVE");
		customerPTDGSpecification.setCustomerBirthDate("19820829");
		customerPTDGSpecification.setCustomerEmployeeStatus("");
		customerPTDGSpecification.setNumberOfCustomers(numberOfCustomers);
		return customerPTDGSpecification;
	}
	
	/**
	 * Customer Profile 1: 35K Customers have one SPA Accounts that has a manual exception for ‘Permanent Waiver’ (10%)
        * Accounts (24)
            * Deposit Account -  5 [ Product Code : DEP ]
            * IPS GIC - 14 [ Product Code : GIC ]
            * SPA - 1 [ Product Code : SPA ]
            * Mutual Funds : 4 [ Product Code : MF ]
	 */
	private static CustomerPTDGSpecification createCustomerPTDGSpecification1(long numberOfCustomers) {
		CustomerPTDGSpecification customerPTDGSpecification = createCustomerPTDGSpecification(numberOfCustomers, NON_WOOD_GUNDY_SEGMENT);
		
		//SPA - 1 [ Product Code : SPA ]
		AccountsPTDGSpecification spaAccountsPTDGSpecification = createAccountsPTDGSpecification(1, EntityType.DEPOSIT_ACCOUNT, SPA_PRODUCT_CODE, 1000);
		spaAccountsPTDGSpecification.getManualExceptionPTDGSpecifications().add(createManualExceptionPTDGSpecification("20170101","20201231","F",MANUAL_EXCEPTION_FEE_ITEM_CODE,PERMANENT_WAIVER_REASON_CODE));
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(spaAccountsPTDGSpecification);
	    	
		//Deposit Account: eSavings - 5 [ Product Code : DEP ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(5, EntityType.DEPOSIT_ACCOUNT, DEPOSIT_PRODUCT_CODE, 1000));
	    
	    //IPS GIC - 14 [ Product Code : GIC ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(14, EntityType.TIME_DEPOSIT_ACCOUNT, TERM_DEPOSIT_PRODUCT_CODE, 1000));
	    
	    //Mutual Funds : 4 [ Product Code : MF ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(4, EntityType.MUTUAL_FUND_ACCOUNT, MUTUAL_FUND_PRODUCT_CODE, 1000));
	    
		return customerPTDGSpecification;
	}
	
	private static ManualExceptionPTDGSpecification createManualExceptionPTDGSpecification(String startDate, String endDate, String feeOrReward, String feeItemCode, String reason) {
		ManualExceptionPTDGSpecification manualExceptionPTDGSpecification = new ManualExceptionPTDGSpecification();
		manualExceptionPTDGSpecification.setEndDate(endDate);
		manualExceptionPTDGSpecification.setFeeItemCode(feeItemCode);
		manualExceptionPTDGSpecification.setFeeOrReward(feeOrReward);
		manualExceptionPTDGSpecification.setReason(reason);
		manualExceptionPTDGSpecification.setStartDate(startDate);
		return manualExceptionPTDGSpecification;
	}

	/**
	 * Customer Profile 2: 70K of Customer have one SPA Accounts have minimum balance of 6K (20%)
        * Accounts (24)
            * Deposit Account -  5 [ Product Code : DEP ]
            * IPS GIC - 14 [ Product Code : GIC ]
            * SPA - 1 [ Product Code : SPA ]
            * Mutual Funds : 4 [ Product Code : MF ]
	 */
	private static CustomerPTDGSpecification createCustomerPTDGSpecification2(long numberOfCustomers) {
		CustomerPTDGSpecification customerPTDGSpecification = createCustomerPTDGSpecification(numberOfCustomers, NON_WOOD_GUNDY_SEGMENT);
		
		//SPA - 1 [ Product Code : SPA ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(1, EntityType.DEPOSIT_ACCOUNT, SPA_PRODUCT_CODE, 7000));
	    
		//Deposit Account: eSavings - 5 [ Product Code : DEP ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(5, EntityType.DEPOSIT_ACCOUNT, DEPOSIT_PRODUCT_CODE, 1000));
	    
	    //IPS GIC - 14 [ Product Code : GIC ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(14, EntityType.TIME_DEPOSIT_ACCOUNT, TERM_DEPOSIT_PRODUCT_CODE, 1000));
	    
	    //Mutual Funds : 4 [ Product Code : MF ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(4, EntityType.MUTUAL_FUND_ACCOUNT, MUTUAL_FUND_PRODUCT_CODE, 1000));
	    
		return customerPTDGSpecification;
	}
	
	/**
	 * Customer Profile 3: 70K of Customer has a customer segment of ‘WoodGundy’ (20%)
	 	* Accounts (24)
            * Deposit Account -  5 [ Product Code : DEP ]
            * IPS GIC - 14 [ Product Code : GIC ]
            * SPA - 1 [ Product Code : SPA ]
            * Mutual Funds : 4 [ Product Code : MF ]
     	* Each of the accounts have 1K balance each
	 */
	private static CustomerPTDGSpecification createCustomerPTDGSpecification3(long numberOfCustomers) {
		CustomerPTDGSpecification customerPTDGSpecification = createCustomerPTDGSpecification(numberOfCustomers, WOOD_GUNDY_SEGMENT);
		
		//SPA - 1 [ Product Code : SPA ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(1, EntityType.DEPOSIT_ACCOUNT, SPA_PRODUCT_CODE, 1000));
	    
		//Deposit Account: eSavings - 5 [ Product Code : DEP ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(5, EntityType.DEPOSIT_ACCOUNT, DEPOSIT_PRODUCT_CODE, 1000));
	    
	    //IPS GIC - 14 [ Product Code : GIC ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(14, EntityType.TIME_DEPOSIT_ACCOUNT, TERM_DEPOSIT_PRODUCT_CODE, 1000));
	    
	    //Mutual Funds : 4 [ Product Code : MF ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(4, EntityType.MUTUAL_FUND_ACCOUNT, MUTUAL_FUND_PRODUCT_CODE, 1000));
	    
		return customerPTDGSpecification;
	}

	/**
	 * Customer Profile 4: 35K of Customers get 100K (10%)
         * Customer is not a Wood Gundy segment
         * Each of SPA accounts have less than 6K and no permanent waiver
         * Each of the other accounts have 5K balance
	     * Accounts (27)
            * Deposit Account -  5 [ Product Code : DEP ]
            * IPS GIC - 14 [ Product Code : GIC ]
            * SPA - 4 [ Product Code : SPA ]
            * Mutual Funds : 4 [ Product Code : MF
	 */
	private static CustomerPTDGSpecification createCustomerPTDGSpecification4(long numberOfCustomers) {
		CustomerPTDGSpecification customerPTDGSpecification = createCustomerPTDGSpecification(numberOfCustomers, NON_WOOD_GUNDY_SEGMENT);
		
		//4 SPA Accounts [ Product Code : SPA ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(4, EntityType.DEPOSIT_ACCOUNT, SPA_PRODUCT_CODE, 5000));
	    
		//5 eSavings [ Product Code : DEP ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(5, EntityType.DEPOSIT_ACCOUNT, DEPOSIT_PRODUCT_CODE, 5000));
	    
	    //14 GIC Accounts [ Product Code : GIC ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(14, EntityType.TIME_DEPOSIT_ACCOUNT, TERM_DEPOSIT_PRODUCT_CODE, 5000));
	    
	    //4 Mutual Fund Accounts [ Product Code : MF ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(4, EntityType.MUTUAL_FUND_ACCOUNT, MUTUAL_FUND_PRODUCT_CODE, 5000));
	    
		return customerPTDGSpecification;
	}
	
	/**
	 * Customer Profile 5: 35K of Customers gets grace period
        * Accounts (24)
            * Deposit Account -  5 [ Product Code : DEP ]
            * IPS GIC - 14 [ Product Code : GIC ]
            * SPA - 1 [ Product Code : SPA ]
            * Mutual Funds : 4 [ Product Code : MF ]
        * No permanent waivers, not wood gundy customers
        * Each of the accounts have 1K balance each
	 */
	private static CustomerPTDGSpecification createCustomerPTDGSpecification5(long numberOfCustomers) {
		CustomerPTDGSpecification customerPTDGSpecification = createCustomerPTDGSpecification(numberOfCustomers, NON_WOOD_GUNDY_SEGMENT);
		
		//SPA - 1 [ Product Code : SPA ] - Accounts open on 1st November
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(1, EntityType.DEPOSIT_ACCOUNT, SPA_PRODUCT_CODE, 1000, "20181101"));
	    
		//Deposit Account: eSavings - 5 [ Product Code : DEP ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(5, EntityType.DEPOSIT_ACCOUNT, DEPOSIT_PRODUCT_CODE, 1000));
	    
	    //IPS GIC - 14 [ Product Code : GIC ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(14, EntityType.TIME_DEPOSIT_ACCOUNT, TERM_DEPOSIT_PRODUCT_CODE, 1000));
	    
	    //Mutual Funds : 4 [ Product Code : MF ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(4, EntityType.MUTUAL_FUND_ACCOUNT, MUTUAL_FUND_PRODUCT_CODE, 1000));
	    
		return customerPTDGSpecification;
	}
	
	/**
	 * Customer Profile 6: 105K of Customers get no waiver
        * Accounts (24)
            * Deposit Account -  5 [ Product Code : DEP ]
            * IPS GIC - 14 [ Product Code : GIC ]
            * SPA - 1 [ Product Code : SPA ]
            * Mutual Funds : 4 [ Product Code : MF ]
        * No permanent waivers, not wood gundy customers
        * Each of the accounts have 1K balance each
	 */
	private static CustomerPTDGSpecification createCustomerPTDGSpecification6(long numberOfCustomers) {
		CustomerPTDGSpecification customerPTDGSpecification = createCustomerPTDGSpecification(numberOfCustomers, NON_WOOD_GUNDY_SEGMENT);
		
		//SPA - 1 [ Product Code : SPA ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(1, EntityType.DEPOSIT_ACCOUNT, SPA_PRODUCT_CODE, 1000));
	    
		//Deposit Account: eSavings - 5 [ Product Code : DEP ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(5, EntityType.DEPOSIT_ACCOUNT, DEPOSIT_PRODUCT_CODE, 1000));
	    
	    //IPS GIC - 14 [ Product Code : GIC ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(14, EntityType.TIME_DEPOSIT_ACCOUNT, TERM_DEPOSIT_PRODUCT_CODE, 1000));
	    
	    //Mutual Funds : 4 [ Product Code : MF ]
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(4, EntityType.MUTUAL_FUND_ACCOUNT, MUTUAL_FUND_PRODUCT_CODE, 1000));
	    
		return customerPTDGSpecification;
	}
	
	
}
