package com.zafin.rpe.test.tools.generator;

import java.util.ArrayList;
import java.util.List;

public class CitibanamexConfig {

	private static final long numberOfCustomers = 120L;
	
	public static PTDGSpecification getDataGenerationSpecification() {
		PTDGSpecification specification = new PTDGSpecification();
		specification.setCustomPTDGSpecification(getCustomPTDGSpecification(numberOfCustomers));
		return specification;
	}
	
	private static CustomPTDGSpecification getCustomPTDGSpecification(long numberOfCustomers) {
		CustomPTDGSpecification customPTDGSpecification = new CustomPTDGSpecification();
		//customPTDGSpecification.setCustomerPTDGSpecification(getCustomerPTDGSpecification());
		//customPTDGSpecification.setAccountsPTDGSpecifications(getAccountsPTDGSpecifications());
		//customPTDGSpecification.setTrasactionPTDGSpecifications(getTransactionsPTDGSpecifications());
		customPTDGSpecification.setBranchPTDGSpecifications(getBranchPTDGSpecifications());
		//customPTDGSpecification.setNumberOfCustomers(numberOfCustomers);
		return customPTDGSpecification;
	}
	
	private static List<BranchPTDGSpecification> getBranchPTDGSpecifications() {
		List<BranchPTDGSpecification> branchPTDGSpecifications = new ArrayList<>();
		branchPTDGSpecifications.add(createBranchPTDGSpecification("001","","MX"));
		branchPTDGSpecifications.add(createBranchPTDGSpecification("002","","MX"));
		branchPTDGSpecifications.add(createBranchPTDGSpecification("003","","MX"));
		return branchPTDGSpecifications;
	}
	
	private static BranchPTDGSpecification createBranchPTDGSpecification(String branchCode, String regionCode, String countryCode) {
		BranchPTDGSpecification branchPTDGSpecification = new BranchPTDGSpecification();
		branchPTDGSpecification.setBranchCode(branchCode);
		branchPTDGSpecification.setCountryCode(countryCode);
		branchPTDGSpecification.setRegionCode(regionCode);
		return branchPTDGSpecification;
	}

	private static List<TransactionsPTDGSpecification> getTransactionsPTDGSpecifications() {
		List<TransactionsPTDGSpecification> specifications = new ArrayList<>();
		specifications.add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"00660006001","WIRE-001","","","",10.12));
		specifications.add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"00660006001","CHQ-001","","","",11.23));
		specifications.add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"00660006001","POS","FPOS","","",12000.34));
		specifications.add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"00660006001","ATM","FATM","","",13.45));
		specifications.add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"00660006001","POS","LPOS","5541","WALMART",14000.56));
		specifications.add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"00660007001","WIRE-001","","","",10.12));
		specifications.add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"00660007001","CHQ-001","","","",11.23));
		specifications.add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"00660007001","POS","LPOS","5541","WALMART",14000.56));
		specifications.add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"00660008001","WIRE-001","","","",10.12));
		specifications.add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"00660008001","CHQ-001","","","",11.23));
		specifications.add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"00660008001","POS","LPOS","5541","WALMART",14000.56));
		specifications.add(createTransactionsPTDGSpecification(EntityType.CREDIT_CARD_TRANSACTION, 1,"121","100","","","",110.67));
		return specifications;
	}
	
	private static TransactionsPTDGSpecification createTransactionsPTDGSpecification(EntityType entityType, int numberOfTransactions, String productCode, 
			String transactionCode, String transactionChannel, String merchantCategoryCode, String merchantCode, double transactionValue) {
		TransactionsPTDGSpecification transactionsPTDGSpecification = new TransactionsPTDGSpecification();
		transactionsPTDGSpecification.setEntityType(entityType);
		transactionsPTDGSpecification.setNumberOfTransactions(numberOfTransactions);
		transactionsPTDGSpecification.setProductCode(productCode);
		transactionsPTDGSpecification.setTransactionCode(transactionCode);
		transactionsPTDGSpecification.setTransactionChannel(transactionChannel);
		transactionsPTDGSpecification.setMerchantCategoryCode(merchantCategoryCode);
		transactionsPTDGSpecification.setMerchantCode(merchantCode);
		transactionsPTDGSpecification.setTransactionValue(transactionValue);
		return transactionsPTDGSpecification;
	}
	
	private static AccountsPTDGSpecification createAccountsPTDGSpecification(int numberOfAccounts, EntityType entityType, String productCode, 
			double closingDayBalance, double averageBalance, String accountCohort, String accountOpenDate, boolean rent) {
		AccountsPTDGSpecification accountsPTDGSpecification = new AccountsPTDGSpecification();
		accountsPTDGSpecification.setAccountCohort(accountCohort);
		accountsPTDGSpecification.setAccountOpenDate(accountOpenDate);
		accountsPTDGSpecification.setAverageBalance(averageBalance);
		accountsPTDGSpecification.setClosingDayBalance(closingDayBalance);
		accountsPTDGSpecification.setProductCode(productCode);
		accountsPTDGSpecification.setEntityType(entityType);
		if(rent) {
			AccountFeaturePTDGSpecification accountFeaturePTDGSpecification = new AccountFeaturePTDGSpecification();
			accountFeaturePTDGSpecification.setFeatureCode("MF");
			accountFeaturePTDGSpecification.setOptionCode("RENT");
			accountFeaturePTDGSpecification.setActionCode("SELECT");
			List<AccountFeaturePTDGSpecification> featureOptions = new ArrayList<>();
			featureOptions.add(accountFeaturePTDGSpecification);
			accountsPTDGSpecification.setAccountFeatureSpecifications(featureOptions);
		}
		return accountsPTDGSpecification;
	}

	private static CustomerPTDGSpecification getCustomerPTDGSpecification() {
		CustomerPTDGSpecification customerPTDGSpecification = new CustomerPTDGSpecification();
		customerPTDGSpecification.setCustomerSegment("CDC");
		customerPTDGSpecification.setCustomerOpenDate("20170101");
		return customerPTDGSpecification;
	}
	
	
}
