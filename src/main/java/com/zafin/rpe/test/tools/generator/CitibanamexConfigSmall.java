package com.zafin.rpe.test.tools.generator;

import java.util.ArrayList;
import java.util.List;

public class CitibanamexConfigSmall {

	private static long multiplicationFactor = 1;
	
	public static PTDGSpecification getDataGenerationSpecification() {
		PTDGSpecification specification = new PTDGSpecification();
		specification.setCustomPTDGSpecification(getCustomPTDGSpecification());
		return specification;
	}
	
	private static CustomPTDGSpecification getCustomPTDGSpecification() {
		CustomPTDGSpecification customPTDGSpecification = new CustomPTDGSpecification();
		
		//Day Zero: 26 Feb 2018
	    DayEndSpecification dayEndSpecification = new DayEndSpecification("20180226", true, 0, 0);
		//50% of customers
	    dayEndSpecification.getCustomerPTDGSpecifications().add(createCustomerPTDGSpecification5(50*multiplicationFactor));//MODIFY
	    //20% of customers
	    dayEndSpecification.getCustomerPTDGSpecifications().add(createCustomerPTDGSpecification3(20*multiplicationFactor));//MODIFY
	    //30% of customers
	    dayEndSpecification.getCustomerPTDGSpecifications().add(createCustomerPTDGSpecification4(30*multiplicationFactor));//MODIFY
	    customPTDGSpecification.getDayEndSpecifications().add(dayEndSpecification);
	    
		//Normal Day: 27 Feb 2018
	    DayEndSpecification normalDayEndSpecification = new DayEndSpecification("20180227", false, 5*multiplicationFactor, 0);
	    normalDayEndSpecification.getCustomerPTDGSpecifications().add(createCustomerPTDGSpecification3(1*multiplicationFactor));//MODIFY
	    customPTDGSpecification.getDayEndSpecifications().add(normalDayEndSpecification);
		
		//Month End: 28 Feb 2018
	    DayEndSpecification monthEndSpecification = new DayEndSpecification("20180228", false, 5*multiplicationFactor, 0);
	    customPTDGSpecification.getDayEndSpecifications().add(monthEndSpecification);
	    
		customPTDGSpecification.setBranchPTDGSpecifications(getBranchPTDGSpecifications());
		
		return customPTDGSpecification;
	}
	
	private static List<BranchPTDGSpecification> getBranchPTDGSpecifications() {
		List<BranchPTDGSpecification> branchPTDGSpecifications = new ArrayList<>();
		branchPTDGSpecifications.add(createBranchPTDGSpecification("001","","MX"));
		branchPTDGSpecifications.add(createBranchPTDGSpecification("002","","MX"));
		branchPTDGSpecifications.add(createBranchPTDGSpecification("003","","MX"));
		return branchPTDGSpecifications;
	}
	
	private static BranchPTDGSpecification createBranchPTDGSpecification(String branchCode, String regionCode, String countryCode) {
		BranchPTDGSpecification branchPTDGSpecification = new BranchPTDGSpecification();
		branchPTDGSpecification.setBranchCode(branchCode);
		branchPTDGSpecification.setCountryCode(countryCode);
		branchPTDGSpecification.setRegionCode(regionCode);
		return branchPTDGSpecification;
	}

	private static TransactionsPTDGSpecification createTransactionsPTDGSpecification(EntityType entityType, int numberOfTransactions, String productCode, 
			String transactionCode, String transactionChannel, String merchantCategoryCode, String merchantCode, double transactionValue) {
		TransactionsPTDGSpecification transactionsPTDGSpecification = new TransactionsPTDGSpecification();
		transactionsPTDGSpecification.setEntityType(entityType);
		transactionsPTDGSpecification.setNumberOfTransactions(numberOfTransactions);
		transactionsPTDGSpecification.setProductCode(productCode);
		transactionsPTDGSpecification.setTransactionCode(transactionCode);
		transactionsPTDGSpecification.setTransactionChannel(transactionChannel);
		transactionsPTDGSpecification.setMerchantCategoryCode(merchantCategoryCode);
		transactionsPTDGSpecification.setMerchantCode(merchantCode);
		transactionsPTDGSpecification.setTransactionValue(transactionValue);
		return transactionsPTDGSpecification;
	}
	
	private static AccountsPTDGSpecification createAccountsPTDGSpecification(int numberOfAccounts, EntityType entityType, String productCode, 
			double closingDayBalance, double averageBalance, String accountCohort, String accountOpenDate, boolean rent, int percentage,
			String payrollAccount, String cycleEndDate, String customCategory) {
		AccountsPTDGSpecification accountsPTDGSpecification = new AccountsPTDGSpecification();
		accountsPTDGSpecification.setAccountCohort(accountCohort);
		accountsPTDGSpecification.setAccountOpenDate(accountOpenDate);
		accountsPTDGSpecification.setAverageBalance(averageBalance);
		accountsPTDGSpecification.setClosingDayBalance(closingDayBalance);
		accountsPTDGSpecification.setProductCode(productCode);
		accountsPTDGSpecification.setEntityType(entityType);
		accountsPTDGSpecification.setPayrollAccount(payrollAccount);
		accountsPTDGSpecification.setCycleEndDate(cycleEndDate);
		accountsPTDGSpecification.setCustomCategory(customCategory);
		if(rent) {
			AccountFeaturePTDGSpecification accountFeaturePTDGSpecification = new AccountFeaturePTDGSpecification();
			accountFeaturePTDGSpecification.setFeatureCode("MF");
			accountFeaturePTDGSpecification.setOptionCode("RENT");
			accountFeaturePTDGSpecification.setActionCode("SELECT");
			List<AccountFeaturePTDGSpecification> featureOptions = new ArrayList<>();
			featureOptions.add(accountFeaturePTDGSpecification);
			accountsPTDGSpecification.setAccountFeatureSpecifications(featureOptions);
		}
		return accountsPTDGSpecification;
	}
	
	//// Credit Card Account -> CITIBANK CLASSIC
	//customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(1, EntityType.CREDIT_CARD_ACCOUNT, "121", 0, 0, "", 
	//		"20180101", false, 50, "N", "20180228", ""));
	
	//Customers have one 'Perfiles Ejectivo' with maintenance and one 'Horizonte Plus' 
	private static CustomerPTDGSpecification createCustomerPTDGSpecification3(long numberOfCustomers) {
		CustomerPTDGSpecification customerPTDGSpecification = new CustomerPTDGSpecification();
		customerPTDGSpecification.setCustomerSegment("CDC");
		customerPTDGSpecification.setCustomerOpenDate("20170101");
		customerPTDGSpecification.setBranchCode("001");
		customerPTDGSpecification.setCustomerCohortCategoryCode("CT");
		customerPTDGSpecification.setCustomerCohortCode("GENERAL");
		customerPTDGSpecification.setCustomerStatus("ACTIVE");
		customerPTDGSpecification.setCustomerSegment("1");
		customerPTDGSpecification.setCustomerBirthDate("19820829");
		customerPTDGSpecification.setCustomerEmployeeStatus("");
		customerPTDGSpecification.setNumberOfCustomers(numberOfCustomers);
		
		//Deposit Account -> Perfiles Ejecutivo
		AccountsPTDGSpecification accountsPTDGSpecification1 = createAccountsPTDGSpecification(1, EntityType.DEPOSIT_ACCOUNT, "05000001001", 49700.12, 49700.12, 
				"ACC-03", "20180101", false, 100, "N", "20180205", "");
		accountsPTDGSpecification1.getTransactionPTDGSpecifications().add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"05000001001","POS","LPOS","5541","WALMART",14000.56));
		accountsPTDGSpecification1.getTransactionPTDGSpecifications().add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"05000001001","WIRE-001","ONLINE","","",340.00));
		accountsPTDGSpecification1.getTransactionPTDGSpecifications().add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"05000001001","CHQ-001","ONLINE","","",440.00));
		customerPTDGSpecification.getAccountsPTDGSpecifications().add(accountsPTDGSpecification1);
		
		// Call Deposit Account -> HORIZONTE PLUS
	    customerPTDGSpecification.getAccountsPTDGSpecifications().add(createAccountsPTDGSpecification(1, EntityType.DEPOSIT_ACCOUNT, "8400230001", 
	    		20301.44, 20301.44, "ACC-03", "20180101", false, 10, "N", "20180205", ""));

		return customerPTDGSpecification;
	}

	//Customers have only 'Perfiles Ejectivo' with maintenance
	private static CustomerPTDGSpecification createCustomerPTDGSpecification4(long numberOfCustomers) {
		CustomerPTDGSpecification customerPTDGSpecification = new CustomerPTDGSpecification();
		customerPTDGSpecification.setCustomerSegment("CDC");
		customerPTDGSpecification.setCustomerOpenDate("20170101");
		customerPTDGSpecification.setBranchCode("001");
		customerPTDGSpecification.setCustomerCohortCategoryCode("CT");
		customerPTDGSpecification.setCustomerCohortCode("GENERAL");
		customerPTDGSpecification.setCustomerStatus("ACTIVE");
		customerPTDGSpecification.setCustomerSegment("1");
		customerPTDGSpecification.setCustomerBirthDate("19820829");
		customerPTDGSpecification.setCustomerEmployeeStatus("");
		customerPTDGSpecification.setNumberOfCustomers(numberOfCustomers);
		
		//Deposit Account -> Perfiles Ejecutivo
		AccountsPTDGSpecification accountsPTDGSpecification1 = createAccountsPTDGSpecification(1, EntityType.DEPOSIT_ACCOUNT, "05000001001", 49700.12, 49700.12, 
				"ACC-03", "20180101", false, 100, "N", "20180205", "");
		accountsPTDGSpecification1.getTransactionPTDGSpecifications().add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"05000001001","POS","LPOS","5541","WALMART",14000.56));
		accountsPTDGSpecification1.getTransactionPTDGSpecifications().add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"05000001001","WIRE-001","ONLINE","","",340.00));
		accountsPTDGSpecification1.getTransactionPTDGSpecifications().add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"05000001001","CHQ-001","ONLINE","","",440.00));
		customerPTDGSpecification.getAccountsPTDGSpecifications().add(accountsPTDGSpecification1);
		
		return customerPTDGSpecification;
	}
	
	//Customers have only 'Perfiles Ejectivo' with rent
	private static CustomerPTDGSpecification createCustomerPTDGSpecification5(long numberOfCustomers) {
		CustomerPTDGSpecification customerPTDGSpecification = new CustomerPTDGSpecification();
		customerPTDGSpecification.setCustomerSegment("CDC");
		customerPTDGSpecification.setCustomerOpenDate("20170101");
		customerPTDGSpecification.setBranchCode("001");
		customerPTDGSpecification.setCustomerCohortCategoryCode("CT");
		customerPTDGSpecification.setCustomerCohortCode("GENERAL");
		customerPTDGSpecification.setCustomerStatus("ACTIVE");
		customerPTDGSpecification.setCustomerSegment("1");
		customerPTDGSpecification.setCustomerBirthDate("19820829");
		customerPTDGSpecification.setCustomerEmployeeStatus("EMPLOYEE");
		customerPTDGSpecification.setNumberOfCustomers(numberOfCustomers);
		
		//Deposit Account -> Perfiles Ejecutivo
		AccountsPTDGSpecification accountsPTDGSpecification1 = createAccountsPTDGSpecification(1, EntityType.DEPOSIT_ACCOUNT, "05000001001", 49700.12, 49700.12, 
				"ACC-03", "20180101", true, 100, "Y", "20180205", "");
		accountsPTDGSpecification1.getTransactionPTDGSpecifications().add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"05000001001","POS","LPOS","5541","WALMART",14000.56));
		accountsPTDGSpecification1.getTransactionPTDGSpecifications().add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"05000001001","WIRE-001","ONLINE","","",340.00));
		accountsPTDGSpecification1.getTransactionPTDGSpecifications().add(createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1,"05000001001","CHQ-001","ONLINE","","",440.00));
		customerPTDGSpecification.getAccountsPTDGSpecifications().add(accountsPTDGSpecification1);
		
		return customerPTDGSpecification;
	}
	
}
