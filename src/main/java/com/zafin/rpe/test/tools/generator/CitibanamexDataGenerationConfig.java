package com.zafin.rpe.test.tools.generator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * RPE-3722: data generation config for Citibanamex
 *
 */
public class CitibanamexDataGenerationConfig {

	private static final Logger logger = LoggerFactory.getLogger(CitibanamexDataGenerationConfig.class);

	private static long multiplicationFactor = 1;
	private static DecimalFormat df = new DecimalFormat("#.##");
	
	public static PTDGSpecification getDataGenerationSpecification() {
		PTDGSpecification specification = new PTDGSpecification();
		specification.setCustomPTDGSpecification(getCustomPTDGSpecification());
		return specification;
	}
	
	private static CustomPTDGSpecification getCustomPTDGSpecification() {
		CustomPTDGSpecification customPTDGSpecification = new CustomPTDGSpecification();
		
		//Day Zero: 01 Nov 2018
	    DayEndSpecification dayEndSpecification = new DayEndSpecification("20190915", true, 0, 0);
	    dayEndSpecification.getCustomerPTDGSpecifications().addAll(createRandomCustomerPTDGSpecification(2));//MODIFY
	    customPTDGSpecification.getDayEndSpecifications().add(dayEndSpecification);

		return customPTDGSpecification;
	}
	
	private static AccountsPTDGSpecification createAccountsPTDGSpecification(int numberOfAccounts, EntityType entityType,
																			 String productCode, double closingDayBalance, String accountOpenDate, String regionCode, AccountStatus accountStatus) {
		AccountsPTDGSpecification accountsPTDGSpecification = new AccountsPTDGSpecification();
		accountsPTDGSpecification.setNumberOfAccounts(numberOfAccounts);
		accountsPTDGSpecification.setAccountCohort("");
		accountsPTDGSpecification.setAccountOpenDate(accountOpenDate);
		accountsPTDGSpecification.setAverageBalance(0.0);
		accountsPTDGSpecification.setClosingDayBalance(closingDayBalance);
		accountsPTDGSpecification.setProductCode(productCode);
		accountsPTDGSpecification.setEntityType(entityType);
		accountsPTDGSpecification.setPayrollAccount(getRandomValue(Arrays.asList("Y", "N")));
		accountsPTDGSpecification.setCycleEndDate("");
		accountsPTDGSpecification.setCustomCategory("");
		accountsPTDGSpecification.setRegionCode(regionCode);
		accountsPTDGSpecification.setAccountStatus(accountStatus.toString());
		accountsPTDGSpecification.setInGoodStanding(getRandomValue(Arrays.asList("Y", "N")));

		return accountsPTDGSpecification;
	}
	
	private static AccountsPTDGSpecification createAccountsPTDGSpecification(int numberOfAccounts, EntityType entityType,
																			 String productCode, double closingDayBalance, String regionCode, AccountStatus accountStatus) {
		return createAccountsPTDGSpecification(numberOfAccounts, entityType, productCode, closingDayBalance, "20180101", regionCode, accountStatus);
	}
	
	private static CustomerPTDGSpecification createCustomerPTDGSpecification(long numberOfCustomers, List<String> customerSegments, List<String> customerCohorts, List<String> customerEmployeeStatusList) {
		CustomerPTDGSpecification customerPTDGSpecification = new CustomerPTDGSpecification();
		customerPTDGSpecification.setCustomerSegmentList(customerSegments);
		customerPTDGSpecification.setCustomerOpenDate("20170101");
		customerPTDGSpecification.setBranchCode("");
		customerPTDGSpecification.setCustomerCohortCategoryCode("");
		customerPTDGSpecification.setCustomerCohortList(customerCohorts);
		customerPTDGSpecification.setBankEmployeeStatusList(customerEmployeeStatusList);
		customerPTDGSpecification.setCustomerStatus("ACTIVE");
		customerPTDGSpecification.setCustomerBirthDate("19820829");
		customerPTDGSpecification.setNumberOfCustomers(numberOfCustomers);
		customerPTDGSpecification.setCustomerEmployeeStatus(getRandomValue(customerEmployeeStatusList));
		return customerPTDGSpecification;
	}
	
	private static TransactionsPTDGSpecification createTransactionsPTDGSpecification(EntityType entityType, int numberOfTransactions, String productCode,
																					 String transactionCode, String transactionChannel, String merchantCategoryCode, String merchantCode,
																					 double transactionValue, String merchantRegionCode, String merchantStateCode) {
		TransactionsPTDGSpecification transactionsPTDGSpecification = new TransactionsPTDGSpecification();
		transactionsPTDGSpecification.setEntityType(entityType);
		transactionsPTDGSpecification.setNumberOfTransactions(numberOfTransactions);
		transactionsPTDGSpecification.setProductCode(productCode);
		transactionsPTDGSpecification.setTransactionCode(transactionCode);
		transactionsPTDGSpecification.setTransactionChannel(transactionChannel);
		transactionsPTDGSpecification.setMerchantCategoryCode(merchantCategoryCode);
		transactionsPTDGSpecification.setMerchantCode(merchantCode);
		transactionsPTDGSpecification.setMerchantRegionCode(merchantRegionCode);
		transactionsPTDGSpecification.setMerchantStateCode(merchantStateCode);
		transactionsPTDGSpecification.setTransactionValue(transactionValue);
		return transactionsPTDGSpecification;
	}
	
	private static FeePTDGSpecification createFeePTDGSpecification(String feeCode, String feeName, 
			double feeAmount, double benefitAmount, String benefitCode, int numberOfFees) {
		FeePTDGSpecification feePTDGSpecification = new FeePTDGSpecification();
		feePTDGSpecification.setFeeCode(feeCode);
		feePTDGSpecification.setFeeName(feeName);
		feePTDGSpecification.setFeeAmount(feeAmount);
		feePTDGSpecification.setBenefitAmount(benefitAmount);
		feePTDGSpecification.setBenefitCode(benefitCode);
		feePTDGSpecification.setNumberOfFees(numberOfFees);
		return feePTDGSpecification;
	}
	
	
	/**
	 * Customer Profile 6: 105K of Customers get no waiver
        * Accounts (24)
            * Deposit Account -  5 [ Product Code : DEP ]
            * IPS GIC - 14 [ Product Code : GIC ]
            * SPA - 1 [ Product Code : SPA ]
            * Mutual Funds : 4 [ Product Code : MF ]
        * No permanent waivers, not wood gundy customers
        * Each of the accounts have 1K balance each
	 */
	private static List<CustomerPTDGSpecification> createRandomCustomerPTDGSpecification(int numberOfTransactionsPerAccount) {
		
		List<String> customerSegments = Arrays.asList(
				"1382", "1498", "0898", "0508", "0104", "1022", "0875", "1584", "5108", "5208", "0000", "0205", "0709",
				"1483", "0306", "0998", "1689", "9898", "0407", "0924", "1298", "1698", "1280", "0613", "0798", "0698",
				"1181", "0598", "5308", "1598", "1198", "4207", "1098", "1398");

        List<String> customerCohorts = Arrays.asList("1", "3", "11", "16", "7", "5", "15", "14", "4", "2");

		List<String> customerEmployeeStatusList = Arrays.asList("ACTIVE", "NOT_APPLICABLE");

		List<String> regions = populateRegions();

		List<CustomerCount> customerCountList = populateCustomerCount();
		List<String> channels = populateTransactionChannels();
		List<String> transactionCodes = populateTransactionCodes();

		List<String> merchantStateCodes = populateMerchantStateCodes();

		List<CustomerPTDGSpecification> customerSpecs = new ArrayList<>();

		List<String> productCodeList = getProductCodeList();

		for (CustomerCount customerCount : customerCountList) {
			int numOfCustomers = customerCount.customerCount;
			int numOfAccounts = customerCount.accountCount;

			CustomerPTDGSpecification customerPTDGSpecification = createCustomerPTDGSpecification(numOfCustomers, customerSegments, customerCohorts, customerEmployeeStatusList);
			customerSpecs.add(customerPTDGSpecification);

			List<AccountsPTDGSpecification> accountSpecs = new ArrayList<>();
			for (int i = 0; i < numOfAccounts; i++) {
				String regionCode = getRandomValue(regions);

				AccountStatus accountStatus = AccountStatus.ACTIVE;
				String productCode = getRandomValue(productCodeList);

				// FIXME: file format for credit cards seems to be invalid
//				EntityType accountType = (i + 1) < (numOfAccounts * 0.8) ? EntityType.DEPOSIT_ACCOUNT : EntityType.CREDIT_CARD_ACCOUNT;
				EntityType accountType = EntityType.DEPOSIT_ACCOUNT;

				AccountsPTDGSpecification accountsPTDGSpecification = createAccountsPTDGSpecification(1, accountType, productCode, 1000, regionCode, accountStatus);
				accountSpecs.add(accountsPTDGSpecification);

				//Transactions
				for (int j = 0; j < numberOfTransactionsPerAccount; j++) {
					String transactionCode = getRandomValue(transactionCodes);
					String merchantRegionCode = getRandomValue(regions);
					String txnChannel = getRandomValue(channels);
					double txnValue = ThreadLocalRandom.current().nextDouble(1, 10000);
					EntityType txType = accountType == EntityType.DEPOSIT_ACCOUNT ? EntityType.DEPOSIT_TRANSACTION : EntityType.CREDIT_CARD_TRANSACTION;
					accountsPTDGSpecification.getTransactionPTDGSpecifications().add(
							createTransactionsPTDGSpecification(txType, 1,
									productCode, transactionCode, txnChannel, "", "",
									Double.valueOf(df.format(txnValue)), merchantRegionCode, getRandomValue(merchantStateCodes)));
				}
			}

			customerPTDGSpecification.getAccountsPTDGSpecifications().addAll(accountSpecs);
		}

		return customerSpecs;
	}

	private static List<String> populateMerchantStateCodes() {
		return Arrays.asList("24", "13", "28", "12", "4", "03", "08", "5", "27", "21", "02", "3", "14", "05", "26",
				"01", "19", "07", "11", "1", "2", "04", "17", "30", "18", "06", "7", "16", "29", "9", "23", "8", "20",
				"10", "32", "25", "09", "15", "22", "6", "31");
	}

	private static List<String> populateRegions() {
		return Arrays.asList("28019", "13010", "06015", "30003", "21001", "03002", "03000", "12011", "01014", "20027",
				"21007", "13000", "20011", "12021", "04012", "09006", "16005", "01013", "12000", "03006", "32004",
				"09013", "24011", "06018", "21009", "20058", "17008", "14037", "01015", "25002", "15041", "22002",
				"19001", "31001", "25001", "20025", "12027", "06021", "20042", "30029", "26000", "15036", "18009",
				"28004", "21000", "15004", "11011", "15026", "15019", "32018", "00000", "20051", "14045", "12035",
				"04008", "25024", "30058", "12029", "11012", "01003", "08004", "17004", "24008", "25022", "26005",
				"05009", "06000", "14023", "21013", "11000", "01005", "18006", "03003", "11015", "30067", "32012",
				"17005", "12019", "14010", "02007", "19005", "08005", "15012", "12018", "14001", "04001", "10009",
				"09003", "24005", "30060", "14014", "11022", "04011", "20053", "30066", "30031", "20048", "10000",
				"08008", "19021", "01016", "28010", "29005", "24013", "25011", "08019", "24019", "10005", "20047",
				"05004", "32008", "16026", "04005", "13006", "11025", "12009", "12016", "05008", "26014", "11013",
				"30030", "19000", "08023", "14033", "14026", "02000", "02009", "05001", "30041", "14053", "17010",
				"04004", "14013", "28008", "05000", "10003", "28007", "13012", "20014", "15013", "30017", "18011",
				"28012", "31000", "30019", "28018", "28006", "19013", "20050", "19012", "29000", "14046", "27012",
				"11008", "13005", "16013", "14019", "15015", "08012", "14004", "11018", "06016", "09005", "14002",
				"15024", "30037", "12014", "24000", "29001", "13014", "19020", "25008", "30015", "09002", "32001",
				"30000", "18007", "11016", "09000", "30047", "18017", "02008", "19022", "14043", "22000", "13007",
				"17007", "24007", "12007", "20029", "04002", "02001", "20019", "25009", "20015", "11010", "30054",
				"03007", "30044", "30071", "25017", "15029", "30022", "14034", "15030", "11035", "14050", "15027",
				"11039", "30052", "18016", "31003", "15032", "09014", "11003", "32003", "25004", "06002", "08021",
				"30027", "28021", "28011", "20023", "19004", "11005", "18012", "29006", "12004", "20055", "04007",
				"30040", "14024", "13002", "21003", "13003", "32006", "20045", "30038", "01010", "01007", "09007",
				"15021", "11036", "01008", "16019", "23002", "30072", "30033", "19007", "20012", "18002", "26013",
				"08007", "19006", "26001", "20028", "17002", "14030", "06001", "22001", "11021", "13001", "30046",
				"32013", "05007", "16003", "15008", "15028", "14025", "11006", "14029", "06013", "19003", "25006",
				"20026", "18000", "20024", "27011", "20034", "30043", "19011", "16017", "14028", "15007", "30068",
				"14036", "20059", "10001", "20030", "27007", "30035", "32010", "27000", "14017", "30057", "14027",
				"11001", "09012", "14035", "30016", "27001", "16008", "11007", "06008", "02005", "16020", "14047",
				"27008", "12002", "11033", "15031", "14038", "26012", "11030", "15014", "24015", "12012", "08000",
				"11024", "30001", "14022", "30039", "10010", "15017", "17000", "11028", "27005", "15011", "15001",
				"30028", "19008", "30055", "15016", "11037", "14042", "28000", "28005", "08013", "32016", "30014",
				"16002", "08001", "07000", "30023", "15009", "30008", "20003", "08024", "21014", "12017", "28013",
				"20033", "27002", "25018", "12037", "20006", "26002", "24001", "24009", "14016", "30056", "01000",
				"30009", "22004", "16027", "12022", "01012", "06024", "31006", "30012", "14011", "11023", "06014",
				"05005", "16011", "20046", "06011", "16007", "32014", "24014", "11014", "16010", "21002", "08009",
				"11004", "14058", "15023", "23006", "05006", "32002", "25014", "20001", "06004", "32007", "10004",
				"10012", "30049", "20049", "28003", "26011", "12030", "31005", "26004", "27003", "09015", "32000",
				"06006", "18005", "13011", "06020", "24003", "13008", "16016", "28014", "14039", "02002", "07002",
				"20017", "15006", "14041", "18003", "08011", "23003", "16006", "18001", "31002", "19015", "06009",
				"12028", "30051", "11038", "25010", "15005", "21006", "02004", "18015", "30018", "14051", "06007",
				"20036", "30032", "25016", "08006", "16025", "28020", "05011", "04000", "08003", "12041", "12038",
				"23005", "01002", "30059", "20035", "08015", "28009", "26006", "11031", "30002", "12013", "14006",
				"21011", "04010", "12006", "25021", "09009", "17009", "20005", "16024", "06012", "15039", "11017",
				"30053", "20041", "14020", "26008", "05003", "24016", "15018", "13004", "14021", "06005", "25003",
				"16000", "12015", "06022", "04006", "14015", "12036", "29004", "16023", "30006", "12003", "30042",
				"12034", "25023", "20032", "24002", "30010", "16014", "20054", "30013", "14049", "12005", "20022",
				"18010", "11002", "30021", "21012", "32015", "11027", "01004", "30069", "19018", "20009", "17006",
				"19009", "03004", "10007", "24010", "18008", "18018", "25000", "08017", "07001", "25019", "29003",
				"27013", "15000", "14054", "03005", "12024", "13013", "24006", "16018", "30011", "08002", "12026",
				"14052", "19002", "19010", "15022", "13009", "04009", "06010", "20043", "20039", "13015", "12023",
				"25015", "30062", "12040", "30048", "09011", "30007", "17003", "14018", "12032", "23007", "20020",
				"25020", "20057", "26007", "25013", "21008", "06023", "30050", "11034", "08020", "15010", "01011",
				"12025", "23004", "20016", "21010", "20018", "10008", "18004", "06019", "28017", "27010", "20052",
				"32011", "24018", "24012", "14009", "32017", "14007", "20037", "16021", "12039", "14003", "20013",
				"15040", "16015", "25007", "20044", "12010", "06003", "20031", "20008", "16004", "18013", "02006",
				"10002", "14048", "18014", "26010", "24017", "14057", "32009", "12031", "23000", "01017", "30034",
				"20056", "02003", "08014", "30020", "20040", "26003", "29002", "30026", "09004", "06017", "03001",
				"11029", "07005", "30024", "30045", "11009", "32019", "11019", "26009", "25005", "30070", "20004",
				"12033", "27004", "01006", "14044", "08025", "21017", "12001", "11020", "09010", "28001", "21015",
				"07003", "21004", "14012", "14056", "15003", "15020", "14000", "14055", "24004", "19017", "30005",
				"31004", "14008", "20038", "10006", "23001", "08022", "09001", "20007", "11026", "05002", "27009",
				"08018", "16001", "21005", "20021", "15002", "04003", "28016", "14032", "10011", "08016", "08010",
				"12020", "07004", "30025", "25012", "14005", "30061", "01009", "17011", "21016", "14031", "20002",
				"28002", "20000", "16012", "16022", "30036", "28015", "32005", "17001", "24020", "30004", "23008",
				"16009", "11032");
	}

	private static List<String> populateTransactionChannels() {
		return Arrays.asList("15", "09", "14", "COMPMXECOM", "17", "BMX", "08", "CAT", "10", "12", "19", "00", "16",
				"22", "13", "ATMNOBMX", "MBK", "18", "BAT", "11", "BE_", "02", "SYS", "IVR", "01", "05", "5411", "ATM",
				"COMPMXPOS", "26", "COMPEXECOM", "DE5812", "ATMEXTR", "WKN", "04", "COMPEXPOS", "APP_", "07", "20", "06",
				"HBK", "21", "ATMBMX", "CI", "COR", "03", "27", "SABS_", "TEOS", "CLEARING_", "TELLER_");
	}

	private static class CustomerCount {
		private int customerCount;
		private int accountCount;

		public CustomerCount(int customerCount, int accountCount) {
			this.customerCount = customerCount;
			this.accountCount = accountCount;
		}

	}

    private static List<String> populateTransactionCodes() {
        return Arrays.asList("161935076", "14584010003", "CHQ-001", "14585010005", "100", "WIRE-001", "14588677779",
                "14583910004", "1619355910", "161935012", "161935009", "5800", "1619355800", "14585010006", "161935078",
                "5910", "161935015", "161935001", "5400", "1619355400", "161935145", "1619355408", "6855", "161935014",
                "14588677778", "161935203", "14588610001", "5497", "1619356855", "5408", "14584010002", "1619355497", "14588677777");
    }

	private static List<CustomerCount> populateCustomerCount() {
		List<CustomerCount> result = new ArrayList<>();

		result.add(new CustomerCount(10573815, 1));
		result.add(new CustomerCount(4969101, 2));
		result.add(new CustomerCount(1705484, 3));
		result.add(new CustomerCount(720216, 4));
		result.add(new CustomerCount(300117, 5));
		result.add(new CustomerCount(125657, 6));
		result.add(new CustomerCount(53731, 7));
		result.add(new CustomerCount(24427, 8));
		result.add(new CustomerCount(11833, 9));
		result.add(new CustomerCount(6098, 10));
		result.add(new CustomerCount(3553, 11));
		result.add(new CustomerCount(2212, 12));
		result.add(new CustomerCount(1620, 13));
		result.add(new CustomerCount(1295, 14));
		result.add(new CustomerCount(996, 15));
		result.add(new CustomerCount(893, 16));
		result.add(new CustomerCount(778, 17));
		result.add(new CustomerCount(679, 18));
		result.add(new CustomerCount(644, 19));
		result.add(new CustomerCount(557, 20));
		result.add(new CustomerCount(550, 21));
		result.add(new CustomerCount(541, 22));
		result.add(new CustomerCount(529, 23));
		result.add(new CustomerCount(496, 24));
		result.add(new CustomerCount(451, 25));
		result.add(new CustomerCount(429, 26));
		result.add(new CustomerCount(411, 27));
		result.add(new CustomerCount(370, 28));
		result.add(new CustomerCount(349, 29));
		result.add(new CustomerCount(291, 30));
		result.add(new CustomerCount(319, 31));
		result.add(new CustomerCount(299, 32));
		result.add(new CustomerCount(294, 33));
		result.add(new CustomerCount(290, 34));
		result.add(new CustomerCount(237, 35));
		result.add(new CustomerCount(251, 36));
		result.add(new CustomerCount(223, 37));
		result.add(new CustomerCount(249, 38));
		result.add(new CustomerCount(203, 39));
		result.add(new CustomerCount(204, 40));
		result.add(new CustomerCount(200, 41));
		result.add(new CustomerCount(177, 42));
		result.add(new CustomerCount(160, 43));
		result.add(new CustomerCount(128, 44));
		result.add(new CustomerCount(131, 45));
		result.add(new CustomerCount(105, 46));
		result.add(new CustomerCount(94, 47));
		result.add(new CustomerCount(106, 48));
		result.add(new CustomerCount(71, 49));
		result.add(new CustomerCount(79, 50));
		result.add(new CustomerCount(68, 51));
		result.add(new CustomerCount(72, 52));
		result.add(new CustomerCount(53, 53));
		result.add(new CustomerCount(47, 54));
		result.add(new CustomerCount(40, 55));
		result.add(new CustomerCount(48, 56));
		result.add(new CustomerCount(27, 57));
		result.add(new CustomerCount(35, 58));
		result.add(new CustomerCount(25, 59));
		result.add(new CustomerCount(25, 60));
		result.add(new CustomerCount(14, 61));
		result.add(new CustomerCount(18, 62));
		result.add(new CustomerCount(21, 63));
		result.add(new CustomerCount(14, 64));
		result.add(new CustomerCount(27, 65));
		result.add(new CustomerCount(23, 66));
		result.add(new CustomerCount(19, 67));
		result.add(new CustomerCount(10, 68));
		result.add(new CustomerCount(12, 69));
		result.add(new CustomerCount(10, 70));
		result.add(new CustomerCount(10, 71));
		result.add(new CustomerCount(9, 72));
		result.add(new CustomerCount(7, 73));
		result.add(new CustomerCount(11, 74));
		result.add(new CustomerCount(9, 75));
		result.add(new CustomerCount(11, 76));
		result.add(new CustomerCount(4, 77));
		result.add(new CustomerCount(9, 78));
		result.add(new CustomerCount(8, 79));
		result.add(new CustomerCount(6, 80));
		result.add(new CustomerCount(11, 81));
		result.add(new CustomerCount(6, 82));
		result.add(new CustomerCount(4, 83));
		result.add(new CustomerCount(5, 84));
		result.add(new CustomerCount(5, 85));
		result.add(new CustomerCount(3, 86));
		result.add(new CustomerCount(2, 87));
		result.add(new CustomerCount(5, 88));
		result.add(new CustomerCount(2, 89));
		result.add(new CustomerCount(4, 90));
		result.add(new CustomerCount(5, 91));
		result.add(new CustomerCount(2, 92));
		result.add(new CustomerCount(2, 93));
		result.add(new CustomerCount(3, 94));
		result.add(new CustomerCount(4, 95));
		result.add(new CustomerCount(2, 96));
		result.add(new CustomerCount(2, 97));
		result.add(new CustomerCount(1, 98));
		result.add(new CustomerCount(2, 101));
		result.add(new CustomerCount(5, 102));
		result.add(new CustomerCount(1, 104));
		result.add(new CustomerCount(1, 107));
		result.add(new CustomerCount(1, 108));
		result.add(new CustomerCount(3, 109));
		result.add(new CustomerCount(1, 112));
		result.add(new CustomerCount(1, 114));
		result.add(new CustomerCount(1, 116));
		result.add(new CustomerCount(2, 118));
		result.add(new CustomerCount(3, 119));
		result.add(new CustomerCount(1, 122));
		result.add(new CustomerCount(1, 124));
		result.add(new CustomerCount(2, 125));
		result.add(new CustomerCount(1, 127));
		result.add(new CustomerCount(1, 136));
		result.add(new CustomerCount(1, 137));
		result.add(new CustomerCount(2, 138));
		result.add(new CustomerCount(2, 140));
		result.add(new CustomerCount(1, 142));
		result.add(new CustomerCount(2, 143));
		result.add(new CustomerCount(2, 147));
		result.add(new CustomerCount(2, 148));
		result.add(new CustomerCount(1, 149));
		result.add(new CustomerCount(2, 150));
		result.add(new CustomerCount(1, 151));
		result.add(new CustomerCount(1, 152));
		result.add(new CustomerCount(1, 155));
		result.add(new CustomerCount(2, 156));
		result.add(new CustomerCount(1, 157));
		result.add(new CustomerCount(1, 159));
		result.add(new CustomerCount(1, 161));
		result.add(new CustomerCount(1, 169));
		result.add(new CustomerCount(1, 173));
		result.add(new CustomerCount(1, 179));
		result.add(new CustomerCount(1, 184));
		result.add(new CustomerCount(1, 185));
		result.add(new CustomerCount(2, 198));
		result.add(new CustomerCount(1, 213));
		result.add(new CustomerCount(1, 234));
		result.add(new CustomerCount(1, 242));
		result.add(new CustomerCount(1, 287));

		return result;
	}

	private static List<String> getProductCodeList() {
		return Arrays.asList(
				"000100010001", "000100030001", "000100110001", "000100120001", "000100130001", "006600060001",
				"006600070001", "006600080001", "006600090001", "006600120001", "006600140001", "006600160001",
				"006600260001", "006600270001", "006600280001", "008400120001", "008400130001", "008400140001",
				"008400160001", "008400180001", "008400190001", "008400220001", "008400230001", "008400240001",
				"008400250001", "008400260001", "008700020001", "008700040001", "008700060001", "008700070001",
				"008700080001", "008700090001", "050000010001", "110", "120", "121", "130", "131", "133", "210",
				"211", "220", "221", "222", "310", "410", "420", "430", "510", "520", "530", "620", "640", "810",
				"820", "850", "860", "881137", "882203", "882272", "882279", "882290", "910", "911", "912", "913",
				"914", "915", "916", "920", "941", "950", "954", "955", "956", "959", "973", "980", "9902FUND"
				);
	}

	private static String getRandomValue(List<String> valueSet) {
		Random rand = new Random();
		return valueSet.get(rand.nextInt(valueSet.size()));
	}

	private enum  AccountStatus {
		ACTIVE, CLOSED;
	}

}
