package com.zafin.rpe.test.tools.generator;

import java.util.ArrayList;
import java.util.List;

public class CustomPTDGSpecification {

	private List<DayEndSpecification> dayEndSpecifications = new ArrayList<>();
	private List<BranchPTDGSpecification> branchPTDGSpecifications = new ArrayList<>();
	
	public List<BranchPTDGSpecification> getBranchPTDGSpecifications() {
		return branchPTDGSpecifications;
	}
	public void setBranchPTDGSpecifications(List<BranchPTDGSpecification> branchPTDGSpecifications) {
		this.branchPTDGSpecifications = branchPTDGSpecifications;
	}
	public List<DayEndSpecification> getDayEndSpecifications() {
		return dayEndSpecifications;
	}
	public void setDayEndSpecifications(List<DayEndSpecification> dayEndSpecifications) {
		this.dayEndSpecifications = dayEndSpecifications;
	}
	
}
