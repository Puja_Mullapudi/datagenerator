package com.zafin.rpe.test.tools.generator;

import java.util.ArrayList;
import java.util.List;

public class CustomerAccountData {

	private List<CustomerData> customerDataList = new ArrayList<>();
	private List<AccountData> accountDataList = new ArrayList<>();
	private List<AccountRelationshipData> accountRelationshipDataList = new ArrayList<>();
	
	public List<CustomerData> getCustomerDataList() {
		return customerDataList;
	}
	public List<AccountData> getAccountDataList() {
		return accountDataList;
	}
	public List<AccountRelationshipData> getAccountRelationshipDataList() {
		return accountRelationshipDataList;
	}
    public void setCustomerDataList(List<CustomerData> customerDataList) {
        this.customerDataList = customerDataList;
    }
    public void setAccountDataList(List<AccountData> accountDataList) {
        this.accountDataList = accountDataList;
    }
    public void setAccountRelationshipDataList(List<AccountRelationshipData> accountRelationshipDataList) {
        this.accountRelationshipDataList = accountRelationshipDataList;
    }
	
}
