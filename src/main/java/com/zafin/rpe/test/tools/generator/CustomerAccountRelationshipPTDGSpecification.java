package com.zafin.rpe.test.tools.generator;

public class CustomerAccountRelationshipPTDGSpecification {

	private String customerProfileName;
	private String accountProfileName;
	private String role;
	
	public CustomerAccountRelationshipPTDGSpecification(String customerProfileName, String accountProfileName, String role) {
		this.customerProfileName = customerProfileName;
		this.accountProfileName = accountProfileName;
		this.role = role;
	}
	
	public String getCustomerProfileName() {
		return customerProfileName;
	}
	
	public String getAccountProfileName() {
		return accountProfileName;
	}

	public String getRole() {
		return role;
	}
	
	
}
