package com.zafin.rpe.test.tools.generator;

import java.util.ArrayList;
import java.util.List;

public class CustomerData {

	private String customerProfileName;
	private String customerId;
	private boolean isCustomerMerged = false;
	
	public boolean isCustomerMerged() {
        return isCustomerMerged;
    }
    public void setCustomerMerged(boolean isCustomerMerged) {
        this.isCustomerMerged = isCustomerMerged;
    }

    private String customerSegment;
	private CustomerPTDGSpecification customerPTDGSpecification;

	private String customerCohort;

	private String customerEmployeeStatus;
	//private List<AccountData> accountDataList = new ArrayList<>();
	//private List<AccountRelationshipData> accountRelationshipDataList = new ArrayList<>();
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/*public List<AccountData> getAccountDataList() {
		return accountDataList;
	}*/
	public CustomerPTDGSpecification getCustomerPTDGSpecification() {
		return customerPTDGSpecification;
	}
	public void setCustomerPTDGSpecification(CustomerPTDGSpecification customerPTDGSpecification) {
		this.customerPTDGSpecification = customerPTDGSpecification;
	}
	public String getCustomerProfileName() {
		return customerProfileName;
	}
	public void setCustomerProfileName(String customerProfileName) {
		this.customerProfileName = customerProfileName;
	}
	/*public List<AccountRelationshipData> getAccountRelationshipDataList() {
		return accountRelationshipDataList;
	}*/
	public String getCustomerSegment() {
		return customerSegment;
	}
	public void setCustomerSegment(String customerSegment) {
		this.customerSegment = customerSegment;
	}

	public String getCustomerCohort() {
		return customerCohort;
	}

	public void setCustomerCohort(String customerCohort) {
		this.customerCohort = customerCohort;
	}

	public String getCustomerEmployeeStatus() {
		return customerEmployeeStatus;
	}

	public void setCustomerEmployeeStatus(String customerEmployeeStatus) {
		this.customerEmployeeStatus = customerEmployeeStatus;
	}

}
