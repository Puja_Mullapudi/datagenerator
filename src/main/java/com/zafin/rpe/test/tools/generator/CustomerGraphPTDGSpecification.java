package com.zafin.rpe.test.tools.generator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CustomerGraphPTDGSpecification {

	private long numberOfCustomerGraphs;
	private List<CustomerPTDGSpecification> customerProfiles = new ArrayList<>();
	private List<AccountsPTDGSpecification> accountProfiles = new ArrayList<>();
	private List<CustomerAccountRelationshipPTDGSpecification> customerAccountRelationships = new ArrayList<>();
	
	public List<CustomerAccountRelationshipPTDGSpecification> getCustomerAccountRelationships() {
		return customerAccountRelationships;
	}
	public void setCustomerAccountRelationships(List<CustomerAccountRelationshipPTDGSpecification> customerAccountRelationships) {
		this.customerAccountRelationships = customerAccountRelationships;
	}
	public void addCustomerAccountRelationship(String customerProfileName, String accountProfileName, String role) {
		customerAccountRelationships.add(new CustomerAccountRelationshipPTDGSpecification(customerProfileName, accountProfileName, role));
	}
	public long getNumberOfCustomerGraphs() {
		return numberOfCustomerGraphs;
	}
	public void setNumberOfCustomerGraphs(long numberOfCustomerGraphs) {
		this.numberOfCustomerGraphs = numberOfCustomerGraphs;
	}
	public List<CustomerPTDGSpecification> getCustomerProfiles() {
		return customerProfiles;
	}
	public List<AccountsPTDGSpecification> getAccountProfiles() {
		return accountProfiles;
	}
	
}
