package com.zafin.rpe.test.tools.generator;

/**
 * 
 * The customer id max of '999999999'. Nine digits. 
 *
 */
public class CustomerIdRange {

	private String startId;
	private String endId;
	
	public String getStartId() {
		return startId;
	}
	public void setStartId(String startId) {
		this.startId = startId;
	}
	public String getEndId() {
		return endId;
	}
	public void setEndId(String endId) {
		this.endId = endId;
	}
	
	
	
}
