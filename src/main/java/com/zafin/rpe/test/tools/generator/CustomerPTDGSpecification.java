package com.zafin.rpe.test.tools.generator;

import java.util.ArrayList;
import java.util.List;

public class CustomerPTDGSpecification {

	private long numberOfCustomers;
	private String customerSegment;
	private String customerOpenDate;
	private String customerCohortCategoryCode;
	private String customerCohortCode;
	private String customerStatus;
	private String customerBirthDate;
	private String branchCode;
	private String customerEmployeeStatus = "";
	private String customerProfileName;
	private List<AccountsPTDGSpecification> accountsPTDGSpecifications = new ArrayList<>();
	
	private List<String> customerSegmentList = new ArrayList<String>();

	private List<String> customerCohortList = new ArrayList<>();
	private List<String> customerEmployeeStatusList;

	public String getCustomerSegment() {
		return customerSegment;
	}
	public void setCustomerSegment(String customerSegment) {
		this.customerSegment = customerSegment;
	}
	public String getCustomerOpenDate() {
		return customerOpenDate;
	}
	public void setCustomerOpenDate(String customerOpenDate) {
		this.customerOpenDate = customerOpenDate;
	}
	public List<AccountsPTDGSpecification> getAccountsPTDGSpecifications() {
		return accountsPTDGSpecifications;
	}
	public void setAccountsPTDGSpecifications(List<AccountsPTDGSpecification> accountsPTDGSpecifications) {
		this.accountsPTDGSpecifications = accountsPTDGSpecifications;
	}
	public long getNumberOfCustomers() {
		return numberOfCustomers;
	}
	public void setNumberOfCustomers(long numberOfCustomers) {
		this.numberOfCustomers = numberOfCustomers;
	}
	public String getCustomerCohortCategoryCode() {
		return customerCohortCategoryCode;
	}
	public void setCustomerCohortCategoryCode(String customerCohortCategoryCode) {
		this.customerCohortCategoryCode = customerCohortCategoryCode;
	}
	public String getCustomerCohortCode() {
		return customerCohortCode;
	}
	public void setCustomerCohortCode(String customerCohortCode) {
		this.customerCohortCode = customerCohortCode;
	}
	public String getCustomerStatus() {
		return customerStatus;
	}
	public void setCustomerStatus(String customerStatus) {
		this.customerStatus = customerStatus;
	}
	public String getCustomerBirthDate() {
		return customerBirthDate;
	}
	public void setCustomerBirthDate(String customerBirthDate) {
		this.customerBirthDate = customerBirthDate;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getCustomerEmployeeStatus() {
		return customerEmployeeStatus;
	}
	public void setCustomerEmployeeStatus(String customerEmployeeStatus) {
		this.customerEmployeeStatus = customerEmployeeStatus;
	}
	public String getCustomerProfileName() {
		return customerProfileName;
	}
	public void setCustomerProfileName(String customerProfileName) {
		this.customerProfileName = customerProfileName;
	}
	public List<String> getCustomerSegmentList() {
		return customerSegmentList;
	}
	public void setCustomerSegmentList(List<String> customerSegmentList) {
		this.customerSegmentList = customerSegmentList;
	}

	public List<String> getCustomerCohortList() {
		return customerCohortList;
	}

	public void setCustomerCohortList(List<String> customerCohortList) {
		this.customerCohortList = customerCohortList;
	}

	public void setBankEmployeeStatusList(List<String> customerEmployeeStatusList) {
		this.customerEmployeeStatusList = customerEmployeeStatusList;
	}

	public List<String> getCustomerEmployeeStatusList() {
		return customerEmployeeStatusList;
	}

}
