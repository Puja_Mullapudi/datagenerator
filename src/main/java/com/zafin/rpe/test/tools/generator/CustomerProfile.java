package com.zafin.rpe.test.tools.generator;

import java.util.ArrayList;
import java.util.List;

public class CustomerProfile {

	private List<AccountsPTDGSpecification> accountSpecifications = new ArrayList<>();

	public List<AccountsPTDGSpecification> getAccountSpecifications() {
		return accountSpecifications;
	}

	public void setAccountSpecifications(List<AccountsPTDGSpecification> accountSpecifications) {
		this.accountSpecifications = accountSpecifications;
	}
	
}
