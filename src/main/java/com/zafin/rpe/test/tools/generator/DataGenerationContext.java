package com.zafin.rpe.test.tools.generator;

public class DataGenerationContext {

	private static long lastCustomerId = 1;
	private static long lastCustomerGraphId = 1;
	private static long lastAccountId = 1;
	private static long totalCustomersGenerated = 0;
	private static long totalCustomersGraphsGenerated = 0;
	private static long totalDepositAccountsGenerated = 0;
	private static long totalCreditCardAccountsGenerated = 0;
	private static long totalTimeDepositAccountsGenerated = 0;
	private static long totalMutualFundAccountsGenerated = 0;
	private static long totalMortgageAccountsGenerated = 0;
	private static long totalSecuritiesAccountGenerated=0;
	private static long totalLoanAccountsGenerated = 0;
	private static long totalAccountsGenerated = 0;
	private static long totalDepositTransactionsGenerated = 0;
	private static long totalCreditCardTransactionsGenerated = 0;
	private static long totalLoanTransactionsGenerated = 0;
	private static long totalBranchesGenerated = 0;
	private static long totalAccountFeaturesGenerated = 0;
	private static long totalManualExceptionsGenerated = 0;
	private static long totalCustomerAccountRelationshipsGenerated = 0;
	private static long totalFeesGenerated = 0;
	private static long lastcustomerServiceContractId = 1;
	private static long totalcustomerServiceContractId = 0;
	private static long lastAccountServiceSubscribeId=1;
	private static long totalAccountServiceSubscribeId=0;
	private static long lastpackageSubscribeId=1;
	private static long totalpackageSubscribeGenerated=0;
	private static long lastBillingGroupId=1;
	private static long totalBillingGroupId=0;
	
	public static long incrementBillingGroupId() {
	    totalBillingGroupId++;
        return lastBillingGroupId++;
    }
	
	public static long getTotalBillingGroupId() {
        return totalBillingGroupId;
    }
	
	public static long getTotalpackageSubscribeGenerated() {
        return totalpackageSubscribeGenerated;
    }
	
	public static long incrementTotalpackageSubscribeGenerated() {
	    totalpackageSubscribeGenerated++;
	    return lastpackageSubscribeId++;
	}

    public static long incrementAccountServiceSubscribeId() {
	    
	    totalAccountServiceSubscribeId++;
	    return lastAccountServiceSubscribeId++;
	}

    public static long getTotalAccountServiceSubscribeId() {
        return totalAccountServiceSubscribeId;
    }

    public static long getTotalFeesGenerated() {
		return totalFeesGenerated;
	}

	public static long incrementFeesGenerated() {
		return totalFeesGenerated++;
	}
	
	public static long incrementCustomerId() {
		totalCustomersGenerated++;
		return lastCustomerGraphId++;
	}
	
	public static long incrementCustomerGraphId() {
		totalCustomersGraphsGenerated++;
		return lastCustomerId++;
	}
	
	public static long getTotalCustomersGraphsGenerated() {
		return totalCustomersGraphsGenerated;
	}
	
	public static long getTotalCustomersGenerated() {
		return totalCustomersGenerated;
	}
	
	public static long incrementAccountId(EntityType entityType) {
		if(entityType == EntityType.DEPOSIT_ACCOUNT) {
			totalDepositAccountsGenerated++;
		} else if(entityType == EntityType.TIME_DEPOSIT_ACCOUNT) {
			totalTimeDepositAccountsGenerated++;
		} else if(entityType == EntityType.CREDIT_CARD_ACCOUNT) {
			totalCreditCardAccountsGenerated++;
		} else if(entityType == EntityType.MORTGAGE_ACCOUNT) {
			totalMortgageAccountsGenerated++;
		} else if(entityType == EntityType.MUTUAL_FUND_ACCOUNT) {
			totalMutualFundAccountsGenerated++;
		} else if(entityType == EntityType.LOAN_ACCOUNT) {
			totalLoanAccountsGenerated++;
		} else if(entityType == EntityType.SECURITIES_ACCOUNT) {
		    totalSecuritiesAccountGenerated++;
		}
		totalAccountsGenerated++;
		return lastAccountId++;
	}
	
	public static long getTotalSecuritiesAccountGenerated() {
        return totalSecuritiesAccountGenerated;
    }

    public static void incrementDepositTransactionsGenerated() {
		totalDepositTransactionsGenerated++;
	}
	
	public static void incrementCreditCardTransactionsGenerated() {
		totalCreditCardTransactionsGenerated++;
	}
	
	public static void incrementLoanTransactionsGenerated() {
		totalLoanTransactionsGenerated++;
	}
	
	public static void incrementBranchesGenerated() {
		totalBranchesGenerated++;
	}
	
	public static void incrementAccountFeaturesGenerated() {
		totalAccountFeaturesGenerated++;
	}
	
	public static void incrementManualExceptionsGenerated() {
		totalManualExceptionsGenerated++;
	}
	
	public static long incrementcustomerServiceContractId() {
	    totalcustomerServiceContractId++;
	    return lastcustomerServiceContractId++;
	}
	
	public static long getLastAccountId() {
		return lastAccountId;
	}

	public static long getTotalDepositAccountsGenerated() {
		return totalDepositAccountsGenerated;
	}

	public static long getTotalCreditCardAccountsGenerated() {
		return totalCreditCardAccountsGenerated;
	}

	public static long getTotalRelationshipsGenerated() {
		return totalAccountsGenerated;
	}
	
	public static long getTotalAccountsGenerated() {
		return totalAccountsGenerated;
	}

	public static long getTotalTimeDepositAccountsGenerated() {
		return totalTimeDepositAccountsGenerated;
	}

	public static long getTotalMutualFundAccountsGenerated() {
		return totalMutualFundAccountsGenerated;
	}

	public static long getTotalMortgageAccountsGenerated() {
		return totalMortgageAccountsGenerated;
	}

	public static long getTotalLoanAccountsGenerated() {
		return totalLoanAccountsGenerated;
	}

	public static long getTotalDepositTransactionsGenerated() {
		return totalDepositTransactionsGenerated;
	}

	public static long getTotalCreditCardTransactionsGenerated() {
		return totalCreditCardTransactionsGenerated;
	}

	public static long getTotalLoanTransactionsGenerated() {
		return totalLoanTransactionsGenerated;
	}

	public static long getTotalBranchesGenerated() {
		return totalBranchesGenerated;
	}

	public static long getTotalAccountFeaturesGenerated() {
		return totalAccountFeaturesGenerated;
	}

	public static long getTotalManualExceptionsGenerated() {
		return totalManualExceptionsGenerated;
	}
	
	public static void incrementCustomerAccountRelationshipsGenerated() {
		totalCustomerAccountRelationshipsGenerated++;
	}
	
	public static long getTotalCustomerAccountRelationshipsGenerated() {
		return totalCustomerAccountRelationshipsGenerated;
	}
	
	public static long getTotalCustomerServiceContractId() {
	    return totalcustomerServiceContractId;
	}
	
}