package com.zafin.rpe.test.tools.generator;

public class DayEndFileLineCounts {

	private long totalCustomersGenerated = 0;
	private long totalDepositAccountsGenerated = 0;
	private long totalCreditCardAccountsGenerated = 0;
	private long totalTimeDepositAccountsGenerated = 0;
	private long totalMutualFundAccountsGenerated = 0;
	private long totalMortgageAccountsGenerated = 0;
	private long totalSecuritiesAccountGenerated=0;
    private long totalLoanAccountsGenerated = 0;
	private long totalCustomerAccountRelationshipsGenerated = 0;
	private long totalAccountsGenerated = 0;
	private long totalDepositTransactionsGenerated = 0;
	private long totalCreditCardTransactionsGenerated = 0;
	private long totalLoanTransactionsGenerated = 0;
	private long totalBranchesGenerated = 0;
	private long totalAccountFeaturesGenerated = 0;
	private long totalManualExceptionsGenerated = 0;
	private long numberOfAccountsHavingCycleEnd = 0;
	private long numberOfAccountsHavingBalanceUpdates = 0;
	private long totalcustomerServiceContractId =0;
	private long totalCustomerMergeGenerated=0;
	private long totalAccountServiceSubscribeGenerated = 0;
	private long totalAccountConsentGenerated = 0;
	private long totalpackageSubscribeGenerated=0;
	private long totalpackageAccountInfoGenerated=0;
	private long totalAccountIdentifierChangeGenerated=0;
	private long totalOfferEnrollmentGenerated=0;
	private long totalFeeExclusionGenerated=0;
	private long totalOptionSelecterGenerated=0;
	private long totalExternalFeeGenerated=0;
	private long totalbillingAccountGroupGenerated=0;
	private long totalbillingAccountGroupAccountGenerated=0;
	private long totalAccountBalancesGenerated=0;
	
    public long incrementTotalAccountBalancesGenerated() {
        return totalAccountBalancesGenerated++;
    }
	public long getTotalAccountBalancesGenerated() {
        return totalAccountBalancesGenerated;
    }
    public long getTotalSecuritiesAccountGenerated() {
         return totalSecuritiesAccountGenerated;
        
    }
	public void incrementSecuritiesAccountGenerated(int increment) {
	    totalSecuritiesAccountGenerated+=increment;
        totalAccountsGenerated+=increment;
	}
	public long incrementTotalFeeExclusionGenerated() {
        return totalFeeExclusionGenerated++;
    }
    
    public long incrementTotalOptionSelecterGenerated() {
        return totalOptionSelecterGenerated++;
    }

    public long incrementTotalExternalFeeGenerated() {
        return totalExternalFeeGenerated++;
    }

    public long incrementTotalbillingAccountGroupGenerated() {
        return totalbillingAccountGroupGenerated++;
    }

    public long incrementTotalbillingAccountGroupAccountGenerated() {
        return totalbillingAccountGroupAccountGenerated++;
    }
	
	
	public long getTotalFeeExclusionGenerated() {
        return totalFeeExclusionGenerated;
    }

    public long getTotalOptionSelecterGenerated() {
        return totalOptionSelecterGenerated;
    }

    public long getTotalExternalFeeGenerated() {
        return totalExternalFeeGenerated;
    }

    public long getTotalbillingAccountGroupGenerated() {
        return totalbillingAccountGroupGenerated;
    }

    public long getTotalbillingAccountGroupAccountGenerated() {
        return totalbillingAccountGroupAccountGenerated;
    }

    public long getTotalOfferEnrollmentGenerated() {
        return totalOfferEnrollmentGenerated;
    }
	
	public void incrementTotalOfferEnrollmentGenerated() {
	    totalOfferEnrollmentGenerated++;
	}
    public void incrementalAccountIdentifierChangeGenerated() {
	    totalAccountIdentifierChangeGenerated++;
	}
	public long getTotalpackageSubscribeGenerated() {
        return totalpackageSubscribeGenerated;
    }

    public long getTotalAccountIdentifierChangeGenerated() {
        return totalAccountIdentifierChangeGenerated;
    }

    public long getTotalpackageAccountInfoGenerated() {
        return totalpackageAccountInfoGenerated;
    }
    
    public void incrementTotalpackageSubscribeGenerated() {
        totalpackageSubscribeGenerated++;
    }
    
    public void incrementTotalpackageAccountInfoGenerated() {
        totalpackageAccountInfoGenerated++;
    }

    public void incrementCustomers(int increment) {
		totalCustomersGenerated += increment;
	}
	
	public void incrementDepositAccounts(int increment) {
		totalDepositAccountsGenerated += increment;
		totalAccountsGenerated += increment;
	}
	
	public void incrementTimeDepositAccounts(int increment) {
		totalTimeDepositAccountsGenerated += increment;
		totalAccountsGenerated += increment;
	}
	
	public void incrementCreditCardAccounts(int increment) {
		totalCreditCardAccountsGenerated += increment;
		totalAccountsGenerated += increment;
	}
	
	public void incrementMortgageAccounts(int increment) {
		totalMortgageAccountsGenerated += increment;
		totalAccountsGenerated += increment;
	}
	
	public void incrementMutualFundAccounts(int increment) {
		totalMutualFundAccountsGenerated += increment;
		totalAccountsGenerated += increment;
	}
	
	public void incrementLoanAccounts(int increment) {
	    totalLoanAccountsGenerated += increment;
		totalAccountsGenerated += increment;
	}
	
	public long getTotalCustomersGenerated() {
		return totalCustomersGenerated;
	}
	
	public void incrementDepositTransactionsGenerated() {
		totalDepositTransactionsGenerated ++;
	}
	
	public void incrementCreditCardTransactionsGenerated() {
		totalCreditCardTransactionsGenerated ++;
	}
	
	public void incrementLoanTransactionsGenerated() {
		totalLoanTransactionsGenerated ++;
	}
	
	public void incrementBranchesGenerated(int increment) {
		totalBranchesGenerated += increment;
	}
	
	public void incrementAccountFeaturesGenerated(int increment) {
		totalAccountFeaturesGenerated += increment;
	}
	
	public void incrementAccountFeaturesGenerated() {
		totalAccountFeaturesGenerated ++;
	}
	
	public void incrementTotalcustomerServiceContractId() {
	    totalcustomerServiceContractId++;
	}
	
	public long getTotalDepositAccountsGenerated() {
		return totalDepositAccountsGenerated;
	}

	public long getTotalCreditCardAccountsGenerated() {
		return totalCreditCardAccountsGenerated;
	}

	public long getTotalRelationshipsGenerated() {
		return totalAccountsGenerated;
	}
	
	public long getTotalAccountsGenerated() {
		return totalAccountsGenerated;
	}

	public long getTotalTimeDepositAccountsGenerated() {
		return totalTimeDepositAccountsGenerated;
	}

	public long getTotalMutualFundAccountsGenerated() {
		return totalMutualFundAccountsGenerated;
	}

	public long getTotalMortgageAccountsGenerated() {
		return totalMortgageAccountsGenerated;
	}

	public long getTotalLoanAccountsGenerated() {
		return totalLoanAccountsGenerated;
	}

	public long getTotalDepositTransactionsGenerated() {
		return totalDepositTransactionsGenerated;
	}

	public long getTotalCreditCardTransactionsGenerated() {
		return totalCreditCardTransactionsGenerated;
	}

	public long getTotalLoanTransactionsGenerated() {
		return totalLoanTransactionsGenerated;
	}

	public long getTotalBranchesGenerated() {
		return totalBranchesGenerated;
	}

	public long getTotalAccountFeaturesGenerated() {
		return totalAccountFeaturesGenerated;
	}

	public long getNumberOfAccountsHavingCycleEnd() {
		return numberOfAccountsHavingCycleEnd;
	}

	public void setNumberOfAccountsHavingCycleEnd(long numberOfAccountsHavingCycleEnd) {
		this.numberOfAccountsHavingCycleEnd = numberOfAccountsHavingCycleEnd;
	}

	public long getTotalManualExceptionsGenerated() {
		return totalManualExceptionsGenerated;
	}

	public void incrementManualExceptionsGenerated() {
		totalManualExceptionsGenerated ++;
	}

	public long getNumberOfAccountsHavingBalanceUpdates() {
		return numberOfAccountsHavingBalanceUpdates;
	}

	public void incrementNumberOfAccountsHavingBalanceUpdates(long numberOfAccountsHavingBalanceUpdates) {
		this.numberOfAccountsHavingBalanceUpdates = numberOfAccountsHavingBalanceUpdates;
	}
	
	public void incrementCustomerAccountRelationshipsGenerated() {
		totalCustomerAccountRelationshipsGenerated ++;
	}
	
	public long getTotalCustomerAccountRelationshipsGenerated() {
		return totalCustomerAccountRelationshipsGenerated;
	}
	
	public long getTotalcustomerServiceContractId() {
	    return totalcustomerServiceContractId;
	}
	public void incrementTotalCustomerMergeGenerated() {
	    totalCustomerMergeGenerated++;
	}
	
	public long getTotalCustomerMergeGenerated() {
        return totalCustomerMergeGenerated;
    }

    public void incrementTotalAccountServiceSubscribeGenerated() {
	    totalAccountServiceSubscribeGenerated++;
	}
	public long getTotalAccountServiceSubscribeGenerated() {
	    return totalAccountServiceSubscribeGenerated;
	}
	
	public void incrementAccountConsentGenerated() {
	    totalAccountConsentGenerated++;
    }
    public long getTotalAccountConsentGenerated() {
        return totalAccountConsentGenerated;
    }
	
}