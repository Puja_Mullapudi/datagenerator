package com.zafin.rpe.test.tools.generator;

import java.util.ArrayList;
import java.util.List;

public class DayEndSpecification {

	private String dayEndDate;
	private boolean dayZero;
	private boolean generateFeeFile;
	private long numberOfAccountsHavingCycleEnd = 0;
	private long numberOfAccountsHavingBalanceUpdates = 0;
	private List<CustomerPTDGSpecification> customerPTDGSpecifications = new ArrayList<>();
	private List<CustomerGraphPTDGSpecification> customerGraphPTDGSpecifications = new ArrayList<>();
	
	public DayEndSpecification(String dayEndDate, boolean dayZero, long numberOfAccountsHavingCycleEnd, long numberOfAccountsHavingBalanceUpdates) {
		this.dayEndDate = dayEndDate;
		this.dayZero = dayZero;
		this.numberOfAccountsHavingCycleEnd = numberOfAccountsHavingCycleEnd;
		this.numberOfAccountsHavingBalanceUpdates = numberOfAccountsHavingBalanceUpdates;
	}
	
	public String getDayEndDate() {
		return dayEndDate;
	}
	public long getNumberOfAccountsHavingCycleEnd() {
		return numberOfAccountsHavingCycleEnd;
	}
	public boolean isDayZero() {
		return dayZero;
	}
	
	public List<CustomerPTDGSpecification> getCustomerPTDGSpecifications() {
		return customerPTDGSpecifications;
	}
	public void setCustomerPTDGSpecifications(List<CustomerPTDGSpecification> customerPTDGSpecifications) {
		this.customerPTDGSpecifications = customerPTDGSpecifications;
	}

	public long getNumberOfAccountsHavingBalanceUpdates() {
		return numberOfAccountsHavingBalanceUpdates;
	}

	public List<CustomerGraphPTDGSpecification> getCustomerGraphPTDGSpecifications() {
		return customerGraphPTDGSpecifications;
	}
	public boolean isGenerateFeeFile() {
		return generateFeeFile;
	}

	public void setGenerateFeeFile(boolean generateFeeFile) {
		this.generateFeeFile = generateFeeFile;
	}
}
