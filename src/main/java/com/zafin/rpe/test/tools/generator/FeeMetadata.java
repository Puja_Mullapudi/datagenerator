package com.zafin.rpe.test.tools.generator;

public class FeeMetadata {

	private String feeCode;
	private String feeName;
	private double feeValue;
	private int minValue;
	private int maxValue;
	
	public FeeMetadata(String feeCode, String feeName, double feeValue, int minValue, int maxValue) {
		this.feeCode = feeCode;
		this.feeName = feeName;
		this.feeValue = feeValue;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}
	
	public String getFeeCode() {
		return feeCode;
	}
	public String getFeeName() {
		return feeName;
	}
	public double getFeeValue() {
		return feeValue;
	}
	public int getMinValue() {
		return minValue;
	}
	public int getMaxValue() {
		return maxValue;
	}

}
