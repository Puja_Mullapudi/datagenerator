package com.zafin.rpe.test.tools.generator;

public class FeePTDGSpecification {
	
	private int numberOfFees;
	private long feeIdentifier;
	private String accountNumber;
	private String feeCode;
	private String feeName; 
	private String processingDate;
	private String createdDate;
	private double feeAmount;
	private double benefitAmount;
	private String benefitCode;
	
	public int getNumberOfFees() {
		return numberOfFees;
	}
	public void setNumberOfFees(int numberOfFees) {
		this.numberOfFees = numberOfFees;
	}
	public long getFeeIdentifier() {
		return feeIdentifier;
	}
	public void setFeeIdentifier(long feeIdentifier) {
		this.feeIdentifier = feeIdentifier;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getFeeCode() {
		return feeCode;
	}
	public void setFeeCode(String feeCode) {
		this.feeCode = feeCode;
	}
	public String getFeeName() {
		return feeName;
	}
	public void setFeeName(String feeName) {
		this.feeName = feeName;
	}
	public String getProcessingDate() {
		return processingDate;
	}
	public void setProcessingDate(String processingDate) {
		this.processingDate = processingDate;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public double getFeeAmount() {
		return feeAmount;
	}
	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}
	public double getBenefitAmount() {
		return benefitAmount;
	}
	public void setBenefitAmount(double benefitAmount) {
		this.benefitAmount = benefitAmount;
	}
	public String getBenefitCode() {
		return benefitCode;
	}
	public void setBenefitCode(String benefitCode) {
		this.benefitCode = benefitCode;
	}
	
}
