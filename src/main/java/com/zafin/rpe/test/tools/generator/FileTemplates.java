package com.zafin.rpe.test.tools.generator;

import java.util.Arrays;

import com.zafin.rpe.test.tools.TestingToolsRuntimeException;

public class FileTemplates {

	public static String customerDataTemplate = "{customerId}|{customerOpenDate}||{customerStatus}|{customerBirthDate}|{customerEmployeeStatus}|{customerSegmentCode}|{customerCohortCategoryCode}|{customerCohortCode}|{branchCode}";
	public static String depositAccountDataTemplate = "{accountNumber}|{productCode}|{accountStatus}|{accountOpenDate}||||||{payrollAccount}|{accountCohortCode}|CALENDAR||{accountCycleEndDate}|{inGoodStanding}||{regionCode}||ABC|CAD|{taxExempt}|CA|{billngPlan}";
	public static String dayZeroDepositAccountDataTemplate = "{accountNumber}|{productCode}|{accountStatus}|{accountOpenDate}||||||{payrollAccount}|{accountCohortCode}|CALENDAR||{inGoodStanding}|||ABC|CAD|{taxExempt}|CA|{billngPlan}";
	public static String timeDepositAccountDataTemplate = "{accountNumber}|{productCode}|{accountStatus}|{accountOpenDate}||||||N|||||{regionCode}|CALENDAR|||{accountCohortCode}||ABC|CAD|{taxExempt}|CA|{billngPlan}";
	public static String creditCardAccountDataTemplate = "{accountNumber}|{productCode}|{accountStatus}|{accountOpenDate}||||||||{regionCode}|CALENDAR|||{accountCohortCode}||ABC|CAD|{taxExempt}|CA|{billngPlan}";
	public static String mutualFundAccountDataTemplate = "{accountNumber}|{productCode}|{accountStatus}|{accountOpenDate}||||{regionCode}|CALENDAR|||{accountCohortCode}||ABC|CAD|{taxExempt}|CA|{billngPlan}";
	public static String morgageAccountDataTemplate = "{accountNumber}|{productCode}|{accountStatus}|{accountOpenDate}||||||{regionCode}|CALENDAR|||{accountCohortCode}||ABC|CAD|{taxExempt}|CA|{billngPlan}";
	public static String securitiesAccountDataTemplate = "{accountNumber}|{productCode}|{accountStatus}|{accountOpenDate}||||{regionCode}|CALENDAR|||{accountCohortCode}||ABC|CAD|{taxExempt}|CA|{billngPlan}";
    public static String loanAccountDataTemplate = "{accountNumber}|{productCode}|{accountStatus}|{accountOpenDate}||||||{regionCode}|CALENDAR|||{accountCohortCode}||ABC|CAD|{taxExempt}|CA|{billngPlan}";
	public static String relationshipDataTemplate = "{accountNumber}|{customerId}|{role}";
	public static String creditCardTransactionDataTemplate = "{accountNumber}|{transactionCode}|{transactionAmount}|DR|{dayEndDate}|{transactionChannel}||||||CA|{merchantStateCode}|{merchantRegionCode}|{merchantCategoryCode}|{merchantCode}";
	public static String depositTransactionDataTemplate = "{accountNumber}|{transactionCode}|{transactionAmount}|DR|{dayEndDate}|{transactionChannel}|2102|REF_20200101_1|POS Purchase|CA|{merchantStateCode}|{merchantRegionCode}|{merchantCategoryCode}|{merchantCode}";
	public static String loanTransactionDataTemplate = "{accountNumber}|{transactionCode}|{transactionAmount}|DR|{dayEndDate}|{transactionChannel}|||";
	public static String accountBalanceDataTemplate = "{accountNumber}|{dayEndDate}|{closingDayBalance}|CR|{averageBalance}|CR";
	public static String branchDataTemplate = "{branchCode}|{regionCode}||{countryCode}";
	public static String accountFeatureDataTemplate = "{accountNumber}|{featureCode}|{optionCode}|SELECT";
	public static String manualExceptionDataTemplate = "{accountNumber}|{feeOrReward}|{feeItemCode}|{startDate}|{endDate}|{reason}";
	public static String feeDataTemplate = "{feeIdentifier}|{accountNumber}||{feeCode}|{feeName}||{processingDate}|{createdDate}|{feeAmount}|DR|{benefitAmount}|||||1|0||||{benefitCode}||";
	public static String accountIdentifierChange = "{accountNumber}|{newAccountNumber}";
	public static String customerServiceInformationTemplate= "{customerId}|{contractId}|{serviceCode}|{optionCode}|SUBSCRIBE|20200101|{accountNumber}";
	public static String customerMergeInformationTemplate = "{sourceCustomerId}|{targetCustomerId}";
	public static String accountConsentInformationTemplate = "{customerId}|{accountNumber}|{consentCode}";
	public static String accountServiceSubsribeInfoTemplate = "{accountNumber}|{subscribeId}|{serviceCode}|SUBSCRIBE|{billngPlan}|ABC|CAD";
	public static String packageSubscribeTemplate = "{subscribeId}|{subscribeDate}|{packageCode}|SUBSCRIBE|{accountNumber}|CALENDAR";
	public static String packageAccountTemplate = "{subscribeId}|{accountNumber}|ADD|20200101";
	public static String offerEnrollment = "OFFER_2|{customerId}|";
	public static String optionSelecter = "{accountNumber}|ACCOUNT|MF_O|{optionCode}";
	public static String feeExclusion = "{accountNumber}|{dayEnd}|{dayEnd}|{reason}";
	public static String billingAccount="{billingId}|CREATE|BP_2|ABC|CAD";
	public static String billingAccountGroupAccount="{billingId}|{accountNumber}|ADD";

	public static String generateFee(FeePTDGSpecification feePTDGSpecification) {
		String feeLine = FileTemplates.feeDataTemplate.replaceAll("\\{feeIdentifier\\}", ""+feePTDGSpecification.getFeeIdentifier());
		feeLine = feeLine.replaceAll("\\{accountNumber\\}", feePTDGSpecification.getAccountNumber());
		feeLine = feeLine.replaceAll("\\{feeCode\\}", feePTDGSpecification.getFeeCode());
		feeLine = feeLine.replaceAll("\\{feeName\\}", feePTDGSpecification.getFeeName());
		feeLine = feeLine.replaceAll("\\{processingDate\\}", feePTDGSpecification.getProcessingDate());
		feeLine = feeLine.replaceAll("\\{createdDate\\}", feePTDGSpecification.getCreatedDate());
		feeLine = feeLine.replaceAll("\\{feeAmount\\}", ""+feePTDGSpecification.getFeeAmount());
		feeLine = feeLine.replaceAll("\\{benefitAmount\\}", ""+feePTDGSpecification.getBenefitAmount());
		feeLine = feeLine.replaceAll("\\{benefitCode\\}", feePTDGSpecification.getBenefitCode());
		return feeLine;
	}
	
	public static String generateCustomer(CustomerData customerData, String dayEndDate) {
		String customerLine = FileTemplates.customerDataTemplate.replaceAll("\\{customerId\\}", customerData.getCustomerId());
		customerLine = customerLine.replaceAll("\\{customerOpenDate\\}", customerData.getCustomerPTDGSpecification().getCustomerOpenDate());
		customerLine = customerLine.replaceAll("\\{customerStatus\\}", customerData.getCustomerPTDGSpecification().getCustomerStatus());
		customerLine = customerLine.replaceAll("\\{customerBirthDate\\}", customerData.getCustomerPTDGSpecification().getCustomerBirthDate());
		if(customerData.getCustomerSegment() == null) {
			customerLine = customerLine.replaceAll("\\{customerSegmentCode\\}", customerData.getCustomerPTDGSpecification().getCustomerSegment());
		} else {
			customerLine = customerLine.replaceAll("\\{customerSegmentCode\\}", customerData.getCustomerSegment());
		}
		customerLine = customerLine.replaceAll("\\{customerCohortCategoryCode\\}", customerData.getCustomerPTDGSpecification().getCustomerCohortCategoryCode());
		String customerCohortCode = customerData.getCustomerCohort() != null ? customerData.getCustomerCohort() : customerData.getCustomerPTDGSpecification().getCustomerCohortCode();
		customerLine = customerLine.replaceAll("\\{customerCohortCode\\}", customerCohortCode);

		String empStatus = customerData.getCustomerEmployeeStatus() != null ? customerData.getCustomerEmployeeStatus() : customerData.getCustomerPTDGSpecification().getCustomerEmployeeStatus();
		customerLine = customerLine.replaceAll("\\{branchCode\\}", customerData.getCustomerPTDGSpecification().getBranchCode());
		customerLine = customerLine.replaceAll("\\{customerEmployeeStatus\\}", empStatus);
		return customerLine;
	}
	
	public static String generateAccountFeature(AccountData accountData, String dayEndDate, AccountFeaturePTDGSpecification accountFeaturePTDGSpecification) {
		String accountFeatureLine = FileTemplates.accountFeatureDataTemplate.replaceAll("\\{accountNumber\\}", accountData.getAccountNumber());
		accountFeatureLine = accountFeatureLine.replaceAll("\\{featureCode\\}", accountFeaturePTDGSpecification.getFeatureCode());
		accountFeatureLine = accountFeatureLine.replaceAll("\\{optionCode\\}", ""+accountFeaturePTDGSpecification.getOptionCode());
		return accountFeatureLine;
	}
	
	public static String generateManualException(AccountData accountData, String dayEndDate, ManualExceptionPTDGSpecification manualExceptionPTDGSpecification) {
		String manualExceptionLine = FileTemplates.manualExceptionDataTemplate.replaceAll("\\{accountNumber\\}", accountData.getAccountNumber());
		manualExceptionLine = manualExceptionLine.replaceAll("\\{feeOrReward\\}", manualExceptionPTDGSpecification.getFeeOrReward());
		manualExceptionLine = manualExceptionLine.replaceAll("\\{feeItemCode\\}", ""+manualExceptionPTDGSpecification.getFeeItemCode());
		manualExceptionLine = manualExceptionLine.replaceAll("\\{startDate\\}", ""+manualExceptionPTDGSpecification.getStartDate());
		manualExceptionLine = manualExceptionLine.replaceAll("\\{endDate\\}", ""+manualExceptionPTDGSpecification.getEndDate());
		manualExceptionLine = manualExceptionLine.replaceAll("\\{reason\\}", ""+manualExceptionPTDGSpecification.getReason());
		return manualExceptionLine;
	}
	
	public static String generateCreditCardTransaction(AccountData accountData, String dayEndDate, TransactionsPTDGSpecification transactionsPTDGSpecification) {
	    //"{accountNumber}|{transactionCode}|{transactionAmount}|DR|{dayEndDate}|{transactionChannel}||||||CA|{merchantStateCode}|{merchantRegionCode}|{merchantCategoryCode}|{merchantCode}
		String transactionLine = FileTemplates.creditCardTransactionDataTemplate.replaceAll("\\{accountNumber\\}", accountData.getAccountNumber());
		transactionLine = transactionLine.replaceAll("\\{transactionCode\\}", transactionsPTDGSpecification.getTransactionCode());
		transactionLine = transactionLine.replaceAll("\\{transactionAmount\\}", ""+transactionsPTDGSpecification.getTransactionValue());
		transactionLine = transactionLine.replaceAll("\\{transactionChannel\\}", transactionsPTDGSpecification.getTransactionChannel());
        transactionLine = transactionLine.replaceAll("\\{merchantCategoryCode\\}", transactionsPTDGSpecification.getMerchantCategoryCode());
        transactionLine = transactionLine.replaceAll("\\{merchantCode\\}", transactionsPTDGSpecification.getMerchantCode());
        transactionLine = transactionLine.replaceAll("\\{merchantRegionCode\\}", transactionsPTDGSpecification.getMerchantRegionCode());
        transactionLine = transactionLine.replaceAll("\\{merchantStateCode\\}", transactionsPTDGSpecification.getMerchantStateCode());
		return transactionLine.replaceAll("\\{dayEndDate\\}", dayEndDate);
	}
	
	public static String generateDepositTransaction(AccountData accountData, String dayEndDate, TransactionsPTDGSpecification transactionsPTDGSpecification) {
		String transactionLine = FileTemplates.depositTransactionDataTemplate.replaceAll("\\{accountNumber\\}", accountData.getAccountNumber());
		transactionLine = transactionLine.replaceAll("\\{transactionCode\\}", transactionsPTDGSpecification.getTransactionCode());
		transactionLine = transactionLine.replaceAll("\\{transactionAmount\\}", ""+transactionsPTDGSpecification.getTransactionValue());
		transactionLine = transactionLine.replaceAll("\\{transactionChannel\\}", transactionsPTDGSpecification.getTransactionChannel());
		transactionLine = transactionLine.replaceAll("\\{merchantCategoryCode\\}", transactionsPTDGSpecification.getMerchantCategoryCode());
		transactionLine = transactionLine.replaceAll("\\{merchantCode\\}", transactionsPTDGSpecification.getMerchantCode());
		transactionLine = transactionLine.replaceAll("\\{merchantRegionCode\\}", transactionsPTDGSpecification.getMerchantRegionCode());
		transactionLine = transactionLine.replaceAll("\\{merchantStateCode\\}", transactionsPTDGSpecification.getMerchantStateCode());
		return transactionLine.replaceAll("\\{dayEndDate\\}", dayEndDate);
	}
	
	public static String generateBranch(BranchPTDGSpecification branchPTDGSpecification) {
		String branchLine = FileTemplates.branchDataTemplate.replaceAll("\\{branchCode\\}", branchPTDGSpecification.getBranchCode());
		branchLine = branchLine.replaceAll("\\{countryCode\\}", branchPTDGSpecification.getCountryCode());
		return branchLine.replaceAll("\\{regionCode\\}", branchPTDGSpecification.getRegionCode());
	}
	
	public static String generateLoanTransaction(AccountData accountData, String dayEndDate, TransactionsPTDGSpecification transactionsPTDGSpecification) {
	    //{accountNumber}|{transactionCode}|{transactionAmount}|DR|{dayEndDate}|{transactionChannel}|||";
		String transactionLine = FileTemplates.loanTransactionDataTemplate.replaceAll("\\{accountNumber\\}", accountData.getAccountNumber());
		transactionLine = transactionLine.replaceAll("\\{transactionCode\\}", transactionsPTDGSpecification.getTransactionCode());
        transactionLine = transactionLine.replaceAll("\\{transactionAmount\\}", ""+transactionsPTDGSpecification.getTransactionValue());
        transactionLine = transactionLine.replaceAll("\\{transactionChannel\\}", transactionsPTDGSpecification.getTransactionChannel());
		return transactionLine.replaceAll("\\{dayEndDate\\}", dayEndDate);
	}
	
	public static String generateMutualFundAccount(AccountData accountData) {
	    //{accountNumber}|{productCode}|ACTIVE|{accountOpenDate}||||{regionCode}|CALENDAR|||{accountCohortCode}||{taxExempt}|CAD||CA|{billngPlan}
		String accountLine = FileTemplates.mutualFundAccountDataTemplate.replaceAll("\\{accountNumber\\}", accountData.getAccountNumber());
		accountLine = accountLine.replaceAll("\\{accountStatus\\}", accountData.getAccountStatus());
		accountLine = accountLine.replaceAll("\\{accountOpenDate\\}", accountData.getAccountsPTDGSpecification().getAccountOpenDate());
		accountLine = accountLine.replaceAll("\\{regionCode\\}", accountData.getAccountsPTDGSpecification().getRegionCode());
        accountLine = accountLine.replaceAll("\\{accountCohortCode\\}", accountData.getAccountsPTDGSpecification().getAccountCohort());
        accountLine = accountLine.replaceAll("\\{taxExempt\\}", PTDGUtils.getRandomValue(Arrays.asList("Y","N")));
        accountLine = accountLine.replaceAll("\\{billngPlan\\}", accountData.getAccountsPTDGSpecification().getBillingPlan());
		return accountLine.replaceAll("\\{productCode\\}", accountData.getAccountsPTDGSpecification().getProductCode());
	}
	
	public static String generateMortgageAccount(AccountData accountData) {
	    //"{accountNumber}|{productCode}|ACTIVE|{accountOpenDate}||||{regionCode}|CALENDAR|||{accountCohortCode}||{taxExempt}|CAD||CA|{billngPlan}";
        String accountLine = FileTemplates.morgageAccountDataTemplate.replaceAll("\\{accountNumber\\}", accountData.getAccountNumber());
        accountLine = accountLine.replaceAll("\\{accountStatus\\}", accountData.getAccountStatus());
        accountLine = accountLine.replaceAll("\\{accountOpenDate\\}", accountData.getAccountsPTDGSpecification().getAccountOpenDate());
        accountLine = accountLine.replaceAll("\\{regionCode\\}", accountData.getAccountsPTDGSpecification().getRegionCode());
        accountLine = accountLine.replaceAll("\\{accountCohortCode\\}", accountData.getAccountsPTDGSpecification().getAccountCohort());
        accountLine = accountLine.replaceAll("\\{taxExempt\\}", PTDGUtils.getRandomValue(Arrays.asList("Y", "N")));
        accountLine = accountLine.replaceAll("\\{billngPlan\\}", accountData.getAccountsPTDGSpecification().getBillingPlan());
		return accountLine.replaceAll("\\{productCode\\}", accountData.getAccountsPTDGSpecification().getProductCode());
	}
	
	public static String generateAccount(AccountData accountData, EntityType entityType, String dayEndDate, String accountCycleEndDate, String regionCode) {
		if(entityType == EntityType.CREDIT_CARD_ACCOUNT) {
			return generateCreditCardAccount(accountData);
		} else if(entityType == EntityType.DEPOSIT_ACCOUNT) {
			return generateDepositAccount(accountData, dayEndDate, accountCycleEndDate, regionCode);
		} else if(entityType == EntityType.LOAN_ACCOUNT) {
			return generateLoanAccount(accountData);
		} else if(entityType == EntityType.MORTGAGE_ACCOUNT) {
			return generateMortgageAccount(accountData);
		} else if(entityType == EntityType.MUTUAL_FUND_ACCOUNT) {
			return generateMutualFundAccount(accountData);
		} else if(entityType == EntityType.TIME_DEPOSIT_ACCOUNT) {
			return generateTimeDepositAccount(accountData);
		} else if(entityType == EntityType.SECURITIES_ACCOUNT) {
            return generateSecuritiesAccount(accountData);
		} else {
			throw new TestingToolsRuntimeException("Unsupported account type: " + entityType);
		}
	}
	
	public static String generateDepositAccount(AccountData accountData, String dayEndDate, String accountCycleEndDate, String regionCode) {
		String depositAccountLine = FileTemplates.depositAccountDataTemplate.replaceAll("\\{accountNumber\\}", accountData.getAccountNumber());
		depositAccountLine = depositAccountLine.replaceAll("\\{accountStatus\\}", accountData.getAccountStatus());
		depositAccountLine = depositAccountLine.replaceAll("\\{accountCohortCode\\}", accountData.getAccountsPTDGSpecification().getAccountCohort());
		depositAccountLine = depositAccountLine.replaceAll("\\{accountOpenDate\\}", accountData.getAccountsPTDGSpecification().getAccountOpenDate());
		depositAccountLine = depositAccountLine.replaceAll("\\{payrollAccount\\}", accountData.getAccountsPTDGSpecification().getPayrollAccount());
		depositAccountLine = depositAccountLine.replaceAll("\\{accountCycleEndDate\\}", accountCycleEndDate);
		depositAccountLine = depositAccountLine.replaceAll("\\{regionCode\\}", regionCode);
		depositAccountLine = depositAccountLine.replaceAll("\\{accountStatus\\}", accountData.getAccountStatus());
		depositAccountLine = depositAccountLine.replaceAll("\\{inGoodStanding\\}", accountData.getInGoodStanding());
		depositAccountLine = depositAccountLine.replaceAll("\\{taxExempt\\}", PTDGUtils.getRandomValue(Arrays.asList("Y","N")));
		depositAccountLine = depositAccountLine.replaceAll("\\{billngPlan\\}", accountData.getAccountsPTDGSpecification().getBillingPlan());
		return depositAccountLine.replaceAll("\\{productCode\\}", accountData.getAccountsPTDGSpecification().getProductCode());
	}
	
	public static String generateDayZeroDepositAccount(AccountData accountData, String dayEndDate) {
	    //{accountNumber}|{productCode}|ACTIVE|{accountOpenDate}||||||{payrollAccount}|{accountCohortCode}|CALENDAR||{accountCycleEndDate}|Y";
		String depositAccountLine = FileTemplates.dayZeroDepositAccountDataTemplate.replaceAll("\\{accountNumber\\}", accountData.getAccountNumber());
		depositAccountLine = depositAccountLine.replaceAll("\\{accountCohortCode\\}", accountData.getAccountsPTDGSpecification().getAccountCohort());
        depositAccountLine = depositAccountLine.replaceAll("\\{accountOpenDate\\}", accountData.getAccountsPTDGSpecification().getAccountOpenDate());
        depositAccountLine = depositAccountLine.replaceAll("\\{payrollAccount\\}", accountData.getAccountsPTDGSpecification().getPayrollAccount());
        depositAccountLine = depositAccountLine.replaceAll("\\{accountStatus\\}", accountData.getAccountStatus());
        depositAccountLine = depositAccountLine.replaceAll("\\{inGoodStanding\\}", accountData.getInGoodStanding());
        depositAccountLine = depositAccountLine.replaceAll("\\{taxExempt\\}", PTDGUtils.getRandomValue(Arrays.asList("Y","N")));
        depositAccountLine = depositAccountLine.replaceAll("\\{billngPlan\\}", accountData.getAccountsPTDGSpecification().getBillingPlan());
        return depositAccountLine.replaceAll("\\{productCode\\}", accountData.getAccountsPTDGSpecification().getProductCode());
	}
	
	public static String generateCreditCardAccount(AccountData accountData) {
	    //{accountNumber}|{productCode}|ACTIVE|{accountOpenDate}||||||||{regionCode}|CALENDAR|||{accountCohortCode}|||CAD|{taxExempt}|CA|{billngPlan}";
		String accountLine = FileTemplates.creditCardAccountDataTemplate.replaceAll("\\{accountNumber\\}", accountData.getAccountNumber());
		accountLine = accountLine.replaceAll("\\{accountStatus\\}", accountData.getAccountStatus());
		accountLine = accountLine.replaceAll("\\{accountOpenDate\\}", accountData.getAccountsPTDGSpecification().getAccountOpenDate());
		accountLine = accountLine.replaceAll("\\{regionCode\\}", accountData.getAccountsPTDGSpecification().getRegionCode());
        accountLine = accountLine.replaceAll("\\{accountCohortCode\\}", accountData.getAccountsPTDGSpecification().getAccountCohort());
        accountLine = accountLine.replaceAll("\\{taxExempt\\}", PTDGUtils.getRandomValue(Arrays.asList("Y","N")));
        accountLine = accountLine.replaceAll("\\{billngPlan\\}", accountData.getAccountsPTDGSpecification().getBillingPlan());
		return accountLine.replaceAll("\\{productCode\\}", accountData.getAccountsPTDGSpecification().getProductCode());
	}
	
	public static String generateLoanAccount(AccountData accountData) {
	    //{accountNumber}|{productCode}|{accountStatus}|{accountOpenDate}||||||{regionCode}|CALENDAR|||{accountCohortCode}|||CAD|{taxExempt}|CA|{billngPlan}"
		String accountLine = FileTemplates.loanAccountDataTemplate.replaceAll("\\{accountNumber\\}", accountData.getAccountNumber());
		accountLine = accountLine.replaceAll("\\{accountStatus\\}", accountData.getAccountStatus());
		accountLine = accountLine.replaceAll("\\{accountOpenDate\\}", accountData.getAccountsPTDGSpecification().getAccountOpenDate());
        accountLine = accountLine.replaceAll("\\{regionCode\\}", accountData.getAccountsPTDGSpecification().getRegionCode());
        accountLine = accountLine.replaceAll("\\{accountCohortCode\\}", accountData.getAccountsPTDGSpecification().getAccountCohort());
        accountLine = accountLine.replaceAll("\\{taxExempt\\}", PTDGUtils.getRandomValue(Arrays.asList("Y","N")));
        accountLine = accountLine.replaceAll("\\{billngPlan\\}", accountData.getAccountsPTDGSpecification().getBillingPlan());

		return accountLine.replaceAll("\\{productCode\\}", accountData.getAccountsPTDGSpecification().getProductCode());
	}
	
	public static String generateTimeDepositAccount(AccountData accountData) {
	    //{accountNumber}|{productCode}|ACTIVE|{accountOpenDate}||||||N|||||{regionCode}|CALENDAR|||{accountCohortCode}||{taxExempt}|CAD||CA|{billngPlan}"
		String accountLine = FileTemplates.timeDepositAccountDataTemplate.replaceAll("\\{accountNumber\\}", accountData.getAccountNumber());
		accountLine = accountLine.replaceAll("\\{accountStatus\\}", accountData.getAccountStatus());
		accountLine = accountLine.replaceAll("\\{accountOpenDate\\}", accountData.getAccountsPTDGSpecification().getAccountOpenDate());
		accountLine = accountLine.replaceAll("\\{regionCode\\}", accountData.getAccountsPTDGSpecification().getRegionCode());
		accountLine = accountLine.replaceAll("\\{accountCohortCode\\}", accountData.getAccountsPTDGSpecification().getAccountCohort());
		accountLine = accountLine.replaceAll("\\{taxExempt\\}", PTDGUtils.getRandomValue(Arrays.asList("Y","N")));
		accountLine = accountLine.replaceAll("\\{billngPlan\\}", accountData.getAccountsPTDGSpecification().getBillingPlan());
		return accountLine.replaceAll("\\{productCode\\}", accountData.getAccountsPTDGSpecification().getProductCode());
	}
	
	public static String generateSecuritiesAccount(AccountData accountData) {
        //"{accountNumber}|{productCode}|{accountStatus}|{accountOpenDate}||||{regionCode}|CALENDAR|||{accountCohortCode}||{taxExempt}|CAD||CA|{billngPlan}";
        String accountLine = FileTemplates.securitiesAccountDataTemplate.replaceAll("\\{accountNumber\\}", accountData.getAccountNumber());
        accountLine = accountLine.replaceAll("\\{accountStatus\\}", accountData.getAccountStatus());
        accountLine = accountLine.replaceAll("\\{accountOpenDate\\}", accountData.getAccountsPTDGSpecification().getAccountOpenDate());
        accountLine = accountLine.replaceAll("\\{regionCode\\}", accountData.getAccountsPTDGSpecification().getRegionCode());
        accountLine = accountLine.replaceAll("\\{accountCohortCode\\}", accountData.getAccountsPTDGSpecification().getAccountCohort());
        accountLine = accountLine.replaceAll("\\{taxExempt\\}", PTDGUtils.getRandomValue(Arrays.asList("Y","N")));
        accountLine = accountLine.replaceAll("\\{billngPlan\\}", accountData.getAccountsPTDGSpecification().getBillingPlan());
        return accountLine.replaceAll("\\{productCode\\}", accountData.getAccountsPTDGSpecification().getProductCode());
}
}
