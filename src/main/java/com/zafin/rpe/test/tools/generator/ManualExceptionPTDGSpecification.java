package com.zafin.rpe.test.tools.generator;

public class ManualExceptionPTDGSpecification {

	private String feeOrReward;
	private String feeItemCode;
	private String startDate;
	private String endDate;
	private String reason;
	
	public String getFeeOrReward() {
		return feeOrReward;
	}
	public void setFeeOrReward(String feeOrReward) {
		this.feeOrReward = feeOrReward;
	}
	public String getFeeItemCode() {
		return feeItemCode;
	}
	public void setFeeItemCode(String feeItemCode) {
		this.feeItemCode = feeItemCode;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	
	
}
