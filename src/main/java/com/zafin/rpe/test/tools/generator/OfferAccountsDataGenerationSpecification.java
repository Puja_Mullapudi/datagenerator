package com.zafin.rpe.test.tools.generator;

import java.util.ArrayList;
import java.util.List;

import com.zafin.rpe.test.tools.generator.input.model.DateRange;

public class OfferAccountsDataGenerationSpecification {

	private List<String> products = new ArrayList<>();
	private DateRange accountOpenDate;
	private boolean payroll;
	private boolean includeJointAccounts;
	
	public List<String> getProducts() {
		return products;
	}
	public void setProducts(List<String> products) {
		this.products = products;
	}
	public DateRange getAccountOpenDate() {
		return accountOpenDate;
	}
	public void setAccountOpenDate(DateRange accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}
	public boolean isPayroll() {
		return payroll;
	}
	public void setPayroll(boolean payroll) {
		this.payroll = payroll;
	}
	public boolean isIncludeJointAccounts() {
		return includeJointAccounts;
	}
	public void setIncludeJointAccounts(boolean includeJointAccounts) {
		this.includeJointAccounts = includeJointAccounts;
	}

	
}
