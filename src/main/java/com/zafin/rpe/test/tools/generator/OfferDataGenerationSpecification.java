package com.zafin.rpe.test.tools.generator;

public class OfferDataGenerationSpecification {

	private OfferEnrollmentDataGenerationSpecification offerEnrollmentDataGenerationSpecification;

	public OfferEnrollmentDataGenerationSpecification getOfferEnrollmentDataGenerationSpecification() {
		return offerEnrollmentDataGenerationSpecification;
	}

	public void setOfferEnrollmentDataGenerationSpecification(OfferEnrollmentDataGenerationSpecification offerEnrollmentDataGenerationSpecification) {
		this.offerEnrollmentDataGenerationSpecification = offerEnrollmentDataGenerationSpecification;
	}
	
}
