package com.zafin.rpe.test.tools.generator;

import com.zafin.rpe.test.tools.generator.input.model.DateRange;

public class OfferEnrollmentDataGenerationSpecification {

	private DateRange customerOpenDate;
	private RestrictAccountDataGenerationSpecification restrictAccountDataGenerationSpecification;
	private CustomerHoldingDataGenerationSpecification customerHoldingDataGenerationSpecification;
	private OfferAccountsDataGenerationSpecification offerAccountsDataGenerationSpecification;
    private DateRange executionDateRange; //Start of a month and end of a month 
    private long numberOfCustomerToEnroll;
	
	public DateRange getCustomerOpenDate() {
		return customerOpenDate;
	}

	public void setCustomerOpenDate(DateRange customerOpenDate) {
		this.customerOpenDate = customerOpenDate;
	}

	public RestrictAccountDataGenerationSpecification getRestrictAccountDataGenerationSpecification() {
		return restrictAccountDataGenerationSpecification;
	}

	public void setRestrictAccountDataGenerationSpecification(RestrictAccountDataGenerationSpecification restrictAccountDataGenerationSpecification) {
		this.restrictAccountDataGenerationSpecification = restrictAccountDataGenerationSpecification;
	}

	public CustomerHoldingDataGenerationSpecification getCustomerHoldingDataGenerationSpecification() {
		return customerHoldingDataGenerationSpecification;
	}

	public void setCustomerHoldingDataGenerationSpecification(CustomerHoldingDataGenerationSpecification customerHoldingDataGenerationSpecification) {
		this.customerHoldingDataGenerationSpecification = customerHoldingDataGenerationSpecification;
	}

	public OfferAccountsDataGenerationSpecification getOfferAccountsDataGenerationSpecification() {
		return offerAccountsDataGenerationSpecification;
	}

	public void setOfferAccountsDataGenerationSpecification(OfferAccountsDataGenerationSpecification offerAccountsDataGenerationSpecification) {
		this.offerAccountsDataGenerationSpecification = offerAccountsDataGenerationSpecification;
	}

	public DateRange getExecutionDateRange() {
		return executionDateRange;
	}

	public void setExecutionDateRange(DateRange executionDateRange) {
		this.executionDateRange = executionDateRange;
	}

	public long getNumberOfCustomerToEnroll() {
		return numberOfCustomerToEnroll;
	}

	public void setNumberOfCustomerToEnroll(long numberOfCustomerToEnroll) {
		this.numberOfCustomerToEnroll = numberOfCustomerToEnroll;
	}
	
	
}

