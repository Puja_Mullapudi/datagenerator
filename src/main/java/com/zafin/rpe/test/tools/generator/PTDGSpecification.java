package com.zafin.rpe.test.tools.generator;

public class PTDGSpecification {

	private OfferDataGenerationSpecification offerDataGenerationSpecification;
	private CustomPTDGSpecification customPTDGSpecification;
	private String systemDate;
	
	public OfferDataGenerationSpecification getOfferDataGenerationSpecification() {
		return offerDataGenerationSpecification;
	}

	public void setOfferDataGenerationSpecification(OfferDataGenerationSpecification offerDataGenerationSpecification) {
		this.offerDataGenerationSpecification = offerDataGenerationSpecification;
	}

	public CustomPTDGSpecification getCustomPTDGSpecification() {
		return customPTDGSpecification;
	}

	public void setCustomPTDGSpecification(CustomPTDGSpecification customPTDGSpecification) {
		this.customPTDGSpecification = customPTDGSpecification;
	}

	public String getSystemDate() {
		return systemDate;
	}

	public void setSystemDate(String systemDate) {
		this.systemDate = systemDate;
	}
	
}

