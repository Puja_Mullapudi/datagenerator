package com.zafin.rpe.test.tools.generator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zafin.rpe.test.tools.TestingToolsRuntimeException;

public class PTDGUtils {

    private static final String SPACE = " ";
    private static final Logger logger = LoggerFactory.getLogger(PTDGUtils.class);
    private static final String CUSTOMER_PREFIX = "CUS";
    private static final String ACCOUNT_PREFIX = "ACC";
    private static final String ZERO = "0";
    private static final String QUOTE = "'";
    private static final String DAT_FILE_EXTENSION = ".DAT";
    private static final String UNDERSCORE = "_";
    private static final String START_DAY_END = "START_DAY_END";
    private static final String TRIGGER_FILE_EXTENSION = ".TRG";

    public static void insertTransactionFileHeaders(Map<EntityType, File> transactionFiles, String dayEndDate, DayEndFileLineCounts dayEndFileLineCounts) {
        Set<EntityType> keySet = transactionFiles.keySet();
        for (EntityType inputFileType : keySet) {
            if (inputFileType == EntityType.DEPOSIT_TRANSACTION) {
                insertFileHeader(transactionFiles.get(inputFileType), dayEndDate, dayEndFileLineCounts.getTotalDepositTransactionsGenerated());
            } else if (inputFileType == EntityType.CREDIT_CARD_TRANSACTION) {
                insertFileHeader(transactionFiles.get(inputFileType), dayEndDate, dayEndFileLineCounts.getTotalCreditCardTransactionsGenerated());
            } else if (inputFileType == EntityType.LOAN_TRANSACTION) {
                insertFileHeader(transactionFiles.get(inputFileType), dayEndDate, dayEndFileLineCounts.getTotalLoanTransactionsGenerated());
            }
        }
    }

    public static void generateTransactionFiles(List<AccountData> accountDataList,
                                                String dayEndDate,
                                                Map<EntityType, File> transactionFiles,
                                                String folderPath,
                                                DayEndFileLineCounts dayEndFileLineCounts,
                                                File feeFile) {

        long startTime = System.currentTimeMillis();
        Map<EntityType, List<String>> linesMap = initializeTransactionLinesMap();
        List<EntityType> applicableTransactionTypes = new ArrayList<>();
        List<String> feeLines = new ArrayList<String>();
        // for (CustomerData customerData : chunkedCustomerData) {
        // List<AccountData> accountDataList = customerData.getAccountDataList();
        for (int i = 0; i < accountDataList.size(); i++) {
            AccountData accountData = accountDataList.get(i);
            if (accountData.getAccountStatus() == "ACTIVE") {
                // For each account created, create the necessary transactions
                String productCode = accountData.getAccountsPTDGSpecification().getProductCode();
                List<TransactionsPTDGSpecification> transactionSpecificationsForProduct = getTransactionSpecificationsForProduct(accountData.getAccountsPTDGSpecification()
                                                                                                                                            .getTransactionPTDGSpecifications(),
                                                                                                                                 productCode);
                for (TransactionsPTDGSpecification transactionsPTDGSpecification : transactionSpecificationsForProduct) {
                    if (!applicableTransactionTypes.contains(transactionsPTDGSpecification.getEntityType())) {
                        applicableTransactionTypes.add(transactionsPTDGSpecification.getEntityType());
                    }
                    int numberOfTransactions = transactionsPTDGSpecification.getNumberOfTransactions();
                    if (transactionsPTDGSpecification.getEntityType() == EntityType.CREDIT_CARD_TRANSACTION) {
                        for (int j = 0; j < numberOfTransactions; j++) {
                            DataGenerationContext.incrementCreditCardTransactionsGenerated();
                            dayEndFileLineCounts.incrementCreditCardTransactionsGenerated();
                            linesMap.get(EntityType.CREDIT_CARD_TRANSACTION)
                                    .add(FileTemplates.generateCreditCardTransaction(accountData, dayEndDate, transactionsPTDGSpecification));
                        }
                    } else if (transactionsPTDGSpecification.getEntityType() == EntityType.DEPOSIT_TRANSACTION) {
                        for (int j = 0; j < numberOfTransactions; j++) {
                            DataGenerationContext.incrementDepositTransactionsGenerated();
                            dayEndFileLineCounts.incrementDepositTransactionsGenerated();
                            linesMap.get(EntityType.DEPOSIT_TRANSACTION)
                                    .add(FileTemplates.generateDepositTransaction(accountData, dayEndDate, transactionsPTDGSpecification));
                        }
                    } else if (transactionsPTDGSpecification.getEntityType() == EntityType.LOAN_TRANSACTION) {
                        for (int j = 0; j < numberOfTransactions; j++) {
                            DataGenerationContext.incrementLoanTransactionsGenerated();
                            dayEndFileLineCounts.incrementLoanTransactionsGenerated();
                            linesMap.get(EntityType.LOAN_TRANSACTION)
                                    .add(FileTemplates.generateLoanTransaction(accountData, dayEndDate, transactionsPTDGSpecification));
                        }
                    }
                }

                // For each account created, create the necessary fees
                // List<FeePTDGSpecification> feePTDGSpecifications = accountData.getAccountsPTDGSpecification()
                // .getFeePTDGSpecifications();
                // for (FeePTDGSpecification feePTDGSpecification : feePTDGSpecifications) {
                // int numberOfFees = feePTDGSpecification.getNumberOfFees();
                // for (int j = 0; j < numberOfFees; j++) {
                // long feeId = DataGenerationContext.incrementFeesGenerated() + 1;
                // feePTDGSpecification.setFeeIdentifier(feeId);
                // feePTDGSpecification.setAccountNumber(accountData.getAccountNumber());
                // feePTDGSpecification.setProcessingDate(dayEndDate);
                // feePTDGSpecification.setCreatedDate(dayEndDate);
                // feeLines.add(FileTemplates.generateFee(feePTDGSpecification));
                // }
                // }

                if (i != 0 && i % 10000 == 0 || i == accountDataList.size() - 1) {
                    logger.debug("...processed {} accounts", i + 1);
                }
            }
        }
        // }
        try {
            for (EntityType entityType : applicableTransactionTypes) {
                File file = transactionFiles.get(entityType);
                EntityType inputFileType = getInputFileTypeForTransactionType(entityType);
                if (file == null) {
                    file = createFile(folderPath, inputFileType, dayEndDate, false);
                    transactionFiles.put(entityType, file);
                }
                FileUtils.writeLines(transactionFiles.get(entityType), linesMap.get(entityType), true);
                logger.debug("Generated " + linesMap.get(entityType).size() + SPACE + inputFileType + " transactions in " + (System.currentTimeMillis() - startTime) / 1000 + "s");
            }
            // Write the fee file
            try {
                if (feeFile != null) {
                    FileUtils.writeLines(feeFile, feeLines, true);
                }
            } catch (IOException e) {
                throw new TestingToolsRuntimeException(e);
            }
        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
        logger.debug("Generated Transactions in " + (System.currentTimeMillis() - startTime) / 1000 + "s");
    }

    private static EntityType getInputFileTypeForTransactionType(EntityType entityType) {
        if (entityType == EntityType.CREDIT_CARD_TRANSACTION) {
            return EntityType.CREDIT_CARD_TRANSACTION;
        } else if (entityType == EntityType.DEPOSIT_TRANSACTION) {
            return EntityType.DEPOSIT_TRANSACTION;
        } else if (entityType == EntityType.LOAN_TRANSACTION) {
            return EntityType.LOAN_TRANSACTION;
        } else {
            throw new TestingToolsRuntimeException("Invalid transaction type: " + entityType);
        }
    }

    private static List<TransactionsPTDGSpecification> getTransactionSpecificationsForProduct(List<TransactionsPTDGSpecification> specifications, String productCode) {
        List<TransactionsPTDGSpecification> productSpecificTansactionSpecifications = new ArrayList<>();
        for (TransactionsPTDGSpecification transactionsPTDGSpecification : specifications) {
            if (transactionsPTDGSpecification.getProductCode() == productCode) {
                productSpecificTansactionSpecifications.add(transactionsPTDGSpecification);
            }
        }
        return productSpecificTansactionSpecifications;
    }

    public static Map<EntityType, List<String>> initializeTransactionLinesMap() {
        Map<EntityType, List<String>> linesMap = new HashMap<>();
        linesMap.put(EntityType.CREDIT_CARD_TRANSACTION, new ArrayList<String>());
        linesMap.put(EntityType.DEPOSIT_TRANSACTION, new ArrayList<String>());
        linesMap.put(EntityType.LOAN_TRANSACTION, new ArrayList<String>());
        return linesMap;
    }

    public static Map<EntityType, File> initializeTransactionFiles(String inputsFolderPath, String dayEndDate) {
        Map<EntityType, File> transactionFiles = new HashMap<>();
        transactionFiles.put(EntityType.DEPOSIT_TRANSACTION, createFile(inputsFolderPath, EntityType.DEPOSIT_TRANSACTION, dayEndDate, false));
        transactionFiles.put(EntityType.CREDIT_CARD_TRANSACTION, createFile(inputsFolderPath, EntityType.CREDIT_CARD_TRANSACTION, dayEndDate, false));
        transactionFiles.put(EntityType.LOAN_TRANSACTION, createFile(inputsFolderPath, EntityType.LOAN_TRANSACTION, dayEndDate, false));
        return transactionFiles;
    }

    public static void generateAccountIdentifierChangeFile(List<AccountData> accountDataList,
                                                           String dayEndDate,
                                                           File accountIdentifierChangeFile,
                                                           DayEndFileLineCounts dayendcount,
                                                           long perGraphIterations) {
        long startTime = System.currentTimeMillis();
        List<String> lines = new ArrayList<>();
        int iter = 1;
        for (int i = 0; i < accountDataList.size() && iter <= perGraphIterations; i++) {
            String line = FileTemplates.accountIdentifierChange.replaceAll("\\{accountNumber\\}", accountDataList.get(i).getAccountNumber());
            line = line.replaceAll("\\{newAccountNumber\\}", accountDataList.get(i).getAccountNumber() + "X");
            lines.add(line);
            dayendcount.incrementalAccountIdentifierChangeGenerated();
            iter++;

        }
        try {
            FileUtils.writeLines(accountIdentifierChangeFile, lines, true);
        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
        logger.debug("Generated " + lines.size() + " Account Identifier Changes in " + (System.currentTimeMillis() - startTime) / 1000 + "s");
    }

    public static void generateAccounBalanceFile(List<AccountData> accountDataList, String dayEndDate, File accountBalanceFile, DayEndFileLineCounts dayEndFileLineCounts) {
        long startTime = System.currentTimeMillis();
        List<String> lines = new ArrayList<>();
        for (AccountData accountData : accountDataList) {
            String line = FileTemplates.accountBalanceDataTemplate.replaceAll("\\{accountNumber\\}", accountData.getAccountNumber());
            line = line.replaceAll("\\{dayEndDate\\}", dayEndDate);
            line = line.replaceAll("\\{closingDayBalance\\}", "" + new Double(accountData.getAccountsPTDGSpecification().getClosingDayBalance()));
            line = line.replaceAll("\\{averageBalance\\}", "" + new Double(accountData.getAccountsPTDGSpecification().getAverageBalance()));
            lines.add(line);
            dayEndFileLineCounts.incrementTotalAccountBalancesGenerated();

        }
        try {
            FileUtils.writeLines(accountBalanceFile, lines, true);
        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
        logger.debug("Generated " + lines.size() + " Account Balances in " + (System.currentTimeMillis() - startTime) / 1000 + "s");
    }

    public static void generateDayZeroAccountBalancesFile(List<AccountData> accountDataList, String dayEndDate, File dayZeroaccountBalancesFile) {
        long startTime = System.currentTimeMillis();
        List<String> lines = new ArrayList<>();
        // for (CustomerData customerData : chunkedCustomerData) {
        // List<AccountData> accountDataList = customerData.getAccountDataList();
        for (AccountData accountData : accountDataList) {
            //{accountNumber}|{dayEndDate}|{closingDayBalance}|CR|{averageBalance}|CR";
            String line = FileTemplates.accountBalanceDataTemplate.replaceAll("\\{accountNumber\\}", accountData.getAccountNumber());
            line = line.replaceAll("\\{dayEndDate\\}", dayEndDate);
            line = line.replaceAll("\\{closingDayBalance\\}", "" + new Double(accountData.getAccountsPTDGSpecification().getClosingDayBalance()));
            line = line.replaceAll("\\{averageBalance\\}", "" + new Double(accountData.getAccountsPTDGSpecification().getAverageBalance()));
            lines.add(line);
        }
        // }
        try {
            FileUtils.writeLines(dayZeroaccountBalancesFile, lines, true);
        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
        logger.debug("Generated " + lines.size() + " Account Balances in " + (System.currentTimeMillis() - startTime) / 1000 + "s");
    }

    public static void insertAccountFileHeaders(Map<EntityType, File> accountFiles, String dayEndDate, DayEndFileLineCounts dayEndFileLineCounts) {
        Set<EntityType> keySet = accountFiles.keySet();
        for (EntityType entityType : keySet) {
            if (entityType == EntityType.MORTGAGE_ACCOUNT) {
                insertFileHeader(accountFiles.get(entityType), dayEndDate, dayEndFileLineCounts.getTotalMortgageAccountsGenerated());
            } else if (entityType == EntityType.DEPOSIT_ACCOUNT) {
                insertFileHeader(accountFiles.get(entityType),
                                 dayEndDate,
                                 (dayEndFileLineCounts.getTotalDepositAccountsGenerated() + dayEndFileLineCounts.getNumberOfAccountsHavingCycleEnd()));
            } else if (entityType == EntityType.TIME_DEPOSIT_ACCOUNT) {
                insertFileHeader(accountFiles.get(entityType), dayEndDate, dayEndFileLineCounts.getTotalTimeDepositAccountsGenerated());
            } else if (entityType == EntityType.LOAN_ACCOUNT) {
                insertFileHeader(accountFiles.get(entityType), dayEndDate, dayEndFileLineCounts.getTotalLoanAccountsGenerated());
            } else if (entityType == EntityType.MUTUAL_FUND_ACCOUNT) {
                insertFileHeader(accountFiles.get(entityType), dayEndDate, dayEndFileLineCounts.getTotalMutualFundAccountsGenerated());
            } else if (entityType == EntityType.CREDIT_CARD_ACCOUNT) {
                insertFileHeader(accountFiles.get(entityType), dayEndDate, dayEndFileLineCounts.getTotalCreditCardAccountsGenerated());
            } else if (entityType == EntityType.SECURITIES_ACCOUNT) {
                insertFileHeader(accountFiles.get(entityType), dayEndDate, dayEndFileLineCounts.getTotalSecuritiesAccountGenerated());
            }
        }
    }

    public static Map<EntityType, File> initializeAccountFiles(String inputsFolderPath, String dayEndDate) {
        Map<EntityType, File> accountFiles = new HashMap<>();
        accountFiles.put(EntityType.DEPOSIT_ACCOUNT, createFile(inputsFolderPath, EntityType.DEPOSIT_ACCOUNT, dayEndDate, false));
        accountFiles.put(EntityType.CREDIT_CARD_ACCOUNT, createFile(inputsFolderPath, EntityType.CREDIT_CARD_ACCOUNT, dayEndDate, false));
        accountFiles.put(EntityType.TIME_DEPOSIT_ACCOUNT, createFile(inputsFolderPath, EntityType.TIME_DEPOSIT_ACCOUNT, dayEndDate, false));
        accountFiles.put(EntityType.MUTUAL_FUND_ACCOUNT, createFile(inputsFolderPath, EntityType.MUTUAL_FUND_ACCOUNT, dayEndDate, false));
        accountFiles.put(EntityType.MORTGAGE_ACCOUNT, createFile(inputsFolderPath, EntityType.MORTGAGE_ACCOUNT, dayEndDate, false));
        accountFiles.put(EntityType.LOAN_ACCOUNT, createFile(inputsFolderPath, EntityType.LOAN_ACCOUNT, dayEndDate, false));
        accountFiles.put(EntityType.SECURITIES_ACCOUNT, createFile(inputsFolderPath, EntityType.SECURITIES_ACCOUNT, dayEndDate, false));
        return accountFiles;
    }

    public static int generateAccountFile(List<AccountData> accountDataList,
                                          String dayEndDate,
                                          Map<EntityType, File> accountFiles,
                                          String folderPath,
                                          EntityType entityType) {
        long startTime = System.currentTimeMillis();
        List<String> accountLines = new ArrayList<>();
        // for (CustomerData customerData : chunkedCustomerData) {
        // List<AccountData> accountDataList = customerData.getAccountDataList();
        for (AccountData accountData : accountDataList) {
            if (accountData.getEntityType() == entityType) {
                accountLines.add(FileTemplates.generateAccount(accountData,
                                                               entityType,
                                                               dayEndDate,
                                                               accountData.getAccountsPTDGSpecification().getCycleEndDate(),
                                                               accountData.getAccountsPTDGSpecification().getRegionCode()));
            }
        }
        // }
        try {
            File accountFile = accountFiles.get(entityType);
            if (accountFile == null) {
                accountFile = createFile(folderPath, entityType, dayEndDate, false);
                accountFiles.put(entityType, accountFile);
            }
            if (accountLines != null && accountLines.size() > 0) {
                FileUtils.writeLines(accountFile, accountLines, true);
            }
            logger.debug("Generated " + accountLines.size() + SPACE + entityType + " accounts in " + (System.currentTimeMillis() - startTime) / 1000 + "s");
        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
        return accountLines.size();
    }

    public static void createDailyAccountBalances(File accountBalanceFile,
                                                  File dayZeroaccountBalancesFile,
                                                  String dayEndDate,
                                                  long numberOfAccountsHavingBalanceUpdates,
                                                  String dataFolderPath) {
        try {
            long accountCount = 0;

            List<String> accountBalances = new ArrayList<>();
            LineIterator it = FileUtils.lineIterator(dayZeroaccountBalancesFile, "UTF-8");
            try {
                while (it.hasNext()) {
                    String accountLine = it.nextLine();
                    accountLine = accountLine.replaceAll("\\{dayEndDate\\}", dayEndDate);
                    if (accountCount < numberOfAccountsHavingBalanceUpdates) {
                        accountBalances.add(accountLine);
                    }
                    accountCount++;
                }
            } finally {
                it.close();
            }

            // Append to the balances file
            if (accountBalances != null && accountBalances.size() > 0) {
                FileUtils.writeLines(accountBalanceFile, accountBalances, true);
            }

        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
    }

    public static void addAccountBalances(File accountBalanceFile,
                                          File dayZeroaccountBalancesFile,
                                          String dayEndDate,
                                          long numberOfAccountsHavingBalanceUpdates,
                                          String dataFolderPath) {
        try {
            long accountCount = 0;

            List<String> accountBalances = new ArrayList<>();
            LineIterator it = FileUtils.lineIterator(dayZeroaccountBalancesFile, "UTF-8");
            try {
                while (it.hasNext()) {
                    String accountLine = it.nextLine();
                    if (accountCount < numberOfAccountsHavingBalanceUpdates) {
                        accountBalances.add(accountLine);
                    } /*
                       * else { List<String> updatedDayZeroAccounts = new ArrayList<>();
                       * updatedDayZeroAccounts.add(accountLine);
                       * 
                       * //Update the day zero account data file to temp file
                       * FileUtils.writeLines(dayZeroAccountsTempFile, updatedDayZeroAccounts, true); }
                       */
                    accountCount++;
                }
            } finally {
                it.close();
            }

            // Append to the balances file
            if (accountBalances != null && accountBalances.size() > 0) {
                FileUtils.writeLines(accountBalanceFile, accountBalances, true);
            }

            // Create the temporary file without the accounts that were used in balances
            File dayZeroAccountBalancesTempFile = new File(dataFolderPath + "/DayZeroAccountBalancesTemp.DAT");
            if (dayZeroAccountBalancesTempFile.exists()) {
                dayZeroAccountBalancesTempFile.delete();
            }
            dayZeroAccountBalancesTempFile.createNewFile();
            deleteLines(dataFolderPath, dayZeroaccountBalancesFile, dayZeroAccountBalancesTempFile, numberOfAccountsHavingBalanceUpdates);

            // Replace the original dayZeroaccountBalancesFile with the updated temp file
            dayZeroAccountBalancesTempFile.renameTo(dayZeroaccountBalancesFile);

        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
    }

    public static void addCycleEndDepositAccounts(File depositAccountFile,
                                                  File dayZeroAccountsFile,
                                                  String dayEndDate,
                                                  long numberOfAccountsHavingCycleEnd,
                                                  String dataFolderPath) {
        try {
            long accountCount = 0;

            List<String> cycleEndAccounts = new ArrayList<>();
            LineIterator it = FileUtils.lineIterator(dayZeroAccountsFile, "UTF-8");
            try {
                while (it.hasNext()) {
                    String accountLine = it.nextLine();
                    if (accountCount < numberOfAccountsHavingCycleEnd) {
                        accountLine = accountLine.replaceAll("\\{accountCycleEndDate\\}", dayEndDate);
                        cycleEndAccounts.add(accountLine);
                    } /*
                       * else { List<String> updatedDayZeroAccounts = new ArrayList<>();
                       * updatedDayZeroAccounts.add(accountLine);
                       * 
                       * //Update the day zero account data file to temp file
                       * FileUtils.writeLines(dayZeroAccountsTempFile, updatedDayZeroAccounts, true); }
                       */
                    accountCount++;
                }
            } finally {
                it.close();
            }

            // Append to the deposit account
            if (cycleEndAccounts != null && cycleEndAccounts.size() > 0) {
                FileUtils.writeLines(depositAccountFile, cycleEndAccounts, true);
            }

            // Create the temporary file without the accounts that were used in cycle ends
            File dayZeroAccountsTempFile = new File(dataFolderPath + "/DayZeroAccountsTemp.DAT");
            if (dayZeroAccountsTempFile.exists()) {
                dayZeroAccountsTempFile.delete();
            }
            dayZeroAccountsTempFile.createNewFile();
            deleteLines(dataFolderPath, dayZeroAccountsFile, dayZeroAccountsTempFile, numberOfAccountsHavingCycleEnd);

            // Replace the original dayZeroAccountFile with the updated temp file
            dayZeroAccountsTempFile.renameTo(dayZeroAccountsFile);

        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
    }

    public static void generateDayZeroAccountDataFile(List<AccountData> accountDataList,
                                                      String dayEndDate,
                                                      String folderPath,
                                                      EntityType entityType,
                                                      File dayZeroAccountsFile) {
        long startTime = System.currentTimeMillis();
        List<String> dayZeroAccountLines = new ArrayList<>();
        // for (CustomerData customerData : chunkedCustomerData) {
        // List<AccountData> accountDataList = customerData.getAccountDataList();
        for (AccountData accountData : accountDataList) {
            if (accountData.getEntityType() == entityType) {
                dayZeroAccountLines.add(FileTemplates.generateDayZeroDepositAccount(accountData, dayEndDate));
            }
        }
        // }
        try {
            if (dayZeroAccountLines != null && dayZeroAccountLines.size() > 0) {
                if (!dayZeroAccountsFile.exists()) {
                    dayZeroAccountsFile.createNewFile();
                }
                FileUtils.writeLines(dayZeroAccountsFile, dayZeroAccountLines, true);
            }
            logger.debug("Generated " + dayZeroAccountLines.size() + SPACE + entityType + " accounts in " + (System.currentTimeMillis() - startTime) / 1000 + "s");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void generateAccountFeatureFile(List<AccountData> accountDataList, String dayEndDate, File accountFeatureFile, DayEndFileLineCounts dayEndFileLineCounts) {
        long startTime = System.currentTimeMillis();
        List<String> accountFeatureLines = new ArrayList<>();
        for (AccountData accountData : accountDataList) {
            if (accountData.getAccountsPTDGSpecification().getAccountFeatureSpecifications() != null
                    && accountData.getAccountsPTDGSpecification().getAccountFeatureSpecifications().size() > 0) {
                for (AccountFeaturePTDGSpecification accountFeaturePTDGSpecification : accountData.getAccountsPTDGSpecification().getAccountFeatureSpecifications()) {
                    DataGenerationContext.incrementAccountFeaturesGenerated();
                    dayEndFileLineCounts.incrementAccountFeaturesGenerated();
                    accountFeatureLines.add(FileTemplates.generateAccountFeature(accountData, dayEndDate, accountFeaturePTDGSpecification));
                }
            }
        }
        try {
            FileUtils.writeLines(accountFeatureFile, accountFeatureLines, true);
        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
        logger.debug("Generated Account Features in " + (System.currentTimeMillis() - startTime) / 1000 + "s");
    }

    public static void generateCustomerServiceFile(List<AccountRelationshipData> accountRelationshipDataList,
                                                   String dayEndDate,
                                                   File customerServiceFile,
                                                   DayEndFileLineCounts dayEndFileLineCounts,
                                                   long perGraphIterations) {

        long startTime = System.currentTimeMillis();
        List<String> lines = new ArrayList<>();
        int iter = 1;
        for (int i = 0; i < accountRelationshipDataList.size() && iter <= perGraphIterations; i++) {
            String line = FileTemplates.customerServiceInformationTemplate.replaceAll("\\{customerId\\}", accountRelationshipDataList.get(i).getCustomerId());
            line = line.replaceAll("\\{contractId\\}", generateCustomerServiceContractId());
            String serviceCode = PtDataGenerationConfig.getPaidServiceCodes();
            line = line.replaceAll("\\{serviceCode\\}", serviceCode);
            line = line.replaceAll("\\{optionCode\\}", PtDataGenerationConfig.getOptionCodesForPaidServices(serviceCode));
            line = line.replaceAll("\\{accountNumber\\}", accountRelationshipDataList.get(i).getAccountNumber());
            lines.add(line);
            dayEndFileLineCounts.incrementTotalcustomerServiceContractId();
            iter++;
        }
        try {
            FileUtils.writeLines(customerServiceFile, lines, true);
        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
        logger.debug("Generated " + lines.size() +
                " Customer Service Information in " + (System.currentTimeMillis() - startTime) / 1000 + "s");

    }

    public static void generateCustomerMergeFile(List<CustomerData> customerDataList,
                                                 String dayEndDate,
                                                 File customerMergeFile,
                                                 DayEndFileLineCounts dayEndFileLineCounts,
                                                 long perGraphIterations) {

        long startTime = System.currentTimeMillis();
        List<String> lines = new ArrayList<>();
        int iter = 1;
        for (int i = 0; i < customerDataList.size() && iter <= perGraphIterations; i++) {
            String line = FileTemplates.customerMergeInformationTemplate.replaceAll("\\{sourceCustomerId\\}", customerDataList.get(i).getCustomerId());
            line = line.replaceAll("\\{targetCustomerId\\}", customerDataList.get(i + 2).getCustomerId());
            lines.add(line);
            dayEndFileLineCounts.incrementTotalCustomerMergeGenerated();
            i = i + 2;
            iter++;
        }
        try {
            FileUtils.writeLines(customerMergeFile, lines, true);
        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
        logger.debug("Generated " + lines.size() +
                " Customer Merge Information in " + (System.currentTimeMillis() - startTime) / 1000 + "s");
    }

    public static void generateAccountServiceSubscribe(List<AccountData> accountData,
                                                       String dayEndDate,
                                                       File accountServiceSubscribeFile,
                                                       DayEndFileLineCounts dayEndFileLineCounts,
                                                       long perGraphInterations) {

        long startTime = System.currentTimeMillis();
        List<String> lines = new ArrayList<>();
        int iter = 1;
        for (int i = 0; i < accountData.size() && iter <= perGraphInterations; i++) {
            String accountServiceCode = getRandomValue(PtDataGenerationConfig.getAccountServiceCode(accountData.get(i).getAccountsPTDGSpecification().getProductCode()));
            if (accountServiceCode != null) {
                String line = FileTemplates.accountServiceSubsribeInfoTemplate.replaceAll("\\{accountNumber\\}", accountData.get(i).getAccountNumber());
                String subscribeID = generateAccountServiceSubscribeId();
                line = line.replaceAll("\\{subscribeId\\}", subscribeID);
                line = line.replaceAll("\\{serviceCode\\}", accountServiceCode);
                line = line.replaceAll("\\{billngPlan\\}", accountData.get(i).getAccountsPTDGSpecification().getBillingPlan());
                lines.add(line);
                dayEndFileLineCounts.incrementTotalAccountServiceSubscribeGenerated();
                iter++;
            }
        }
        try {
            FileUtils.writeLines(accountServiceSubscribeFile, lines, true);
        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
        logger.debug("Generated " + lines.size() +
                " Account Service Subscribe in " + (System.currentTimeMillis() - startTime) / 1000 + "s");

    }

    public static void generateAccountConsent(List<AccountRelationshipData> accountRelationshipDataList,
                                              String dayEndDate,
                                              File accountConsentFile,
                                              DayEndFileLineCounts dayEndFileLineCounts,
                                              long perGraphIterations) {

        long startTime = System.currentTimeMillis();
        List<String> lines = new ArrayList<>();
        int iter = 1;
        for (int i = 0; i < accountRelationshipDataList.size() && iter <= perGraphIterations; i++) {
            String line = FileTemplates.accountConsentInformationTemplate.replaceAll("\\{customerId\\}", accountRelationshipDataList.get(i).getCustomerId());
            line = line.replaceAll("\\{accountNumber\\}", accountRelationshipDataList.get(i).getAccountNumber());
            line = line.replaceAll("\\{consentCode\\}", getRandomValue(Arrays.asList("Y", "N")));
            lines.add(line);
            dayEndFileLineCounts.incrementAccountConsentGenerated();
            iter++;

        }
        try {
            FileUtils.writeLines(accountConsentFile, lines, true);
        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
        logger.debug("Generated " + lines.size() +
                " Account Consent Information in " + (System.currentTimeMillis() - startTime) / 1000 + "s");
    }

    public static void generateOfferEnrollment(List<CustomerData> customerDataList,
                                               String dayEndDate,
                                               File offerEnrollmentFile,
                                               DayEndFileLineCounts dayEndFileLineCounts,
                                               int perGraphIterations) {

        long startTime = System.currentTimeMillis();
        List<String> lines = new ArrayList<>();
        int iter = 1;
        for (int i = 0; i < customerDataList.size() && iter <= perGraphIterations; i++) {
            String line = FileTemplates.offerEnrollment.replaceAll("\\{customerId\\}", customerDataList.get(i).getCustomerId());
            lines.add(line);
            dayEndFileLineCounts.incrementTotalOfferEnrollmentGenerated();
            iter++;
        }
        try {
            FileUtils.writeLines(offerEnrollmentFile, lines, true);
        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
        logger.debug("Generated " + lines.size() +
                " Offer Enrollment in " + (System.currentTimeMillis() - startTime) / 1000 + "s");

    }

    public static void generateOptionSelector(List<AccountData> accountDataList,
                                              String dayEndDate,
                                              File optionSelectorFile,
                                              DayEndFileLineCounts dayEndFileLineCounts,
                                              long perGraphIterations) {

        long startTime = System.currentTimeMillis();
        List<String> lines = new ArrayList<>();
        int iter = 1;
        for (int i = 0; i < accountDataList.size() && iter <= perGraphIterations; i++) {
            if (EntityType.DEPOSIT_ACCOUNT == accountDataList.get(i).getEntityType()) {
                String line = FileTemplates.optionSelecter.replaceAll("\\{accountNumber\\}", accountDataList.get(i).getAccountNumber());
                line = line.replaceAll("\\{optionCode\\}", getRandomValue(PtDataGenerationConfig.getOptionCodes()));
                lines.add(line);
                dayEndFileLineCounts.incrementTotalOptionSelecterGenerated();
                iter++;
            }
        }
        try {
            FileUtils.writeLines(optionSelectorFile, lines, true);
        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
        logger.debug("Generated " + lines.size() +
                " option Selector in " + (System.currentTimeMillis() - startTime) / 1000 + "s");

    }

    public static void generateFeeExclusion(List<AccountData> accountDataList,
                                            String dayEndDate,
                                            File feeExclusionFile,
                                            DayEndFileLineCounts dayEndFileLineCounts,
                                            long perGraphIterations) {

        long startTime = System.currentTimeMillis();
        List<String> lines = new ArrayList<>();
        int iter = 1;
        for (int i = 0; i < accountDataList.size() && iter <= perGraphIterations; i++) {
            String line = FileTemplates.feeExclusion.replaceAll("\\{accountNumber\\}", accountDataList.get(i).getAccountNumber());
            line = line.replaceAll("\\{reason\\}", getRandomValue(PtDataGenerationConfig.getExceptionReasonCodes()));
            line = line.replaceAll("\\{dayEnd\\}", dayEndDate);

            lines.add(line);
            dayEndFileLineCounts.incrementTotalFeeExclusionGenerated();
            iter++;

        }
        try {
            FileUtils.writeLines(feeExclusionFile, lines, true);
        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
        logger.debug("Generated " + lines.size() +
                " fee Exclusion in " + (System.currentTimeMillis() - startTime) / 1000 + "s");

    }

    public static void generatePackageSubscription(List<AccountData> accountDataList,
                                                   String dayEndDate,
                                                   File packageSubscribeFile,
                                                   File packageAccuntInfoFile,
                                                   DayEndFileLineCounts dayEndFileLineCounts,
                                                   long perGraphIterations) {

        long startTime = System.currentTimeMillis();
        List<String> packageSubscribe = new ArrayList<>();
        List<String> packageAccountInfo = new ArrayList<>();
        String packageSubscribeId = null;
        int iter = 1;
        for (int i = 0; i < accountDataList.size() && iter <= perGraphIterations; i++) {
            String packageCode = PtDataGenerationConfig.getPackageCode(accountDataList.get(i).getAccountsPTDGSpecification().getProductCode());
            if (packageCode != null) {
                // Package Subscription
                packageSubscribeId = generatePackageSubscribeId();
                String line = FileTemplates.packageSubscribeTemplate.replaceAll("\\{subscribeId\\}", packageSubscribeId);
                line = line.replaceAll("\\{subscribeDate\\}", "20200101");
                line = line.replaceAll("\\{packageCode\\}", PtDataGenerationConfig.getPackageCode(accountDataList.get(i).getAccountsPTDGSpecification().getProductCode()));
                line = line.replaceAll("\\{accountNumber\\}", accountDataList.get(i).getAccountNumber());
                packageSubscribe.add(line);
                dayEndFileLineCounts.incrementTotalpackageSubscribeGenerated();

                // Package Account Subscribe
                String packageAccountInfoline = FileTemplates.packageAccountTemplate.replaceAll("\\{subscribeId\\}", packageSubscribeId);
                packageAccountInfoline = packageAccountInfoline.replaceAll("\\{subscribeDate\\}", "20200101");
                packageAccountInfoline = packageAccountInfoline.replaceAll("\\{packageCode\\}",
                                                                           PtDataGenerationConfig.getPackageCode(accountDataList.get(i)
                                                                                                                                     .getAccountsPTDGSpecification()
                                                                                                                                     .getProductCode()));
                packageAccountInfoline = packageAccountInfoline.replaceAll("\\{accountNumber\\}", accountDataList.get(i).getAccountNumber());
                packageAccountInfo.add(packageAccountInfoline);
                dayEndFileLineCounts.incrementTotalpackageAccountInfoGenerated();

                iter++;
            }

        }
        try {
            FileUtils.writeLines(packageSubscribeFile, packageSubscribe, true);
            FileUtils.writeLines(packageAccuntInfoFile, packageAccountInfo, true);
        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
        logger.debug("Generated Both" + 2 * packageSubscribe.size() +
                " Package Subscribe and Package account Information in " + (System.currentTimeMillis() - startTime) / 1000 + "s");

    }

    public static void generateBillingAccountGroup(List<AccountData> accountDataList,
                                                   String dayEndDate,
                                                   File billingAccountGroupfile,
                                                   File billingAccountGroupAccountfile,
                                                   DayEndFileLineCounts dayEndFileLineCounts,
                                                   long perGrahIterations) {

        long startTime = System.currentTimeMillis();
        List<String> billingAccountGroup = new ArrayList<>();
        List<String> billingAccountGroupAccount = new ArrayList<>();
        int iter = 1;
        for (int i = 0; i < accountDataList.size() && iter <= perGrahIterations; i++) {
            String billingGroupId = generateBillingGroupId();
            String line = FileTemplates.billingAccount.replaceAll("\\{billingId\\}", billingGroupId);
            billingAccountGroup.add(line);
            dayEndFileLineCounts.incrementTotalbillingAccountGroupGenerated();

            String line2 = FileTemplates.billingAccountGroupAccount.replaceAll("\\{billingId\\}", billingGroupId);
            line2 = line2.replaceAll("\\{accountNumber\\}", accountDataList.get(i).getAccountNumber());
            billingAccountGroupAccount.add(line2);
            dayEndFileLineCounts.incrementTotalbillingAccountGroupAccountGenerated();
        }
        try {
            FileUtils.writeLines(billingAccountGroupfile, billingAccountGroup, true);
            FileUtils.writeLines(billingAccountGroupAccountfile, billingAccountGroupAccount, true);
        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
        logger.debug("Generated " + 2 * billingAccountGroup.size() +
                " Both Billing Account and Group Account in " + (System.currentTimeMillis() - startTime) / 1000 + "s");

    }

    public static void generateManualExceptionFile(List<AccountData> accountDataList,
                                                   String dayEndDate,
                                                   File manualExceptionFile,
                                                   DayEndFileLineCounts dayEndFileLineCounts,
                                                   long perGraphIterations) {
        long startTime = System.currentTimeMillis();
        List<String> manualExceptionLines = new ArrayList<>();
        int iter = 1;
        for (int i = 0; i < accountDataList.size() && iter <= perGraphIterations; i++) {
            if (accountDataList.get(i).getAccountsPTDGSpecification().getManualExceptionPTDGSpecifications() != null
                    && accountDataList.get(i).getAccountsPTDGSpecification().getManualExceptionPTDGSpecifications().size() > 0) {
                for (ManualExceptionPTDGSpecification manualExceptionPTDGSpecification : accountDataList.get(i)
                                                                                                        .getAccountsPTDGSpecification()
                                                                                                        .getManualExceptionPTDGSpecifications()) {
                    DataGenerationContext.incrementManualExceptionsGenerated();
                    dayEndFileLineCounts.incrementManualExceptionsGenerated();
                    manualExceptionLines.add(FileTemplates.generateManualException(accountDataList.get(i), dayEndDate, manualExceptionPTDGSpecification));
                    iter++;
                }
            }
        }

        try {
            FileUtils.writeLines(manualExceptionFile, manualExceptionLines, true);
        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
        logger.debug("Generated Manual Exceptions in " + (System.currentTimeMillis() - startTime) / 1000 + "s");
    }

    public static void createAllFilesForDay(String inputsFolderPath, String dayEndDate) {
        createFile(inputsFolderPath, EntityType.ACCOUNT_BALANCE, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.ACCOUNT_FEATURE, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.BRANCH, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.CREDIT_CARD_ACCOUNT, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.CREDIT_CARD_TRANSACTION, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.CUSTOMER, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.CUSTOMER_ACCOUNT_RELATIONSHIP, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.CUSTOMER_SERVICE, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.DEPOSIT_ACCOUNT, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.DEPOSIT_TRANSACTION, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.LOAN_ACCOUNT, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.SECURITIES_ACCOUNT, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.LOAN_TRANSACTION, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.MANUAL_EXCEPTION, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.MORTGAGE_ACCOUNT, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.MUTUAL_FUND_ACCOUNT, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.OFFER_ENROLLMENT, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.OFFER_UNENROLLMENT, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.TIME_DEPOSIT_ACCOUNT, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.CUSTOMER_MERGE, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.ACCOUNT_SERVICE_SUBSCRIPTION, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.ACCOUNT_CONSENT, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.PACKAGE_SUBSCRIPTION, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.PACKAGE_ACCOUNT, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.ACCOUNT_IDENTIFIER_CHANGE, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.FEE_EXCLUSION, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.BILLING_ACCOUNT_GROUP, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.BILLING_ACCOUNT_GROUP_ACCOUNT, dayEndDate, true);
        createFile(inputsFolderPath, EntityType.OPTION_SELECTOR, dayEndDate, true);
        // Generate trigger file
        generateTriggerFile(inputsFolderPath, dayEndDate);
    }

    public static File createFile(String folderPath, String fileName) {
        try {
            String fullFileName = folderPath + "/" + fileName;
            File file = new File(fullFileName);
            if (file.exists()) {
                file.delete();
                file.createNewFile();
            }
            logger.debug("Created empty file: " + QUOTE + fileName + QUOTE);
            return file;
        } catch (Exception e) {
            throw new TestingToolsRuntimeException(e);
        }
    }

    public static File createFile(String inputsFolderPath, EntityType inputFileType, String dayEndDate, boolean createHeader) {
        try {
            String fileName = inputsFolderPath + "/" + inputFileType.getFilePrefix() + UNDERSCORE + dayEndDate + DAT_FILE_EXTENSION;
            File file = new File(fileName);
            if (file.exists()) {
                file.delete();
                file.createNewFile();
            }
            logger.debug("Created empty file: " + QUOTE + fileName + QUOTE);
            if (createHeader) {
                List<String> lines = new ArrayList<>();
                lines.add("#|" + dayEndDate + "|0");
                FileUtils.writeLines(file, lines);
            }
            return file;
        } catch (Exception e) {
            throw new TestingToolsRuntimeException(e);
        }
    }

    public static void generateRelationshipFile(List<AccountRelationshipData> accountRoleDataList,
                                                String string,
                                                File relationshipFile,
                                                DayEndFileLineCounts dayEndFileLineCounts) {
        long startTime = System.currentTimeMillis();
        List<String> lines = new ArrayList<>();
        for (AccountRelationshipData accountRoleData : accountRoleDataList) {

            DataGenerationContext.incrementCustomerAccountRelationshipsGenerated();
            dayEndFileLineCounts.incrementCustomerAccountRelationshipsGenerated();

            String line = FileTemplates.relationshipDataTemplate.replaceAll("\\{accountNumber\\}", accountRoleData.getAccountNumber());
            line = line.replaceAll("\\{customerId\\}", accountRoleData.getCustomerId());
            line = line.replaceAll("\\{role\\}", accountRoleData.getRole());
            lines.add(line);
        }
        try {
            FileUtils.writeLines(relationshipFile, lines, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.debug("Generated " + lines.size() + " Relationships in " + (System.currentTimeMillis() - startTime) / 1000 + "s");
    }

    public static List<Long> chunkNumber(long totalCustomerCount, long chunkSize) {
        List<Long> chunks = new ArrayList<>();
        int numOfChunks = (int) Math.ceil((double) totalCustomerCount / chunkSize);
        for (int i = 0; i < numOfChunks; ++i) {
            long start = i * chunkSize;
            long length = Math.min(totalCustomerCount - start, chunkSize);
            chunks.add(new Long(length));
        }
        return chunks;
    }

    public static int generateCustomerFile(List<CustomerData> chunkedCustomerData, String dayEndDate, File customerFile) {
        long startTime = System.currentTimeMillis();
        List<String> lines = new ArrayList<>();
        for (CustomerData customerData : chunkedCustomerData) {
            String customerSegment = customerData.getCustomerPTDGSpecification().getCustomerSegment();
            String customerCohort = customerData.getCustomerPTDGSpecification().getCustomerCohortCode();
            String empStatus = customerData.getCustomerPTDGSpecification().getCustomerEmployeeStatus();
            customerData.setCustomerSegment(customerSegment);
            customerData.setCustomerCohort(customerCohort);
            customerData.setCustomerEmployeeStatus(empStatus);
            lines.add(FileTemplates.generateCustomer(customerData, dayEndDate));
        }
        try {
            FileUtils.writeLines(customerFile, lines, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.debug("Generated " + lines.size() + " Customers in " + (System.currentTimeMillis() - startTime) / 1000 + "s");
        return lines.size();
    }

    public static String getRandomValue(List<String> valueSet) {
        Random rand = new Random();
        return valueSet == null ? null : valueSet.get(rand.nextInt(valueSet.size()));
    }

    // Returns a list of CustomerData.
    public static CustomerAccountData generateChunkCustomerAndAccountData(long chunk, CustomerPTDGSpecification customerPTDGSpecification) {
        long startTime = System.currentTimeMillis();
        CustomerAccountData customerAccountData = new CustomerAccountData();
        // List<CustomerData> customerDataList = new ArrayList<>();
        List<AccountsPTDGSpecification> accountsPTDGSpecifications = customerPTDGSpecification.getAccountsPTDGSpecifications();

        List<String> customerIds = generateCustomerIds(chunk);
        for (int i = 0; i < customerIds.size(); i++) {
            CustomerData customerData = new CustomerData();
            customerData.setCustomerId(customerIds.get(i));
            customerData.setCustomerPTDGSpecification(customerPTDGSpecification);
            if (i < 2)
                customerData.setCustomerMerged(true);
            for (AccountsPTDGSpecification accountsPTDGSpecification : accountsPTDGSpecifications) {
                int numberOfAccounts = accountsPTDGSpecification.getNumberOfAccounts();
                for (int j = 0; j < numberOfAccounts; j++) {
                    AccountData accountData = generateChunkAccountData(customerIds.get(i), accountsPTDGSpecification);
                    customerAccountData.getAccountDataList().add(accountData);
                    if (accountData.getAccountStatus() == "ACTIVE" && !(customerData.isCustomerMerged())) {
                        customerAccountData.getAccountRelationshipDataList()
                                           .add(new AccountRelationshipData("SOLE",
                                                                            accountData.getAccountNumber(),
                                                                            customerData.getCustomerId()));
                    }
                }
            }
            customerAccountData.getCustomerDataList().add(customerData);
        }
        logger.debug("Generate Identifiers: " + (System.currentTimeMillis() - startTime) / 1000 + "s");
        return customerAccountData;
    }

    // Returns a list of CustomerData.
    public static CustomerAccountData generateChunkCustomerAndAccountData(long chunk, CustomerGraphPTDGSpecification customerGraphPTDGSpecification) {
        long startTime = System.currentTimeMillis();

        CustomerAccountData customerAccountData = new CustomerAccountData();

        List<AccountsPTDGSpecification> accountProfiles = customerGraphPTDGSpecification.getAccountProfiles();
        List<CustomerPTDGSpecification> customerProfiles = customerGraphPTDGSpecification.getCustomerProfiles();

        // Start from the customer account relationship
        // Get customer profile,

        for (long l = 0; l < chunk; l++) {

            String customerGraphId = generateCustomerIdString(DataGenerationContext.incrementCustomerGraphId());

            List<CustomerData> customerDataList = new ArrayList<>();
            List<AccountData> accountDataList = new ArrayList<>();
            List<AccountRelationshipData> accountRelationshipDataList = new ArrayList<>();

            // Generate customers
            for (CustomerPTDGSpecification customerPTDGSpecification : customerProfiles) {
                String customerId = generateCustomerIdString(DataGenerationContext.incrementCustomerId());
                CustomerData customerData = new CustomerData();
                customerData.setCustomerId(customerId);
                customerData.setCustomerPTDGSpecification(customerPTDGSpecification);
                customerData.setCustomerProfileName(customerPTDGSpecification.getCustomerProfileName());
                customerDataList.add(customerData);
            }

            // Generate accounts
            for (AccountsPTDGSpecification accountsPTDGSpecification : accountProfiles) {
                AccountData accountData = generateChunkAccountData(customerGraphId, accountsPTDGSpecification);
                accountDataList.add(accountData);
            }

            List<CustomerAccountRelationshipPTDGSpecification> customerAccountRelationships = customerGraphPTDGSpecification.getCustomerAccountRelationships();
            for (CustomerAccountRelationshipPTDGSpecification customerAccountRelationshipPTDGSpecification : customerAccountRelationships) {
                CustomerData customerData = getCustomerDataForProfileName(customerDataList, customerAccountRelationshipPTDGSpecification.getCustomerProfileName());
                AccountData accountData = getAccountDataForProfileName(accountDataList, customerAccountRelationshipPTDGSpecification.getAccountProfileName());
                if (customerData != null && accountData != null) {
                    accountRelationshipDataList.add(new AccountRelationshipData(
                                                                                customerAccountRelationshipPTDGSpecification.getRole(),
                                                                                accountData.getAccountNumber(),
                                                                                customerData.getCustomerId()));
                }
            }

            customerAccountData.getCustomerDataList().addAll(customerDataList);
            customerAccountData.getAccountDataList().addAll(accountDataList);
            customerAccountData.getAccountRelationshipDataList().addAll(accountRelationshipDataList);
        }

        logger.debug("Generate Identifiers: " + (System.currentTimeMillis() - startTime) / 1000 + "s");
        return customerAccountData;
    }

    public static CustomerData getCustomerDataForProfileName(List<CustomerData> customerDataList, String profileName) {
        for (CustomerData customerData : customerDataList) {
            if (customerData.getCustomerProfileName().equals(profileName)) {
                return customerData;
            }
        }
        return null;
    }

    public static AccountData getAccountDataForProfileName(List<AccountData> accountDataList, String profileName) {
        for (AccountData accountData : accountDataList) {
            if (accountData.getAccountProfileName().equals(profileName)) {
                return accountData;
            }
        }
        return null;
    }

    public static AccountData generateChunkAccountData(String customerId, AccountsPTDGSpecification accountsPTDGSpecification) {
        EntityType entityType = accountsPTDGSpecification.getEntityType();
        String accountNumnber = generateAccountNumberString(DataGenerationContext.incrementAccountId(entityType), customerId, entityType);

        AccountData accountData = new AccountData();
        accountData.setAccountNumber(accountNumnber);
        accountData.setEntityType(entityType);
        accountData.setAccountsPTDGSpecification(accountsPTDGSpecification);
        accountData.setAccountProfileName(accountsPTDGSpecification.getAccountProfileName());
        accountData.setAccountStatus(accountsPTDGSpecification.getAccountStatus());
        accountData.setInGoodStanding(accountsPTDGSpecification.getInGoodStanding());
        return accountData;
    }

    public static String generateAccountNumberString(long customerIdIndex, String customerId, EntityType entityType) {
        String prefix = entityType.getIdPrefix();
        String accountNumber = "" + customerIdIndex;
        accountNumber = StringUtils.leftPad(accountNumber, 9, ZERO);
        return prefix + customerId + ACCOUNT_PREFIX + accountNumber;
    }

    public static List<String> generateCustomerIds(long numberOfCustomers) {
        List<String> customerIds = new ArrayList<>();
        for (int i = 0; i < numberOfCustomers; i++) {
            String customerId = generateCustomerIdString(DataGenerationContext.incrementCustomerId());
            customerIds.add(customerId);
        }
        return customerIds;
    }

    public static String generateCustomerIdString(long customerIdIndex) {
        String customerId = "" + customerIdIndex;
        customerId = StringUtils.leftPad(customerId, 9, ZERO);
        return CUSTOMER_PREFIX + customerId;
    }

    public static String generateCustomerServiceContractId() {
        String customerServiceContractId = String.valueOf(DataGenerationContext.incrementcustomerServiceContractId());
        customerServiceContractId = StringUtils.leftPad(customerServiceContractId, 6, ZERO);
        return customerServiceContractId;
    }

    public static String generateAccountServiceSubscribeId() {
        String accountServiceSubscribeId = String.valueOf(DataGenerationContext.incrementAccountServiceSubscribeId());
        accountServiceSubscribeId = StringUtils.leftPad(accountServiceSubscribeId, 6, ZERO);
        return accountServiceSubscribeId;
    }

    public static String generatePackageSubscribeId() {
        String packageSubscribeId = String.valueOf(DataGenerationContext.incrementTotalpackageSubscribeGenerated());
        packageSubscribeId = StringUtils.leftPad(packageSubscribeId, 6, ZERO);
        return packageSubscribeId;
    }

    public static String generateBillingGroupId() {
        String billingGroupId = String.valueOf(DataGenerationContext.incrementBillingGroupId());
        billingGroupId = StringUtils.leftPad(billingGroupId, 6, ZERO);
        return billingGroupId;
    }

    public static void cleanUpLastLineInFile(File file) {
        try {
            RandomAccessFile rafile = new RandomAccessFile(file, "rw");
            long length = rafile.length();
            rafile.setLength(length - 1);
            rafile.close();
        } catch (Exception e) {
            throw new TestingToolsRuntimeException(e);
        }
    }

    public static String setupDataFolder(String folderPath) {
        File dataFolder = null;
        try {
            if (folderPath == null) {
                folderPath = ".";
            }
            File parentFolder = new File(folderPath);
            if (parentFolder.canWrite()) {
                // Delete existing data folder
                String pathToDataFolder = parentFolder.getAbsolutePath() + "/" + "data";
                dataFolder = new File(pathToDataFolder);
                if (dataFolder.exists()) {
                    logger.debug("Data folder '" + pathToDataFolder + "' exists, deleting");
                    FileUtils.deleteDirectory(dataFolder);
                    logger.debug("Data folder deleted successfully");
                    // Creating an empty data folder
                    dataFolder.mkdir();
                    logger.debug("Created new data folder successfully");
                } else {
                    logger.debug("Data folder '" + pathToDataFolder + "' does not exists, creating one");
                    dataFolder.mkdir();
                    logger.debug("Data folder created successfully");
                }
            } else {
                throw new TestingToolsRuntimeException("Cannot write to directory: " + QUOTE + parentFolder.getAbsolutePath() + QUOTE);
            }
        } catch (Exception e) {
            throw new TestingToolsRuntimeException(e);
        }
        return dataFolder.getAbsolutePath();
    }

    public static String setupDayEndFolder(String dataFolderPath, String dayEndDate) {
        File dayEndDateFolder = new File(dataFolderPath + "/" + dayEndDate);
        dayEndDateFolder.mkdir();
        logger.debug("Created folder '" + dayEndDateFolder.getAbsolutePath() + "' successfully");

        String dayEndDateFolderPath = dayEndDateFolder.getAbsolutePath();

        File inputsFolder = new File(dayEndDateFolderPath + "/inputs");
        inputsFolder.mkdir();
        logger.debug("Created folder '" + inputsFolder.getAbsolutePath() + "' successfully");

        File outputsFolder = new File(dayEndDateFolderPath + "/outputs");
        outputsFolder.mkdir();
        logger.debug("Created folder '" + outputsFolder.getAbsolutePath() + "' successfully");

        File resultsFolder = new File(dayEndDateFolderPath + "/results");
        resultsFolder.mkdir();
        logger.debug("Created folder '" + resultsFolder.getAbsolutePath() + "' successfully");

        File verifyFolder = new File(dayEndDateFolderPath + "/verify");
        verifyFolder.mkdir();
        logger.debug("Created folder '" + verifyFolder.getAbsolutePath() + "' successfully");

        return dayEndDateFolderPath;
    }

    public static void insertFileHeader(File file, String dayEndDate, long count) {
        long startTime = System.currentTimeMillis();
        BufferedReader reader = null;
        BufferedWriter writer = null;
        try {
            File tempFileName = new File(file.getParentFile().getAbsolutePath() + "/TEMP_" + file.getName());
            reader = new BufferedReader(new FileReader(file));
            writer = new BufferedWriter(new FileWriter(tempFileName));
            String fileHeaderRow = "#|" + dayEndDate + "|" + count;
            writer.write(fileHeaderRow + "\n");
            String tmp;
            while ((tmp = reader.readLine()) != null) {
                writer.write(tmp + "\n");
            }

            file.delete();
            tempFileName.renameTo(file);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        cleanUpLastLineInFile(file);
        logger.debug("Inserted header into " + file.getName() + " in " + (System.currentTimeMillis() - startTime) / 1000 + "s");
    }

    public static void generateDepositAccountFileForMonthEnd(File monthEndDepositAccountFile, File initialDayEndDepositAccountFile, String monthEndDayEndDate) {
        try {
            // Copy the content of the file, but replace the account cycle end date
            FileInputStream fis = new FileInputStream(initialDayEndDepositAccountFile);
            BufferedReader in = new BufferedReader(new InputStreamReader(fis));
            FileWriter fstream = new FileWriter(monthEndDepositAccountFile);
            BufferedWriter out = new BufferedWriter(fstream);
            String aLine = null;
            while ((aLine = in.readLine()) != null) {
                // Replace date in the header line
                // Replace the account cycle end date for all the other lines
                out.write(aLine);
                out.newLine();
            }
            // do not forget to close the buffer reader
            in.close();
            // close buffer writer
            out.close();
        } catch (Exception e) {
            throw new TestingToolsRuntimeException(e);
        }
    }

    public static void generateTriggerFile(String inputsFolderPath, String dayEndDate) {
        String triggerFileName = inputsFolderPath + "/" + START_DAY_END + UNDERSCORE + dayEndDate + TRIGGER_FILE_EXTENSION;
        try {
            new File(triggerFileName).createNewFile();
        } catch (IOException e) {
            throw new TestingToolsRuntimeException(e);
        }
    }

    public static void createBranchFile(File branchFile, List<BranchPTDGSpecification> branchSpecifications) {
        long startTime = System.currentTimeMillis();
        List<String> lines = new ArrayList<>();
        for (BranchPTDGSpecification branchPTDGSpecification : branchSpecifications) {
            DataGenerationContext.incrementBranchesGenerated();
            lines.add(FileTemplates.generateBranch(branchPTDGSpecification));
        }
        try {
            FileUtils.writeLines(branchFile, lines, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.debug("Generated " + lines.size() + " Branches in " + (System.currentTimeMillis() - startTime) / 1000 + "s");
    }

    public static boolean deleteLines(String dataFolderPath, File inputFile, File tempFile, long numberOfLinesToDelete) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

            String currentLine;
            long numberOfLinesSkipped = 0;
            while ((currentLine = reader.readLine()) != null) {
                // trim newline when comparing with lineToRemove
                if (numberOfLinesSkipped < numberOfLinesToDelete) {
                    numberOfLinesSkipped++;
                    continue;
                }
                writer.write(currentLine + System.getProperty("line.separator"));
            }
            writer.close();
            reader.close();
            return tempFile.renameTo(inputFile);
        } catch (Exception e) {
            throw new TestingToolsRuntimeException(e);
        }

    }

    public static <T> List<T> getRandomListData(long newListDataSize, List<T> listData) {
        Random rand = new Random();
        int[] randIntegersArray = rand.ints(0, listData.size()).distinct().limit(newListDataSize).toArray();
        List<T> newListData = new ArrayList<>();
        for (int i = 0; i < randIntegersArray.length; i++) {
            newListData.add(listData.get(randIntegersArray[i]));
        }
        return newListData;

    }

}
