package com.zafin.rpe.test.tools.generator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.swing.text.DefaultEditorKit.CutAction;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Generates data across one or more days based on the configuration
 *
 */
public class PerformanceDataGenerator {

    private static final int CHUNK_SIZE = 100000000;
    private static final Logger logger = LoggerFactory.getLogger(PerformanceDataGenerator.class);
    //Multiplication factor to generate 12500000 customers
    private static int factor = 100000;

    public void generateData(PTDGSpecification specification, String folderPath) {
        String dataFolderPath = PTDGUtils.setupDataFolder(folderPath);
        List<DayEndSpecification> dayEndSpecifications = specification.getCustomPTDGSpecification().getDayEndSpecifications();
        for (DayEndSpecification dayEndSpecification : dayEndSpecifications) {
            if (dayEndSpecification.getDayEndDate() != "20200101")
                // Factor for remaining days
                factor = factor / 2;
            generateAllFilesForDay(dayEndSpecification, dataFolderPath, specification.getCustomPTDGSpecification().getBranchPTDGSpecifications());
        }

        // Create run.order
        generateRunOrderFile(dataFolderPath, dayEndSpecifications);

        // Create zrpe_runtime_system_date_update.sql
        generateRuntimeSystemDataUpdateFile(dataFolderPath, specification.getSystemDate());

        logger.debug("Program execution completed");
        logger.debug("Successfully data generated");
    }

    private void generateRuntimeSystemDataUpdateFile(String dataFolderPath, String systemDate) {
        String sql = "update mi_system set curr_date=TO_DATE('" + systemDate + "', 'DD-MON-YYYY');";
        File file = PTDGUtils.createFile(dataFolderPath, "zrpe_runtime_system_date_update.sql");
        List<String> lines = new ArrayList<>();
        lines.add(sql);
        try {
            FileUtils.writeLines(file, lines, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void generateRunOrderFile(String dataFolderPath, List<DayEndSpecification> dayEndSpecifications) {
        File file = PTDGUtils.createFile(dataFolderPath, "run.order");
        List<String> lines = new ArrayList<>();
        for (DayEndSpecification dayEndSpecification : dayEndSpecifications) {
            lines.add(dayEndSpecification.getDayEndDate());
        }
        try {
            FileUtils.writeLines(file, lines, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void generateAllFilesForDay(DayEndSpecification dayEndSpecification, String dataFolderPath, List<BranchPTDGSpecification> branchPTDGSpecifications) {

        String dayEndDate = dayEndSpecification.getDayEndDate();
        String dayEndFolderPath = PTDGUtils.setupDayEndFolder(dataFolderPath, dayEndDate);
        String dayInputsFolderPath = dayEndFolderPath + "/inputs";

        PTDGUtils.createAllFilesForDay(dayInputsFolderPath, dayEndDate);

        // Create customer
        File dayEndCustomerFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.CUSTOMER, dayEndDate, false);
        // Create relationship files
        File dayEndRelationshipFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.CUSTOMER_ACCOUNT_RELATIONSHIP, dayEndDate, false);
        // Create Customer Merge files
        File dayEndCustomerMergeFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.CUSTOMER_MERGE, dayEndDate, false);
        //// Create Customer Service Information files
        File dayEndCustomerServiceFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.CUSTOMER_SERVICE, dayEndDate, false);
        // Create account files
        Map<EntityType, File> dayEndAccountFiles = new HashMap<>();
        // Create account balance file
        File dayEndAccountBalanceFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.ACCOUNT_BALANCE, dayEndDate, false);
        // Create account Feature file
        File dayAccountFeatureFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.ACCOUNT_FEATURE, dayEndDate, false);
        // Create account Identifier change file
        File dayEndAccountIdentifierChangeFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.ACCOUNT_IDENTIFIER_CHANGE, dayEndDate, false);
        // Create manual exception file
        File manualExcetionFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.MANUAL_EXCEPTION, dayEndDate, false);
        // Create Account Service Subscription
        File dayEndAccountServiceSubscribe = PTDGUtils.createFile(dayInputsFolderPath, EntityType.ACCOUNT_SERVICE_SUBSCRIPTION, dayEndDate, false);
        // Create Account Consent Subscription
        File dayEndAccountConsent = PTDGUtils.createFile(dayInputsFolderPath, EntityType.ACCOUNT_CONSENT, dayEndDate, false);
        // Package Subscribe file
        File dayEndPackageSubscribeFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.PACKAGE_SUBSCRIPTION, dayEndDate, false);
        // Package Account Info file
        File dayEndPackageAccountInfoFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.PACKAGE_ACCOUNT, dayEndDate, false);
        // Create OfferEnrollment file
        File dayEndOfferEnrollmentFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.OFFER_ENROLLMENT, dayEndDate, false);
        // create FeeExclusion file
        File dayEndFeeExclusionFIle = PTDGUtils.createFile(dayInputsFolderPath, EntityType.FEE_EXCLUSION, dayEndDate, false);
        // Create OptionSelecter File
        File dayEndOptionSelector = PTDGUtils.createFile(dayInputsFolderPath, EntityType.OPTION_SELECTOR, dayEndDate, false);
        // Create BillingAccount Group file
        File dayEndBillingAccountGroupfile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.BILLING_ACCOUNT_GROUP, dayEndDate, false);
        // Create BillingAccount Group Account file
        File dayEndBillingAccountGroupAccountfile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.BILLING_ACCOUNT_GROUP_ACCOUNT, dayEndDate, false);

        // Create transaction files
        Map<EntityType, File> dayEndTransactionFiles = new HashMap<>();

        boolean dayZero = dayEndSpecification.isDayZero();
        File dayZeroAccountsFile = new File(dataFolderPath + "/DayZeroDepositAccounts.DAT");
        File dayZeroaccountBalancesFile = new File(dataFolderPath + "/DayZeroAccountBalances.DAT");

        // boolean generateFeeFile = dayEndSpecification.isGenerateFeeFile();
        // File feeFile = new File(dataFolderPath + "/Fee.DAT");
        // if (!feeFile.exists()) {
        // try {
        // feeFile.createNewFile();
        // } catch (IOException e) {
        // e.printStackTrace();
        // }
        // }

        DayEndFileLineCounts dayEndFileLineCounts = new DayEndFileLineCounts();

        long startTime = System.currentTimeMillis();
        List<CustomerPTDGSpecification> customerPTDGSpecifications = dayEndSpecification.getCustomerPTDGSpecifications();

        for (int i = 0; i < customerPTDGSpecifications.size(); i++) {
            CustomerPTDGSpecification customerPTDGSpecification = customerPTDGSpecifications.get(i);
            long numOfCustomers = customerPTDGSpecification.getNumberOfCustomers();
            long chunksProcessed = 0;
            List<Long> chunks = PTDGUtils.chunkNumber(numOfCustomers, CHUNK_SIZE);
            for (Long chunk : chunks) {
                // logger.info("Generating " + chunk + " customers and all its related records ... ");
                long chunkStartTime = System.currentTimeMillis();
                logger.info("******************************************");

                CustomerAccountData chunkedCustomeAccountData = PTDGUtils.generateChunkCustomerAndAccountData(chunk, customerPTDGSpecification);
                List<CustomerData> chunkedCustomerData = chunkedCustomeAccountData.getCustomerDataList();
                List<AccountData> accountDataList = chunkedCustomeAccountData.getAccountDataList();
                List<AccountRelationshipData> accountRelationshipDataList = chunkedCustomeAccountData.getAccountRelationshipDataList();

                // Generate files for initial day end
                int numberOfCustomersAdded = PTDGUtils.generateCustomerFile(chunkedCustomerData, dayEndDate, dayEndCustomerFile);
                dayEndFileLineCounts.incrementCustomers(numberOfCustomersAdded);

                // Generate Customer merge files
                logger.debug("***Generate Customer merge files***");
                PTDGUtils.generateCustomerMergeFile(chunkedCustomerData, dayEndDate, dayEndCustomerMergeFile, dayEndFileLineCounts, 5);

                dayEndFileLineCounts.incrementCreditCardAccounts(PTDGUtils.generateAccountFile(accountDataList,
                                                                                               dayEndDate,
                                                                                               dayEndAccountFiles,
                                                                                               dayInputsFolderPath,
                                                                                               EntityType.CREDIT_CARD_ACCOUNT));

                dayEndFileLineCounts.incrementDepositAccounts(PTDGUtils.generateAccountFile(accountDataList,
                                                                                            dayEndDate,
                                                                                            dayEndAccountFiles,
                                                                                            dayInputsFolderPath,
                                                                                            EntityType.DEPOSIT_ACCOUNT));

                dayEndFileLineCounts.incrementTimeDepositAccounts(PTDGUtils.generateAccountFile(accountDataList,
                                                                                                dayEndDate,
                                                                                                dayEndAccountFiles,
                                                                                                dayInputsFolderPath,
                                                                                                EntityType.TIME_DEPOSIT_ACCOUNT));

                dayEndFileLineCounts.incrementLoanAccounts(PTDGUtils.generateAccountFile(accountDataList,
                                                                                         dayEndDate,
                                                                                         dayEndAccountFiles,
                                                                                         dayInputsFolderPath,
                                                                                         EntityType.LOAN_ACCOUNT));

                dayEndFileLineCounts.incrementMutualFundAccounts(PTDGUtils.generateAccountFile(accountDataList,
                                                                                               dayEndDate,
                                                                                               dayEndAccountFiles,
                                                                                               dayInputsFolderPath,
                                                                                               EntityType.MUTUAL_FUND_ACCOUNT));
                dayEndFileLineCounts.incrementMortgageAccounts(PTDGUtils.generateAccountFile(accountDataList,
                                                                                             dayEndDate,
                                                                                             dayEndAccountFiles,
                                                                                             dayInputsFolderPath,
                                                                                             EntityType.MORTGAGE_ACCOUNT));
                dayEndFileLineCounts.incrementSecuritiesAccountGenerated(PTDGUtils.generateAccountFile(accountDataList,
                                                                                                       dayEndDate,
                                                                                                       dayEndAccountFiles,
                                                                                                       dayInputsFolderPath,
                                                                                                       EntityType.SECURITIES_ACCOUNT));

                // Generate account features file
                logger.debug("***Generating account features***");
                PTDGUtils.generateAccountFeatureFile(accountDataList, dayEndDate, dayAccountFeatureFile, dayEndFileLineCounts);
                // Generate account balance file
                logger.debug("***Generating account balances***");
                PTDGUtils.generateAccounBalanceFile(accountDataList, dayEndDate, dayEndAccountBalanceFile, dayEndFileLineCounts);

                logger.debug("***Generating transaction files***");
                PTDGUtils.generateTransactionFiles(accountDataList, dayEndDate, dayEndTransactionFiles, dayInputsFolderPath, dayEndFileLineCounts, null);
                logger.debug("***Generating relationship files***");
                PTDGUtils.generateRelationshipFile(accountRelationshipDataList, dayEndDate, dayEndRelationshipFile, dayEndFileLineCounts);

                if (dayZero) {
                    logger.debug("***Generating day zero account data file***");
                    PTDGUtils.generateDayZeroAccountDataFile(accountDataList,
                                                             dayEndDate,
                                                             dayInputsFolderPath,
                                                             EntityType.DEPOSIT_ACCOUNT,
                                                             dayZeroAccountsFile);
                    logger.debug("***Generating day zero account balances file***");
                    PTDGUtils.generateDayZeroAccountBalancesFile(accountDataList,
                                                                 dayEndDate,
                                                                 dayZeroaccountBalancesFile);
                }

                printCurrentTotals(dayEndFileLineCounts);
                chunksProcessed += chunk;
                logger.info("Generated chunk in " + (System.currentTimeMillis() - chunkStartTime) / 1000 + "s");
                if (chunksProcessed < numOfCustomers) {
                    logger.debug("...processed {} chunks so far", chunksProcessed);
                } else {
                    logger.debug("Processed all {} chunks of customer data", numOfCustomers);
                }
                logger.info("******************************************");
            }

            logger.info("!!!Processed customerSpecification # {}. {} left", i + 1, customerPTDGSpecifications.size() - i - 1);
        }

        List<CustomerGraphPTDGSpecification> customerGraphPTDGSpecifications = dayEndSpecification.getCustomerGraphPTDGSpecifications();
        List<List<CustomerData>> chunkedCustomerDataList = new ArrayList<List<CustomerData>>();
        List<List<AccountData>> chunkedaccountDataList = new ArrayList<List<AccountData>>();
        List<List<AccountRelationshipData>> chunkedaccountRelation = new ArrayList<List<AccountRelationshipData>>();
        int numofProfiles = 0;
        for (CustomerGraphPTDGSpecification customerGraphPTDGSpecification : customerGraphPTDGSpecifications) {
            numofProfiles = numofProfiles + 1;
            List<Long> chunks = PTDGUtils.chunkNumber(customerGraphPTDGSpecification.getNumberOfCustomerGraphs(), CHUNK_SIZE);

            for (Long chunk : chunks) {
                logger.info("Generateing " + chunk + " customer graphs and all its related records ... ");
                long chunkStartTime = System.currentTimeMillis();
                logger.info("******************************************");

                CustomerAccountData customerAccountData = PTDGUtils.generateChunkCustomerAndAccountData(chunk, customerGraphPTDGSpecification);
                List<CustomerData> chunkedCustomerData = customerAccountData.getCustomerDataList();

                List<AccountData> accountDataList = customerAccountData.getAccountDataList();

                List<AccountRelationshipData> accountRelationshipDataList = customerAccountData.getAccountRelationshipDataList();

                // Generate files for initial day end

                int numberOfCustomersAdded = PTDGUtils.generateCustomerFile(chunkedCustomerData, dayEndDate, dayEndCustomerFile);
                dayEndFileLineCounts.incrementCustomers(numberOfCustomersAdded);

                if (numofProfiles > 12) {
                    logger.debug("***Generate Customer merge files***");
                    PTDGUtils.generateCustomerMergeFile(chunkedCustomerData, dayEndDate, dayEndCustomerMergeFile, dayEndFileLineCounts, Math.round(factor * 0.25));
                    break;
                }

                dayEndFileLineCounts.incrementCreditCardAccounts(PTDGUtils.generateAccountFile(accountDataList,
                                                                                               dayEndDate,
                                                                                               dayEndAccountFiles,
                                                                                               dayInputsFolderPath,
                                                                                               EntityType.CREDIT_CARD_ACCOUNT));

                dayEndFileLineCounts.incrementDepositAccounts(PTDGUtils.generateAccountFile(accountDataList,
                                                                                            dayEndDate,
                                                                                            dayEndAccountFiles,
                                                                                            dayInputsFolderPath,
                                                                                            EntityType.DEPOSIT_ACCOUNT));

                dayEndFileLineCounts.incrementTimeDepositAccounts(PTDGUtils.generateAccountFile(accountDataList,
                                                                                                dayEndDate,
                                                                                                dayEndAccountFiles,
                                                                                                dayInputsFolderPath,
                                                                                                EntityType.TIME_DEPOSIT_ACCOUNT));

                dayEndFileLineCounts.incrementLoanAccounts(PTDGUtils.generateAccountFile(accountDataList,
                                                                                         dayEndDate,
                                                                                         dayEndAccountFiles,
                                                                                         dayInputsFolderPath,
                                                                                         EntityType.LOAN_ACCOUNT));

                dayEndFileLineCounts.incrementMutualFundAccounts(PTDGUtils.generateAccountFile(accountDataList,
                                                                                               dayEndDate,
                                                                                               dayEndAccountFiles,
                                                                                               dayInputsFolderPath,
                                                                                               EntityType.MUTUAL_FUND_ACCOUNT));
                dayEndFileLineCounts.incrementMortgageAccounts(PTDGUtils.generateAccountFile(accountDataList,
                                                                                             dayEndDate,
                                                                                             dayEndAccountFiles,
                                                                                             dayInputsFolderPath,
                                                                                             EntityType.MORTGAGE_ACCOUNT));
                dayEndFileLineCounts.incrementSecuritiesAccountGenerated(PTDGUtils.generateAccountFile(accountDataList,
                                                                                                       dayEndDate,
                                                                                                       dayEndAccountFiles,
                                                                                                       dayInputsFolderPath,
                                                                                                       EntityType.SECURITIES_ACCOUNT));

                chunkedCustomerDataList.add(chunkedCustomerData);
                chunkedaccountDataList.add(accountDataList);
                chunkedaccountRelation.add(accountRelationshipDataList);

                PTDGUtils.generateAccountFeatureFile(accountDataList, dayEndDate, dayAccountFeatureFile, dayEndFileLineCounts);
                PTDGUtils.generateAccounBalanceFile(accountDataList, dayEndDate, dayEndAccountBalanceFile, dayEndFileLineCounts);
                PTDGUtils.generateTransactionFiles(accountDataList, dayEndDate, dayEndTransactionFiles, dayInputsFolderPath, dayEndFileLineCounts, null);
                PTDGUtils.generateRelationshipFile(accountRelationshipDataList, dayEndDate, dayEndRelationshipFile, dayEndFileLineCounts);

                if (dayZero) {
                    PTDGUtils.generateDayZeroAccountDataFile(accountDataList,
                                                             dayEndDate,
                                                             dayInputsFolderPath,
                                                             EntityType.DEPOSIT_ACCOUNT,
                                                             dayZeroAccountsFile);
                    PTDGUtils.generateDayZeroAccountBalancesFile(accountDataList,
                                                                 dayEndDate,
                                                                 dayZeroaccountBalancesFile);
                }

                printCurrentTotals(dayEndFileLineCounts);
                logger.info("Generated chunk in " + (System.currentTimeMillis() - chunkStartTime) / 1000 + "s");
                logger.info("******************************************");
            }
        }

        List<CustomerData> fullCustomerData = chunkedCustomerDataList.stream().flatMap(x -> x.stream()).collect(Collectors.toList());
        List<AccountData> fullaccountDataList = chunkedaccountDataList.stream().flatMap(x -> x.stream()).collect(Collectors.toList());
        List<AccountRelationshipData> fullaccountRelation = chunkedaccountRelation.stream().flatMap(x -> x.stream()).collect(Collectors.toList());

        logger.debug("***Generating manual exceptions***");
        PTDGUtils.generateManualExceptionFile(fullaccountDataList, dayEndDate, manualExcetionFile, dayEndFileLineCounts, Math.round(factor * 26.5));

        logger.debug("***Generating Account Consent files***");
        PTDGUtils.generateAccountConsent(fullaccountRelation, dayEndDate, dayEndAccountConsent, dayEndFileLineCounts, Math.round(factor * 44));

        logger.debug("***Generating account Identifier***");
        PTDGUtils.generateAccountIdentifierChangeFile(fullaccountDataList, dayEndDate, dayEndAccountIdentifierChangeFile, dayEndFileLineCounts, Math.round(factor * 26.5));

        logger.debug("***Generating Customer Service files***");
        PTDGUtils.generateCustomerServiceFile(fullaccountRelation, dayEndDate, dayEndCustomerServiceFile, dayEndFileLineCounts, Math.round(factor * 2.5));

        logger.debug("***Generating AccountService Subscribe files***");
        PTDGUtils.generateAccountServiceSubscribe(fullaccountDataList, dayEndDate, dayEndAccountServiceSubscribe, dayEndFileLineCounts, Math.round(factor * 54));

        logger.debug("Genering Both Package Subscribe and Package Account Info files");
        PTDGUtils.generatePackageSubscription(fullaccountDataList,
                                              dayEndDate,
                                              dayEndPackageSubscribeFile,
                                              dayEndPackageAccountInfoFile,
                                              dayEndFileLineCounts,
                                              Math.round(factor * 40));

        logger.debug("***Genering Offer Enrollment files***");
        PTDGUtils.generateOfferEnrollment(fullCustomerData, dayEndDate, dayEndOfferEnrollmentFile, dayEndFileLineCounts, Math.round(factor * 100));

        logger.debug("***Genering Option Selector***");
        PTDGUtils.generateOptionSelector(fullaccountDataList, dayEndDate, dayEndOptionSelector, dayEndFileLineCounts, Math.round(factor * 44));

        logger.debug("***Genering Fee Exclusion***");
        PTDGUtils.generateFeeExclusion(fullaccountDataList, dayEndDate, dayEndFeeExclusionFIle, dayEndFileLineCounts, Math.round(factor * 13.25));

        logger.debug("Genering Both Billing Account and Billing Group Account");
        PTDGUtils.generateBillingAccountGroup(fullaccountDataList,
                                              dayEndDate,
                                              dayEndBillingAccountGroupfile,
                                              dayEndBillingAccountGroupAccountfile,
                                              dayEndFileLineCounts,
                                              Math.round(factor * 80));

        if (dayEndSpecification.getNumberOfAccountsHavingCycleEnd() > 0) {
            File depositAccountFile = dayEndAccountFiles.get(EntityType.DEPOSIT_ACCOUNT);
            if (depositAccountFile == null) {
                depositAccountFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.DEPOSIT_ACCOUNT, dayEndDate, false);
                dayEndAccountFiles.put(EntityType.DEPOSIT_ACCOUNT, depositAccountFile);
            }

            // Use the day zero account data file and append 'numberOfAccountsHavingCycleEnd' accounts to deposit
            // accounts file with the day end date as the cycle end
            PTDGUtils.addCycleEndDepositAccounts(depositAccountFile,
                                                 dayZeroAccountsFile,
                                                 dayEndDate,
                                                 dayEndSpecification.getNumberOfAccountsHavingCycleEnd(),
                                                 dataFolderPath);

            dayEndFileLineCounts.setNumberOfAccountsHavingCycleEnd(dayEndSpecification.getNumberOfAccountsHavingCycleEnd());
        }

        if (dayEndSpecification.getNumberOfAccountsHavingBalanceUpdates() > 0) {
            // File accountBalanceFile = dayEndAccountFiles.get(EntityType.ACCOUNT_BALANCE);
            File accountBalanceFile = dayEndAccountBalanceFile;
            if (accountBalanceFile == null) {
                accountBalanceFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.ACCOUNT_BALANCE, dayEndDate, false);
            }

            // Use the day zero account data file and append 'getNumberOfAccountsHavingBalanceUpdates' accounts to
            // balances file
            PTDGUtils.createDailyAccountBalances(accountBalanceFile,
                                                 dayZeroaccountBalancesFile,
                                                 dayEndDate,
                                                 dayEndSpecification.getNumberOfAccountsHavingBalanceUpdates(),
                                                 dataFolderPath);
            dayEndFileLineCounts.incrementNumberOfAccountsHavingBalanceUpdates(dayEndSpecification.getNumberOfAccountsHavingBalanceUpdates());
            PTDGUtils.insertFileHeader(accountBalanceFile,
                                       dayEndDate,
                                       dayEndFileLineCounts.getNumberOfAccountsHavingBalanceUpdates() +
                                               dayEndFileLineCounts.getTotalAccountsGenerated());
        } else {
            PTDGUtils.insertFileHeader(dayEndAccountBalanceFile, dayEndDate, dayEndFileLineCounts.getTotalAccountBalancesGenerated());
        }

        // Initial day files
        if (dayEndSpecification.isDayZero()) {
            File initialDayEndBranchFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.BRANCH, dayEndDate, false);
            PTDGUtils.createBranchFile(initialDayEndBranchFile, branchPTDGSpecifications);
            PTDGUtils.insertFileHeader(initialDayEndBranchFile, dayEndDate, DataGenerationContext.getTotalBranchesGenerated());
        }

        PTDGUtils.insertFileHeader(dayEndCustomerFile, dayEndDate, dayEndFileLineCounts.getTotalCustomersGenerated());
        PTDGUtils.insertAccountFileHeaders(dayEndAccountFiles, dayEndDate, dayEndFileLineCounts);
        PTDGUtils.insertFileHeader(dayAccountFeatureFile, dayEndDate, dayEndFileLineCounts.getTotalAccountFeaturesGenerated());
        PTDGUtils.insertFileHeader(manualExcetionFile, dayEndDate, dayEndFileLineCounts.getTotalManualExceptionsGenerated());
        PTDGUtils.insertFileHeader(dayEndCustomerServiceFile, dayEndDate, dayEndFileLineCounts.getTotalcustomerServiceContractId());
        PTDGUtils.insertFileHeader(dayEndRelationshipFile, dayEndDate, dayEndFileLineCounts.getTotalCustomerAccountRelationshipsGenerated());
        PTDGUtils.insertTransactionFileHeaders(dayEndTransactionFiles, dayEndDate, dayEndFileLineCounts);
        PTDGUtils.insertFileHeader(dayEndCustomerMergeFile, dayEndDate, dayEndFileLineCounts.getTotalCustomerMergeGenerated());
        PTDGUtils.insertFileHeader(dayEndAccountServiceSubscribe, dayEndDate, dayEndFileLineCounts.getTotalAccountServiceSubscribeGenerated());
        PTDGUtils.insertFileHeader(dayEndAccountConsent, dayEndDate, dayEndFileLineCounts.getTotalAccountConsentGenerated());
        PTDGUtils.insertFileHeader(dayEndPackageSubscribeFile, dayEndDate, dayEndFileLineCounts.getTotalpackageSubscribeGenerated());
        PTDGUtils.insertFileHeader(dayEndPackageAccountInfoFile, dayEndDate, dayEndFileLineCounts.getTotalpackageAccountInfoGenerated());
        PTDGUtils.insertFileHeader(dayEndAccountIdentifierChangeFile, dayEndDate, dayEndFileLineCounts.getTotalAccountIdentifierChangeGenerated());
        PTDGUtils.insertFileHeader(dayEndOfferEnrollmentFile, dayEndDate, dayEndFileLineCounts.getTotalOfferEnrollmentGenerated());
        PTDGUtils.insertFileHeader(dayEndFeeExclusionFIle, dayEndDate, dayEndFileLineCounts.getTotalFeeExclusionGenerated());
        PTDGUtils.insertFileHeader(dayEndOptionSelector, dayEndDate, dayEndFileLineCounts.getTotalOptionSelecterGenerated());
        // PTDGUtils.insertFileHeader(dayEndExternalFee, dayEndDate,
        // dayEndFileLineCounts.getTotalExternalFeeGenerated());
        PTDGUtils.insertFileHeader(dayEndBillingAccountGroupfile, dayEndDate, dayEndFileLineCounts.getTotalbillingAccountGroupGenerated());
        PTDGUtils.insertFileHeader(dayEndBillingAccountGroupAccountfile, dayEndDate, dayEndFileLineCounts.getTotalbillingAccountGroupAccountGenerated());

        // Print the leftovers
        printCurrentTotals(dayEndFileLineCounts);

        logger.info("Total time to generate " + DataGenerationContext.getTotalCustomersGenerated() + " customers and all related data is " + (System.currentTimeMillis()
                - startTime) / 1000 + "s");
    }

    private void printCurrentTotals(DayEndFileLineCounts dayEndFileLineCounts) {
        logger.info("Total customers generated " + dayEndFileLineCounts.getTotalCustomersGenerated());
        logger.info("Total deposit accounts generated " + dayEndFileLineCounts.getTotalDepositAccountsGenerated());
        logger.info("Total time deposit accounts generated " + dayEndFileLineCounts.getTotalTimeDepositAccountsGenerated());
        logger.info("Total credit card accounts generated " + dayEndFileLineCounts.getTotalCreditCardAccountsGenerated());
        logger.info("Total mutual fund accounts generated " + dayEndFileLineCounts.getTotalMutualFundAccountsGenerated());
        logger.info("Total loan accounts generated " + dayEndFileLineCounts.getTotalLoanAccountsGenerated());
        logger.info("Total account features generated " + dayEndFileLineCounts.getTotalAccountFeaturesGenerated());
        logger.info("Total mortgage accounts generated " + dayEndFileLineCounts.getTotalMortgageAccountsGenerated());
        logger.info("Total relationships generated " + dayEndFileLineCounts.getTotalRelationshipsGenerated());
        logger.info("Total credit card transactions generated " + dayEndFileLineCounts.getTotalCreditCardTransactionsGenerated());
        logger.info("Total deposit transactions generated " + dayEndFileLineCounts.getTotalDepositTransactionsGenerated());
        logger.info("Total loan transactions generated " + dayEndFileLineCounts.getTotalLoanTransactionsGenerated());
        logger.info("Total branches generated " + dayEndFileLineCounts.getTotalBranchesGenerated());
    }

    private void printCurrentTotals() {
        logger.info("Total deposit accounts generated " + DataGenerationContext.getTotalDepositAccountsGenerated());
        logger.info("Total time deposit accounts generated " + DataGenerationContext.getTotalTimeDepositAccountsGenerated());
        logger.info("Total credit card accounts generated " + DataGenerationContext.getTotalCreditCardAccountsGenerated());
        logger.info("Total mutual fund accounts generated " + DataGenerationContext.getTotalMutualFundAccountsGenerated());
        logger.info("Total loan accounts generated " + DataGenerationContext.getTotalLoanAccountsGenerated());
        logger.info("Total account features generated " + DataGenerationContext.getTotalAccountFeaturesGenerated());
        logger.info("Total mortgage accounts generated " + DataGenerationContext.getTotalMortgageAccountsGenerated());
        logger.info("Total relationships generated " + DataGenerationContext.getTotalRelationshipsGenerated());
        logger.info("Total credit card transactions generated " + DataGenerationContext.getTotalCreditCardTransactionsGenerated());
        logger.info("Total deposit transactions generated " + DataGenerationContext.getTotalDepositTransactionsGenerated());
        logger.info("Total loan transactions generated " + DataGenerationContext.getTotalLoanTransactionsGenerated());
        logger.info("Total branches generated " + DataGenerationContext.getTotalBranchesGenerated());
    }

}
