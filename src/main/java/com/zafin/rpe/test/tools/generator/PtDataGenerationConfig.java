package com.zafin.rpe.test.tools.generator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.graphbuilder.math.func.RoundFunction;

public class PtDataGenerationConfig {
    private static final Logger logger = LoggerFactory.getLogger(CitibanamexDataGenerationConfig.class);

    //Multiplication factor to generate 12500000 customers
    private static long multiplicationFactor = 100000;
    private static DecimalFormat df = new DecimalFormat("#.##");
    private static String dayEnd = "20200101";

    public static PTDGSpecification getDataGenerationSpecification() {
        PTDGSpecification specification = new PTDGSpecification();
        specification.setCustomPTDGSpecification(getCustomPTDGSpecification());
        return specification;
    }

    private static CustomPTDGSpecification getCustomPTDGSpecification() {
        CustomPTDGSpecification customPTDGSpecification = new CustomPTDGSpecification();

        DayEndSpecification dayEndSpecification = new DayEndSpecification("20200101", true, 0, 0);

        dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph1(5 * multiplicationFactor));// MODIFY

        dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph2(5 * multiplicationFactor));// MODIFY

        dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph3(5 * multiplicationFactor));// MODIFY

        dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph4(5 * multiplicationFactor));// MODIFY

        dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph5(5 * multiplicationFactor));// MODIFY

        dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph6(5 * multiplicationFactor));// MODIFY

        dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph7(5 * multiplicationFactor));// MODIFY

        dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph8(5 * multiplicationFactor));// MODIFY

        dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph9(5 * multiplicationFactor));// MODIFY

        dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph10(5 * multiplicationFactor));// MODIFY

        dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph11(5 * multiplicationFactor));// MODIFY

        dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph12(5 * multiplicationFactor));// MODIFY
       
        //Customer Merge Profile
        dayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph13(5 * multiplicationFactor));// MODIFY

        customPTDGSpecification.getDayEndSpecifications().add(dayEndSpecification);

        // Intermediate days
        for (int i = 2; i < 7; i++) {
            if (i == 2)
                dayEnd = "20200115";
            if (i == 3)
                dayEnd = "20200131";
            if (i == 4)
                dayEnd = "20200201";
            if (i == 5)
                dayEnd = "20200215";
            if (i == 6)
                dayEnd = "20200229";

            multiplicationFactor = multiplicationFactor / 2;
            DayEndSpecification intermediateDayEndSpecification = new DayEndSpecification("" + dayEnd, false, 0, 0);

            intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph1(5 * multiplicationFactor));// MODIFY

            intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph2(5 * multiplicationFactor));// MODIFY

            intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph3(5 * multiplicationFactor));// MODIFY

            intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph4(5 * multiplicationFactor));// MODIFY

            intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph5(5 * multiplicationFactor));// MODIFY

            intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph6(5 * multiplicationFactor));// MODIFY

            intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph7(5 * multiplicationFactor));// MODIFY

            intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph8(5 * multiplicationFactor));// MODIFY

            intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph9(5 * multiplicationFactor));// MODIFY

            intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph10(5 * multiplicationFactor));// MODIFY

            intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph11(5 * multiplicationFactor));// MODIFY

            intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph12(5 * multiplicationFactor));// MODIFY

            //Customer Merge Profile
            intermediateDayEndSpecification.getCustomerGraphPTDGSpecifications().add(createCustomerGraph13(5 * multiplicationFactor));// MODIFY

            customPTDGSpecification.getDayEndSpecifications().add(intermediateDayEndSpecification);
        }

        return customPTDGSpecification;
    }

    private static AccountsPTDGSpecification createAccountsPTDGSpecification(int numberOfAccounts,
                                                                             String accountProfileName,
                                                                             EntityType entityType,
                                                                             String productCode,
                                                                             double closingDayBalance,
                                                                             String accountOpenDate,
                                                                             String regionCode,
                                                                             AccountStatus accountStatus,
                                                                             String accountCohort,
                                                                             String billingPlan) {
        AccountsPTDGSpecification accountsPTDGSpecification = new AccountsPTDGSpecification();
        accountsPTDGSpecification.setNumberOfAccounts(numberOfAccounts);
        accountsPTDGSpecification.setAccountProfileName(accountProfileName);
        accountsPTDGSpecification.setAccountCohort(accountCohort);
        accountsPTDGSpecification.setAccountOpenDate(accountOpenDate);
        accountsPTDGSpecification.setAverageBalance(0.0);
        accountsPTDGSpecification.setClosingDayBalance(closingDayBalance);
        accountsPTDGSpecification.setProductCode(productCode);
        accountsPTDGSpecification.setEntityType(entityType);
        accountsPTDGSpecification.setPayrollAccount(getRandomValue(Arrays.asList("Y", "N")));
        accountsPTDGSpecification.setCycleEndDate("");
        accountsPTDGSpecification.setCustomCategory("");
        accountsPTDGSpecification.setRegionCode(regionCode);
        accountsPTDGSpecification.setAccountStatus(accountStatus.toString());
        accountsPTDGSpecification.setInGoodStanding(getRandomValue(Arrays.asList("Y", "N")));
        accountsPTDGSpecification.setBillingPlan(billingPlan);
        return accountsPTDGSpecification;
    }

    private static AccountsPTDGSpecification createAccountsPTDGSpecification(int numberOfAccounts,
                                                                             String accountProfileName,
                                                                             EntityType entityType,
                                                                             String productCode,
                                                                             double closingDayBalance) {

        AccountStatus accountStatus = AccountStatus.ACTIVE;

        String regionCode = getRandomValue(populateRegions());
        String accountCohort = getRandomValue(getAccountCohorts());
        String billingPlan = getRandomValue(Arrays.asList("BP_1", "BP_2"));
        List<String> channels = populateTransactionChannels();
        List<String> regions = populateRegions();
        List<String> transactionCodes = populateTransactionCodes();
        List<String> merchantStateCodes = populateStateCodes();
        List<String> merchantCodes = Arrays.asList("BESTBUY", "FOODBASICS", "FRESHCO", "IKEA", "METRO", "QUICKIE", "STAPLES", "STRUCTUBE", "WALMART");

        AccountsPTDGSpecification accountsPTDGSpecification = createAccountsPTDGSpecification(numberOfAccounts,
                                                                                              accountProfileName,
                                                                                              entityType,
                                                                                              productCode,
                                                                                              closingDayBalance,
                                                                                              "20180101",
                                                                                              regionCode,
                                                                                              accountStatus,
                                                                                              accountCohort,
                                                                                              billingPlan);
        int numberOfTransactionsPerAccount = 0;
        EntityType txType = null;
        String feeItemCode = null;

        if (entityType == EntityType.DEPOSIT_ACCOUNT) {
            numberOfTransactionsPerAccount = 8;
            txType = EntityType.DEPOSIT_TRANSACTION;
            feeItemCode = productCode == "BAS_CHK" ? getRandomValue(Arrays.asList("FIC_BBF", "FIC_DEP", "FIC_MMF", "FIC_RENT", "FIC_WDL", "FIC_WIRE"))
                    : getRandomValue(Arrays.asList("FIC_BBF", "FIC_DEP", "FIC_MMF", "FIC_RENT", "FIC_WIRE", "FIC_WDL", "FIC_CHQ_ISSU"));
        } else if (entityType == EntityType.CREDIT_CARD_ACCOUNT) {
            numberOfTransactionsPerAccount = 5;
            txType = EntityType.CREDIT_CARD_TRANSACTION;
            feeItemCode = getRandomValue(Arrays.asList("FIC_DEP", "FIC_MMF", "FIC_WDL"));
        } else if (entityType == EntityType.LOAN_ACCOUNT) {
            numberOfTransactionsPerAccount = 4;
            txType = EntityType.LOAN_TRANSACTION;
            feeItemCode = getRandomValue(Arrays.asList("FIC_ORG", "FIC_MMF", "FIC_RPYMT"));
        } else if (entityType == EntityType.MUTUAL_FUND_ACCOUNT) {
            numberOfTransactionsPerAccount = 0;
        } else if (entityType == EntityType.MORTGAGE_ACCOUNT) {
            numberOfTransactionsPerAccount = 0;
        } else if (entityType == EntityType.TIME_DEPOSIT_ACCOUNT) {
            numberOfTransactionsPerAccount = 0;
            feeItemCode = "FIC_MMF";
        } else if (entityType == EntityType.SECURITIES_ACCOUNT) {
            numberOfTransactionsPerAccount = 0;

        }
        // Transactions
        for (int i = 0; i < numberOfTransactionsPerAccount; i++) {
            String transactionCode = getRandomValue(transactionCodes);
            String merchantRegionCode = getRandomValue(regions);
            String txnChannel = getRandomValue(channels);
            double txnValue = ThreadLocalRandom.current().nextDouble(1, 10000);
            accountsPTDGSpecification.getTransactionPTDGSpecifications()
                                     .add(
                                          createTransactionsPTDGSpecification(txType,
                                                                              1,
                                                                              productCode,
                                                                              transactionCode,
                                                                              txnChannel,
                                                                              "",
                                                                              getRandomValue(merchantCodes),
                                                                              Double.valueOf(df.format(txnValue)),
                                                                              merchantRegionCode,
                                                                              getRandomValue(merchantStateCodes)));
        }

        if (StringUtils.isNotEmpty(feeItemCode))
            accountsPTDGSpecification.getManualExceptionPTDGSpecifications()
                                     .add(createManualExceptionPTDGSpecification(dayEnd, "20200501", "F", feeItemCode, getRandomValue(getExceptionReasonCodes())));

        return accountsPTDGSpecification;
    }

    private static CustomerPTDGSpecification createCustomerPTDGSpecification(long numberOfCustomers, String customerProfileName) {
        String customerSegment = getRandomValue(Arrays.asList("PLATINUM", "GOLD", "SILVER", "BRONZE"));
        String customerCohort = getRandomValue(Arrays.asList("CC1", "CC2", "CC3", "CC4", "CC5"));
        String customerEmployeeStatus = getRandomValue(Arrays.asList("ACTIVE", "PARTNER", "RETIRED"));

        CustomerPTDGSpecification customerPTDGSpecification = new CustomerPTDGSpecification();
        customerPTDGSpecification.setCustomerProfileName(customerProfileName);
        customerPTDGSpecification.setCustomerSegment(customerSegment);
        customerPTDGSpecification.setCustomerOpenDate("20190101");
        customerPTDGSpecification.setBranchCode("");
        customerPTDGSpecification.setCustomerCohortCategoryCode("");
        customerPTDGSpecification.setCustomerCohortCode(customerCohort);
        customerPTDGSpecification.setCustomerEmployeeStatus(customerEmployeeStatus);
        customerPTDGSpecification.setCustomerStatus("ACTIVE");
        customerPTDGSpecification.setCustomerBirthDate("19820829");
        customerPTDGSpecification.setNumberOfCustomers(numberOfCustomers);
        customerPTDGSpecification.setCustomerStatus("ACTIVE");
        return customerPTDGSpecification;
    }

    private static TransactionsPTDGSpecification createTransactionsPTDGSpecification(EntityType entityType,
                                                                                     int numberOfTransactions,
                                                                                     String productCode,
                                                                                     String transactionCode,
                                                                                     String transactionChannel,
                                                                                     String merchantCategoryCode,
                                                                                     String merchantCode,
                                                                                     double transactionValue,
                                                                                     String merchantRegionCode,
                                                                                     String merchantStateCode) {
        TransactionsPTDGSpecification transactionsPTDGSpecification = new TransactionsPTDGSpecification();
        transactionsPTDGSpecification.setEntityType(entityType);
        transactionsPTDGSpecification.setNumberOfTransactions(numberOfTransactions);
        transactionsPTDGSpecification.setProductCode(productCode);
        transactionsPTDGSpecification.setTransactionCode(transactionCode);
        transactionsPTDGSpecification.setTransactionChannel(transactionChannel);
        transactionsPTDGSpecification.setMerchantCategoryCode(merchantCategoryCode);
        transactionsPTDGSpecification.setMerchantCode(merchantCode);
        transactionsPTDGSpecification.setMerchantRegionCode(merchantRegionCode);
        transactionsPTDGSpecification.setMerchantStateCode(merchantStateCode);
        transactionsPTDGSpecification.setTransactionValue(transactionValue);
        return transactionsPTDGSpecification;
    }

    private static FeePTDGSpecification createFeePTDGSpecification(String feeCode,
                                                                   String feeName,
                                                                   double feeAmount,
                                                                   double benefitAmount,
                                                                   String benefitCode,
                                                                   int numberOfFees) {
        FeePTDGSpecification feePTDGSpecification = new FeePTDGSpecification();
        feePTDGSpecification.setFeeCode(feeCode);
        feePTDGSpecification.setFeeName(feeName);
        feePTDGSpecification.setFeeAmount(feeAmount);
        feePTDGSpecification.setBenefitAmount(benefitAmount);
        feePTDGSpecification.setBenefitCode(benefitCode);
        feePTDGSpecification.setNumberOfFees(numberOfFees);
        return feePTDGSpecification;
    }

    private static ManualExceptionPTDGSpecification createManualExceptionPTDGSpecification(String startDate,
                                                                                           String endDate,
                                                                                           String feeOrReward,
                                                                                           String feeItemCode,
                                                                                           String reason) {
        ManualExceptionPTDGSpecification manualExceptionPTDGSpecification = new ManualExceptionPTDGSpecification();
        manualExceptionPTDGSpecification.setEndDate(endDate);
        manualExceptionPTDGSpecification.setFeeItemCode(feeItemCode);
        manualExceptionPTDGSpecification.setFeeOrReward(feeOrReward);
        manualExceptionPTDGSpecification.setReason(reason);
        manualExceptionPTDGSpecification.setStartDate(startDate);
        return manualExceptionPTDGSpecification;
    }

    private static CustomerGraphPTDGSpecification createCustomerGraph1(long numberOfCustomerGraphs) {

        CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
        jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);

        // Create customer profiles
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1"));

        // Create account profiles
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_1", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));

        // Add the customer account relationships

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_CHK_1", "SOLE");

        return jointCustomerPTDGSpecification;
    }

    private static CustomerGraphPTDGSpecification createCustomerGraph2(long numberOfCustomerGraphs) {

        CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
        jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);

        // Create customer profiles
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1"));

        // Create account profiles - Number of accounts are ignored in a customer graph setup
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "PRE_CHK_1", EntityType.DEPOSIT_ACCOUNT, "PRE_CHK", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_1", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));

        // Add the customer account relationships

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "PRE_CHK_1", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_CHK_1", "SOLE");

        return jointCustomerPTDGSpecification;
    }

    private static CustomerGraphPTDGSpecification createCustomerGraph3(long numberOfCustomerGraphs) {

        CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
        jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);

        // Create customer profiles
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1"));

        // Create account profiles - Number of accounts are ignored in a customer graph setup
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_1", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "PRE_CHK_1", EntityType.DEPOSIT_ACCOUNT, "PRE_CHK", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CC_1", EntityType.CREDIT_CARD_ACCOUNT, "BAS_CC", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "PRE_CC_1", EntityType.CREDIT_CARD_ACCOUNT, "PRE_CC", 1500));

        // Add the customer account relationships

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_CHK_1", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "PRE_CHK_1", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_CC_1", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "PRE_CC_1", "SOLE");
        return jointCustomerPTDGSpecification;
    }

    private static CustomerGraphPTDGSpecification createCustomerGraph4(long numberOfCustomerGraphs) {

        CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
        jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);

        // Create customer profiles
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1"));

        // Create account profiles - Number of accounts are ignored in a customer graph setup
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_1", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "PRE_CC_1", EntityType.CREDIT_CARD_ACCOUNT, "PRE_CC", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_LOAN", EntityType.LOAN_ACCOUNT, "BAS_LOAN", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "GIC", EntityType.TIME_DEPOSIT_ACCOUNT, "GIC", 1500));

        // Add the customer account relationships

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_CHK_1", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "PRE_CC_1", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_LOAN", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "GIC", "SOLE");
        return jointCustomerPTDGSpecification;
    }

    private static CustomerGraphPTDGSpecification createCustomerGraph5(long numberOfCustomerGraphs) {

        CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
        jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);

        // Create customer profiles
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1"));

        // Create account profiles - Number of accounts are ignored in a customer graph setup
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_1", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "PRE_CC_1", EntityType.CREDIT_CARD_ACCOUNT, "PRE_CC", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_LOAN", EntityType.LOAN_ACCOUNT, "BAS_LOAN", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "GIC", EntityType.TIME_DEPOSIT_ACCOUNT, "GIC", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "HM", EntityType.MORTGAGE_ACCOUNT, "HM", 1500));

        // Add the customer account relationships
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_CHK_1", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "PRE_CC_1", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_LOAN", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "GIC", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "HM", "SOLE");
        return jointCustomerPTDGSpecification;
    }

    private static CustomerGraphPTDGSpecification createCustomerGraph6(long numberOfCustomerGraphs) {

        CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
        jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);

        // Create customer profiles
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1"));

        // Create account profiles - Number of accounts are ignored in a customer graph setup
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_1", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "PRE_CC_1", EntityType.CREDIT_CARD_ACCOUNT, "PRE_CC", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_LOAN", EntityType.LOAN_ACCOUNT, "BAS_LOAN", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "GIC", EntityType.TIME_DEPOSIT_ACCOUNT, "GIC", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "HM", EntityType.MORTGAGE_ACCOUNT, "HM", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "OTC_SPOT", EntityType.SECURITIES_ACCOUNT, "OTC_SPOT", 1500));

        // Add the customer account relationships

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_CHK_1", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "PRE_CC_1", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_LOAN", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "GIC", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "HM", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "OTC_SPOT", "SOLE");
        return jointCustomerPTDGSpecification;
    }

    private static CustomerGraphPTDGSpecification createCustomerGraph7(long numberOfCustomerGraphs) {

        CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
        jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);

        // Create customer profiles
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1"));

        // Create account profiles - Number of accounts are ignored in a customer graph setup
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_1", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "PRE_CC_1", EntityType.CREDIT_CARD_ACCOUNT, "PRE_CC", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_LOAN", EntityType.LOAN_ACCOUNT, "BAS_LOAN", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "GIC", EntityType.TIME_DEPOSIT_ACCOUNT, "GIC", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "HM", EntityType.MORTGAGE_ACCOUNT, "HM", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "OTC_SPOT", EntityType.SECURITIES_ACCOUNT, "OTC_SPOT", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "RP", EntityType.MUTUAL_FUND_ACCOUNT, "RP", 1500));

        // Add the customer account relationships

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_CHK_1", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "PRE_CC_1", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_LOAN", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "GIC", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "HM", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "OTC_SPOT", "SOLE");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "RP", "SOLE");
        return jointCustomerPTDGSpecification;
    }

    private static CustomerGraphPTDGSpecification createCustomerGraph8(long numberOfCustomerGraphs) {

        CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
        jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);

        // Create customer profiles
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1"));
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C2"));

        // Create account profiles - Number of accounts are ignored in a customer graph setup
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_1", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));

        // Add the customer account relationships

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_CHK_1", "PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "BAS_CHK_1", "NON_PRIMARY");

        return jointCustomerPTDGSpecification;
    }

    private static CustomerGraphPTDGSpecification createCustomerGraph9(long numberOfCustomerGraphs) {

        CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
        jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);

        // Create customer profiles
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1"));
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C2"));

        // Create account profiles - Number of accounts are ignored in a customer graph setup
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "PRE_CHK_1", EntityType.DEPOSIT_ACCOUNT, "PRE_CHK", 1500));

        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_1", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));

        // Add the customer account relationships
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "PRE_CHK_1", "SOLE");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_CHK_1", "PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "BAS_CHK_1", "NON_PRIMARY");

        return jointCustomerPTDGSpecification;
    }

    private static CustomerGraphPTDGSpecification createCustomerGraph10(long numberOfCustomerGraphs) {

        CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
        jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);

        // Create customer profiles
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1"));
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C2"));
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C3"));

        // Create account profiles - Number of accounts are ignored in a customer graph setup

        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_1", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_2", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_3", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));

        // Add the customer account relationships

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_CHK_1", "PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "BAS_CHK_1", "NON_PRIMARY");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "BAS_CHK_2", "PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "BAS_CHK_2", "NON_PRIMARY");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "BAS_CHK_3", "PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_CHK_3", "NON_PRIMARY");

        return jointCustomerPTDGSpecification;
    }

    private static CustomerGraphPTDGSpecification createCustomerGraph11(long numberOfCustomerGraphs) {

        CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
        jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);

        // Create customer profiles
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1"));
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C2"));
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C3"));

        // Create account profiles - Number of accounts are ignored in a customer graph setup

        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_1", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "PRE_LOAN_1", EntityType.LOAN_ACCOUNT, "PRE_LOAN", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CC_1", EntityType.CREDIT_CARD_ACCOUNT, "BAS_CC", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "GIC_1", EntityType.TIME_DEPOSIT_ACCOUNT, "GIC", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "PRE_CHK_1", EntityType.DEPOSIT_ACCOUNT, "PRE_CHK", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_LOAN_1", EntityType.LOAN_ACCOUNT, "BAS_LOAN", 1500));

        // Add the customer account relationships

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_CHK_1", "PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "BAS_CHK_1", "NON_PRIMARY");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "PRE_LOAN_1", "PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "PRE_LOAN_1", "NON_PRIMARY");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "BAS_CC_1", "PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "BAS_CC_1", "NON_PRIMARY");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "GIC_1", "PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "GIC_1", "NON_PRIMARY");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "PRE_CHK_1", "PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "PRE_CHK_1", "NON_PRIMARY");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "BAS_LOAN_1", "PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_LOAN_1", "NON_PRIMARY");

        return jointCustomerPTDGSpecification;
    }

    private static CustomerGraphPTDGSpecification createCustomerGraph12(long numberOfCustomerGraphs) {

        CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
        jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);

        // Create customer profiles
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1"));
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C2"));
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C3"));

        // Create account profiles - Number of accounts are ignored in a customer graph setup

        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_1", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "PRE_LOAN_1", EntityType.LOAN_ACCOUNT, "PRE_LOAN", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CC_1", EntityType.CREDIT_CARD_ACCOUNT, "BAS_CC", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "GIC_1", EntityType.TIME_DEPOSIT_ACCOUNT, "GIC", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "PRE_CHK_1", EntityType.DEPOSIT_ACCOUNT, "PRE_CHK", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_LOAN_1", EntityType.LOAN_ACCOUNT, "BAS_LOAN", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_2", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_3", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_4", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "PRE_CC_1", EntityType.CREDIT_CARD_ACCOUNT, "PRE_CC", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "PRE_CC_2", EntityType.CREDIT_CARD_ACCOUNT, "PRE_CC", 1500));
        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "PRE_CC_3", EntityType.CREDIT_CARD_ACCOUNT, "PRE_CC", 1500));
        // Add the customer account relationships

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_CHK_1", "PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "BAS_CHK_1", "NON_PRIMARY");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "PRE_LOAN_1", "PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "PRE_LOAN_1", "NON_PRIMARY");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "BAS_CC_1", "PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "BAS_CC_1", "NON_PRIMARY");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "GIC_1", "PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "GIC_1", "NON_PRIMARY");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "PRE_CHK_1", "PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "PRE_CHK_1", "NON_PRIMARY");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "BAS_LOAN_1", "PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_LOAN_1", "NON_PRIMARY");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_CHK_2", "SOLE");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "BAS_CHK_3", "SOLE");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "BAS_CHK_4", "SOLE");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "PRE_CC_1", "SOLE");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "PRE_CC_2", "SOLE");

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "PRE_CC_3", "SOLE");

        return jointCustomerPTDGSpecification;
    }

    private static CustomerGraphPTDGSpecification createCustomerGraph13(long numberOfCustomerGraphs) {

        CustomerGraphPTDGSpecification jointCustomerPTDGSpecification = new CustomerGraphPTDGSpecification();
        jointCustomerPTDGSpecification.setNumberOfCustomerGraphs(numberOfCustomerGraphs);

        // 13-Customer Merge profile - This profile included only in CustomerMergeFile and Customer file generation
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C1"));
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C2"));
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C3"));
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C4"));
        jointCustomerPTDGSpecification.getCustomerProfiles().add(createCustomerPTDGSpecification(1, "C5"));

        jointCustomerPTDGSpecification.getAccountProfiles().add(createAccountsPTDGSpecification(1, "BAS_CHK_1", EntityType.DEPOSIT_ACCOUNT, "BAS_CHK", 1500));

        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C1", "BAS_CHK_1", "PRMIARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C2", "BAS_CHK_1", "NON_PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C3", "BAS_CHK_1", "NON_PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C4", "BAS_CHK_1", "NON_PRIMARY");
        jointCustomerPTDGSpecification.addCustomerAccountRelationship("C5", "BAS_CHK_1", "NON_PRIMARY");

        return jointCustomerPTDGSpecification;
    }

    private static List<String> populateStateCodes() {
        return Arrays.asList("AB", "BC", "ON", "QC");
    }

    private static List<String> populateRegions() {
        return Arrays.asList("REGION_A", "REGION_B", "REGION_C", "REGION_D", "REGION_E");
    }

    private static List<String> populateTransactionChannels() {
        return Arrays.asList("AGENT", "ATM", "BRANCH", "BROKER", "MAIL", "ONLINE", "PHONE", "POS");
    }

    private static class CustomerCount {
        private int customerCount;
        private int accountCount;

        public CustomerCount(int customerCount, int accountCount) {
            this.customerCount = customerCount;
            this.accountCount = accountCount;
        }

    }

    private static List<String> populateTransactionCodes() {
        return Arrays.asList("DEP", "ISSU", "ORGN", "RPYMT", "WDL", "WIRE");
    }

    private static List<CustomerCount> populateCustomerCount() {
        List<CustomerCount> result = new ArrayList<>();
        /*
         * result.add(new CustomerCount(10573815, 1)); result.add(new CustomerCount(4969101, 2)); result.add(new
         * CustomerCount(1705484, 3)); result.add(new CustomerCount(720216, 4)); result.add(new CustomerCount(300117,
         * 5)); result.add(new CustomerCount(125657, 6)); result.add(new CustomerCount(53731, 7)); result.add(new
         * CustomerCount(24427, 8)); result.add(new CustomerCount(11833, 9)); result.add(new CustomerCount(6098, 10));
         * result.add(new CustomerCount(3553, 11)); result.add(new CustomerCount(2212, 12)); result.add(new
         * CustomerCount(1620, 13)); result.add(new CustomerCount(1295, 14)); result.add(new CustomerCount(996, 15));
         * result.add(new CustomerCount(893, 16)); result.add(new CustomerCount(778, 17)); result.add(new
         * CustomerCount(679, 18)); result.add(new CustomerCount(644, 19)); result.add(new CustomerCount(557, 20));
         * result.add(new CustomerCount(550, 21)); result.add(new CustomerCount(541, 22)); result.add(new
         * CustomerCount(529, 23)); result.add(new CustomerCount(496, 24)); result.add(new CustomerCount(451, 25));
         * result.add(new CustomerCount(429, 26)); result.add(new CustomerCount(411, 27)); result.add(new
         * CustomerCount(370, 28)); result.add(new CustomerCount(349, 29)); result.add(new CustomerCount(291, 30));
         * result.add(new CustomerCount(319, 31)); result.add(new CustomerCount(299, 32)); result.add(new
         * CustomerCount(294, 33)); result.add(new CustomerCount(290, 34)); result.add(new CustomerCount(237, 35));
         * result.add(new CustomerCount(251, 36)); result.add(new CustomerCount(223, 37)); result.add(new
         * CustomerCount(249, 38)); result.add(new CustomerCount(203, 39)); result.add(new CustomerCount(204, 40));
         * result.add(new CustomerCount(200, 41)); result.add(new CustomerCount(177, 42)); result.add(new
         * CustomerCount(160, 43)); result.add(new CustomerCount(128, 44)); result.add(new CustomerCount(131, 45));
         * result.add(new CustomerCount(105, 46)); result.add(new CustomerCount(94, 47)); result.add(new
         * CustomerCount(106, 48)); result.add(new CustomerCount(71, 49)); result.add(new CustomerCount(79, 50));
         * result.add(new CustomerCount(68, 51)); result.add(new CustomerCount(72, 52)); result.add(new
         * CustomerCount(53, 53)); result.add(new CustomerCount(47, 54)); result.add(new CustomerCount(40, 55));
         * result.add(new CustomerCount(48, 56)); result.add(new CustomerCount(27, 57)); result.add(new
         * CustomerCount(35, 58)); result.add(new CustomerCount(25, 59)); result.add(new CustomerCount(25, 60));
         * result.add(new CustomerCount(14, 61)); result.add(new CustomerCount(18, 62)); result.add(new
         * CustomerCount(21, 63)); result.add(new CustomerCount(14, 64)); result.add(new CustomerCount(27, 65));
         * result.add(new CustomerCount(23, 66)); result.add(new CustomerCount(19, 67)); result.add(new
         * CustomerCount(10, 68)); result.add(new CustomerCount(12, 69)); result.add(new CustomerCount(10, 70));
         * result.add(new CustomerCount(10, 71)); result.add(new CustomerCount(9, 72)); result.add(new CustomerCount(7,
         * 73)); result.add(new CustomerCount(11, 74)); result.add(new CustomerCount(9, 75)); result.add(new
         * CustomerCount(11, 76)); result.add(new CustomerCount(4, 77)); result.add(new CustomerCount(9, 78));
         * result.add(new CustomerCount(8, 79)); result.add(new CustomerCount(6, 80)); result.add(new CustomerCount(11,
         * 81)); result.add(new CustomerCount(6, 82)); result.add(new CustomerCount(4, 83)); result.add(new
         * CustomerCount(5, 84)); result.add(new CustomerCount(5, 85)); result.add(new CustomerCount(3, 86));
         * result.add(new CustomerCount(2, 87)); result.add(new CustomerCount(5, 88)); result.add(new CustomerCount(2,
         * 89)); result.add(new CustomerCount(4, 90)); result.add(new CustomerCount(5, 91)); result.add(new
         * CustomerCount(2, 92)); result.add(new CustomerCount(2, 93)); result.add(new CustomerCount(3, 94));
         * result.add(new CustomerCount(4, 95)); result.add(new CustomerCount(2, 96)); result.add(new CustomerCount(2,
         * 97)); result.add(new CustomerCount(1, 98)); result.add(new CustomerCount(2, 101)); result.add(new
         * CustomerCount(5, 102)); result.add(new CustomerCount(1, 104)); result.add(new CustomerCount(1, 107));
         * result.add(new CustomerCount(1, 108)); result.add(new CustomerCount(3, 109)); result.add(new CustomerCount(1,
         * 112)); result.add(new CustomerCount(1, 114)); result.add(new CustomerCount(1, 116)); result.add(new
         * CustomerCount(2, 118)); result.add(new CustomerCount(3, 119)); result.add(new CustomerCount(1, 122));
         * result.add(new CustomerCount(1, 124)); result.add(new CustomerCount(2, 125)); result.add(new CustomerCount(1,
         * 127)); result.add(new CustomerCount(1, 136)); result.add(new CustomerCount(1, 137)); result.add(new
         * CustomerCount(2, 138)); result.add(new CustomerCount(2, 140)); result.add(new CustomerCount(1, 142));
         * result.add(new CustomerCount(2, 143)); result.add(new CustomerCount(2, 147)); result.add(new CustomerCount(2,
         * 148)); result.add(new CustomerCount(1, 149)); result.add(new CustomerCount(2, 150)); result.add(new
         * CustomerCount(1, 151)); result.add(new CustomerCount(1, 152)); result.add(new CustomerCount(1, 155));
         * result.add(new CustomerCount(2, 156)); result.add(new CustomerCount(1, 157)); result.add(new CustomerCount(1,
         * 159)); result.add(new CustomerCount(1, 161)); result.add(new CustomerCount(1, 169)); result.add(new
         * CustomerCount(1, 173)); result.add(new CustomerCount(1, 179)); result.add(new CustomerCount(1, 184));
         * result.add(new CustomerCount(1, 185)); result.add(new CustomerCount(2, 198)); result.add(new CustomerCount(1,
         * 213)); result.add(new CustomerCount(1, 234)); result.add(new CustomerCount(1, 242)); result.add(new
         * CustomerCount(1, 287));
         */
        return result;
    }

    private static List<String> getAccountCohorts() {
        return Arrays.asList("AC1", "AC2", "AC3", "AC4", "AC5");
    }

    public static List<String> getExceptionReasonCodes() {
        return Arrays.asList("MFW", "PFW");
    }

    public static String getOptionCodesForPaidServices(String serviceCode) {
        String optionCode = null;
        if (serviceCode == "RSA") {
            optionCode = getRandomValue(Arrays.asList("OPT1", "OPT2"));
        }
        return optionCode;
    }

    public static String getPaidServiceCodes() {
        return "RSA";
    }

    public static String getPackageCode(String productCode) {
        String packageCode = null;
        if (productCode == "BAS_CHK" || productCode == "BAS_CC" || productCode == "BAS_LOAN") {
            packageCode = "BAS_PKG";
        }

        else if (productCode == "PRE_CHK" || productCode == "PRE_CC" || productCode == "PRE_LOAN")
            packageCode = "PRE_PKG";
        return packageCode;
    }

    private static String getRandomValue(List<String> valueSet) {
        Random rand = new Random();
        return valueSet.get(rand.nextInt(valueSet.size()));
    }

    public static List<String> getAccountServiceCode(String productCode) {
        List<String> accountServiceCode = null;
        if (productCode == "BAS_CHK")
            accountServiceCode = Arrays.asList("BILL_PYMT", "CASH_MGMT", "LOCK_BOX");
        else if (productCode == "BAS_CHK")
            accountServiceCode = Arrays.asList("FP");
        return accountServiceCode;
    }

    public static List<String> getOptionCodes() {
        return Arrays.asList("MMF_OPT_1", "MRF_OPT_2");
    }

    private enum AccountStatus {
        ACTIVE, CLOSED;
    }

}
