package com.zafin.rpe.test.tools.generator;

import java.util.ArrayList;
import java.util.List;

import com.zafin.rpe.test.tools.generator.input.model.DateRange;

public class RestrictAccountDataGenerationSpecification {

	private List<String> products = new ArrayList<>();
	private DateRange accountOpenDate;
	
	
	public DateRange getAccountOpenDate() {
		return accountOpenDate;
	}
	public void setAccountOpenDate(DateRange accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}
	public List<String> getProducts() {
		return products;
	}
	public void setProducts(List<String> products) {
		this.products = products;
	}
	
	
}
