package com.zafin.rpe.test.tools.generator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * Generates data across one or more days based on the configuration
 *
 */
public class SmartDataGenerator {

	private static final int CHUNK_SIZE = 100000;
	private static final Logger logger = LoggerFactory.getLogger(SmartDataGenerator.class);
	
	public void generateData(PTDGSpecification specification, String folderPath) {
		String dataFolderPath = PTDGUtils.setupDataFolder(folderPath);
		List<DayEndSpecification> dayEndSpecifications = specification.getCustomPTDGSpecification().getDayEndSpecifications();
		for (DayEndSpecification dayEndSpecification : dayEndSpecifications) {
			generateAllFilesForDay(dayEndSpecification, dataFolderPath, specification.getCustomPTDGSpecification().getBranchPTDGSpecifications());
		}
		
		//Create run.order
		generateRunOrderFile(dataFolderPath, dayEndSpecifications);
		
		//Create zrpe_runtime_system_date_update.sql
		generateRUntimeSystemDataUpdateFile(dataFolderPath, "01-NOV-2018");
		
		
	}

	private void generateRUntimeSystemDataUpdateFile(String dataFolderPath, String systemDate) {
		String sql = "update mi_system set curr_date=TO_DATE('" + systemDate + "', 'DD-MON-YYYY');";
		File file = PTDGUtils.createFile(dataFolderPath, "zrpe_runtime_system_date_update.sql");
		List<String> lines = new ArrayList<>();
		lines.add(sql);
		try {
			FileUtils.writeLines(file, lines, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}



	private void generateRunOrderFile(String dataFolderPath, List<DayEndSpecification> dayEndSpecifications) {
		File file = PTDGUtils.createFile(dataFolderPath, "run.order");
		List<String> lines = new ArrayList<>();
		for (DayEndSpecification dayEndSpecification : dayEndSpecifications) {
			lines.add(dayEndSpecification.getDayEndDate());
		}
		try {
			FileUtils.writeLines(file, lines, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void generateAllFilesForDay(DayEndSpecification dayEndSpecification, String dataFolderPath, List<BranchPTDGSpecification> branchPTDGSpecifications) {
		
		String dayEndDate = dayEndSpecification.getDayEndDate();
		String dayEndFolderPath = PTDGUtils.setupDayEndFolder(dataFolderPath, dayEndDate);
		String dayInputsFolderPath = dayEndFolderPath + "/inputs";
		
		PTDGUtils.createAllFilesForDay(dayInputsFolderPath, dayEndDate);
		
		//Create customer
		File dayEndCustomerFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.CUSTOMER, dayEndDate, false);
		// Create relationship files
		File dayEndRelationshipFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.CUSTOMER_ACCOUNT_RELATIONSHIP, dayEndDate, false);
		// Create account files
		Map<EntityType, File> dayEndAccountFiles = new HashMap<>();
		// Create account balance file
		File dayEndAccountBalanceFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.ACCOUNT_BALANCE, dayEndDate, false);
		// Create account balance file
		File dayAccountFeatureFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.ACCOUNT_FEATURE, dayEndDate, false);
		// Create manual exception file
		File manualExcetionFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.MANUAL_EXCEPTION, dayEndDate, false);
		// Create transaction files
		Map<EntityType, File> dayEndTransactionFiles = new HashMap<>();
		
		boolean dayZero = dayEndSpecification.isDayZero();
		File dayZeroAccountsFile = new File(dataFolderPath + "/DayZeroDepositAccounts.DAT");
		File dayZeroaccountBalancesFile = new File(dataFolderPath + "/DayZeroAccountBalances.DAT");
		
		//boolean generateFeeFile = dayEndSpecification.isGenerateFeeFile();
		File feeFile = new File(dataFolderPath + "/Fee.DAT");
		if (!feeFile.exists()) {
			try {
				feeFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		DayEndFileLineCounts dayEndFileLineCounts = new DayEndFileLineCounts();
		
		long startTime = System.currentTimeMillis();
		List<CustomerPTDGSpecification> customerPTDGSpecifications = dayEndSpecification.getCustomerPTDGSpecifications();
		for (CustomerPTDGSpecification customerPTDGSpecification : customerPTDGSpecifications) {
			List<Long> chunks = PTDGUtils.chunkNumber(customerPTDGSpecification.getNumberOfCustomers(), CHUNK_SIZE);
			for (Long chunk : chunks) {
				logger.info("Generateing " + chunk + " customers and all its related records ... ");
				long chunkStartTime = System.currentTimeMillis();
				logger.info("******************************************");
				CustomerAccountData customerAccountData = PTDGUtils.generateChunkCustomerAndAccountData(chunk, customerPTDGSpecification);
				List<CustomerData> chunkedCustomerData = customerAccountData.getCustomerDataList();
				List<AccountData> accountDataList = customerAccountData.getAccountDataList();
				List<AccountRelationshipData> accountRelationshipDataList = customerAccountData.getAccountRelationshipDataList();
				
				//Generate files for initial day end
				int numberOfCustomersAdded = PTDGUtils.generateCustomerFile(chunkedCustomerData, dayEndDate, dayEndCustomerFile);
				dayEndFileLineCounts.incrementCustomers(numberOfCustomersAdded);
				
				dayEndFileLineCounts.incrementCreditCardAccounts(PTDGUtils.generateAccountFile(accountDataList, dayEndDate, 
						dayEndAccountFiles, dayInputsFolderPath, EntityType.CREDIT_CARD_ACCOUNT));
				
				dayEndFileLineCounts.incrementDepositAccounts(PTDGUtils.generateAccountFile(accountDataList, dayEndDate, 
						dayEndAccountFiles, dayInputsFolderPath, EntityType.DEPOSIT_ACCOUNT));
				
				dayEndFileLineCounts.incrementTimeDepositAccounts(PTDGUtils.generateAccountFile(accountDataList, dayEndDate, 
						dayEndAccountFiles, dayInputsFolderPath, EntityType.TIME_DEPOSIT_ACCOUNT));
				
				dayEndFileLineCounts.incrementLoanAccounts(PTDGUtils.generateAccountFile(accountDataList, dayEndDate, 
						dayEndAccountFiles, dayInputsFolderPath, EntityType.LOAN_ACCOUNT));
				
				dayEndFileLineCounts.incrementMutualFundAccounts(PTDGUtils.generateAccountFile(accountDataList, dayEndDate, 
						dayEndAccountFiles, dayInputsFolderPath, EntityType.MUTUAL_FUND_ACCOUNT));
				
				//Generate manual exception file
				PTDGUtils.generateManualExceptionFile(accountDataList, dayEndDate, manualExcetionFile, dayEndFileLineCounts,0);
				//Generate account features file
				PTDGUtils.generateAccountFeatureFile(accountDataList, dayEndDate, dayAccountFeatureFile, dayEndFileLineCounts);
				//Generate account balance file
				PTDGUtils.generateAccounBalanceFile(accountDataList, dayEndDate, dayEndAccountBalanceFile,dayEndFileLineCounts);
				PTDGUtils.generateTransactionFiles(accountDataList, dayEndDate, dayEndTransactionFiles, dayInputsFolderPath, dayEndFileLineCounts, feeFile);
				PTDGUtils.generateRelationshipFile(accountRelationshipDataList, dayEndDate, dayEndRelationshipFile, dayEndFileLineCounts);
			
				if(dayZero) {
					PTDGUtils.generateDayZeroAccountDataFile(accountDataList, dayEndDate, 
						dayInputsFolderPath, EntityType.DEPOSIT_ACCOUNT, dayZeroAccountsFile);
					PTDGUtils.generateDayZeroAccountBalancesFile(accountDataList, dayEndDate, 
							dayZeroaccountBalancesFile);
				} 
				
				printCurrentTotals(dayEndFileLineCounts);
				logger.info("Generated chunk in " + (System.currentTimeMillis() - chunkStartTime)/1000 + "s");
				logger.info("******************************************");
			}
		}
		
		if(dayEndSpecification.getNumberOfAccountsHavingCycleEnd() > 0) {
			File depositAccountFile = dayEndAccountFiles.get(EntityType.DEPOSIT_ACCOUNT);
			if (depositAccountFile == null) {
				depositAccountFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.DEPOSIT_ACCOUNT, dayEndDate, false);
				dayEndAccountFiles.put(EntityType.DEPOSIT_ACCOUNT, depositAccountFile);
			}
			
			//Use the day zero account data file and append 'numberOfAccountsHavingCycleEnd' accounts to deposit accounts file with the day end date as the cycle end
			PTDGUtils.addCycleEndDepositAccounts(depositAccountFile, dayZeroAccountsFile, dayEndDate, 
					dayEndSpecification.getNumberOfAccountsHavingCycleEnd(), dataFolderPath);
			
			dayEndFileLineCounts.setNumberOfAccountsHavingCycleEnd(dayEndSpecification.getNumberOfAccountsHavingCycleEnd());
		}

		if(dayEndSpecification.getNumberOfAccountsHavingBalanceUpdates() > 0) {
			File accountBalanceFile = dayEndAccountFiles.get(EntityType.ACCOUNT_BALANCE);
			if (accountBalanceFile == null) {
				accountBalanceFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.ACCOUNT_BALANCE, dayEndDate, false);
			}
			
			//Use the day zero account data file and append 'getNumberOfAccountsHavingBalanceUpdates' accounts to balances file
			PTDGUtils.createDailyAccountBalances(accountBalanceFile, dayZeroaccountBalancesFile, dayEndDate,
					dayEndSpecification.getNumberOfAccountsHavingBalanceUpdates(), dataFolderPath);
			dayEndFileLineCounts.incrementNumberOfAccountsHavingBalanceUpdates(dayEndSpecification.getNumberOfAccountsHavingBalanceUpdates());
			PTDGUtils.insertFileHeader(accountBalanceFile, dayEndDate, dayEndFileLineCounts.getNumberOfAccountsHavingBalanceUpdates());
		} else {
			PTDGUtils.insertFileHeader(dayEndAccountBalanceFile, dayEndDate, dayEndFileLineCounts.getTotalAccountsGenerated());
		}
		
		//Initial day files
		if(dayEndSpecification.isDayZero()) {
			File initialDayEndBranchFile = PTDGUtils.createFile(dayInputsFolderPath, EntityType.BRANCH, dayEndDate, false);
			PTDGUtils.createBranchFile(initialDayEndBranchFile, branchPTDGSpecifications);
			PTDGUtils.insertFileHeader(initialDayEndBranchFile, dayEndDate, DataGenerationContext.getTotalBranchesGenerated());
		}
		
		PTDGUtils.insertFileHeader(dayEndCustomerFile, dayEndDate, dayEndFileLineCounts.getTotalCustomersGenerated());
		PTDGUtils.insertAccountFileHeaders(dayEndAccountFiles, dayEndDate, dayEndFileLineCounts);
		PTDGUtils.insertFileHeader(dayAccountFeatureFile, dayEndDate, dayEndFileLineCounts.getTotalAccountFeaturesGenerated());
		PTDGUtils.insertFileHeader(manualExcetionFile, dayEndDate, dayEndFileLineCounts.getTotalManualExceptionsGenerated());
		PTDGUtils.insertFileHeader(dayEndRelationshipFile, dayEndDate, dayEndFileLineCounts.getTotalCustomerAccountRelationshipsGenerated());
		PTDGUtils.insertTransactionFileHeaders(dayEndTransactionFiles, dayEndDate, dayEndFileLineCounts);
		
		//Print the leftovers
		printCurrentTotals(dayEndFileLineCounts);
		
		logger.info("Total time to generate " + DataGenerationContext.getTotalCustomersGenerated() + " customers and all related data is " + (System.currentTimeMillis() - startTime) / 1000 + "s");
	}
	
	private void printCurrentTotals(DayEndFileLineCounts dayEndFileLineCounts) {
		logger.info("Total customers generated " + dayEndFileLineCounts.getTotalCustomersGenerated());
		logger.info("Total deposit accounts generated " + dayEndFileLineCounts.getTotalDepositAccountsGenerated());
		logger.info("Total time deposit accounts generated " + dayEndFileLineCounts.getTotalTimeDepositAccountsGenerated());
		logger.info("Total credit card accounts generated " + dayEndFileLineCounts.getTotalCreditCardAccountsGenerated());
		logger.info("Total mutual fund accounts generated " + dayEndFileLineCounts.getTotalMutualFundAccountsGenerated());
		logger.info("Total loan accounts generated " + dayEndFileLineCounts.getTotalLoanAccountsGenerated());
		logger.info("Total account features generated " + dayEndFileLineCounts.getTotalAccountFeaturesGenerated());
		logger.info("Total mortgage accounts generated " + dayEndFileLineCounts.getTotalMortgageAccountsGenerated());
		logger.info("Total relationships generated " + dayEndFileLineCounts.getTotalRelationshipsGenerated());
		logger.info("Total credit card transactions generated " + dayEndFileLineCounts.getTotalCreditCardTransactionsGenerated());
		logger.info("Total deposit transactions generated " + dayEndFileLineCounts.getTotalDepositTransactionsGenerated());
		logger.info("Total loan transactions generated " + dayEndFileLineCounts.getTotalLoanTransactionsGenerated());
		logger.info("Total branches generated " + dayEndFileLineCounts.getTotalBranchesGenerated());
	}

	private void printCurrentTotals() {
		logger.info("Total deposit accounts generated " + DataGenerationContext.getTotalDepositAccountsGenerated());
		logger.info("Total time deposit accounts generated " + DataGenerationContext.getTotalTimeDepositAccountsGenerated());
		logger.info("Total credit card accounts generated " + DataGenerationContext.getTotalCreditCardAccountsGenerated());
		logger.info("Total mutual fund accounts generated " + DataGenerationContext.getTotalMutualFundAccountsGenerated());
		logger.info("Total loan accounts generated " + DataGenerationContext.getTotalLoanAccountsGenerated());
		logger.info("Total account features generated " + DataGenerationContext.getTotalAccountFeaturesGenerated());
		logger.info("Total mortgage accounts generated " + DataGenerationContext.getTotalMortgageAccountsGenerated());
		logger.info("Total relationships generated " + DataGenerationContext.getTotalRelationshipsGenerated());
		logger.info("Total credit card transactions generated " + DataGenerationContext.getTotalCreditCardTransactionsGenerated());
		logger.info("Total deposit transactions generated " + DataGenerationContext.getTotalDepositTransactionsGenerated());
		logger.info("Total loan transactions generated " + DataGenerationContext.getTotalLoanTransactionsGenerated());
		logger.info("Total branches generated " + DataGenerationContext.getTotalBranchesGenerated());
	}
	
	
}
