package com.zafin.rpe.test.tools.generator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 
 *
 */
public class SnowflakeDataGenerationConfig {

	private static long multiplicationFactor = 1;
	private static DecimalFormat df = new DecimalFormat("#.##");
	
	public static PTDGSpecification getDataGenerationSpecification() {
		PTDGSpecification specification = new PTDGSpecification();
		specification.setCustomPTDGSpecification(getCustomPTDGSpecification());
		return specification;
	}
	
	private static CustomPTDGSpecification getCustomPTDGSpecification() {
		CustomPTDGSpecification customPTDGSpecification = new CustomPTDGSpecification();
		
		//Day Zero: 01 Nov 2018
	    DayEndSpecification dayEndSpecification = new DayEndSpecification("20190915", true, 0, 0);
	    dayEndSpecification.getCustomerPTDGSpecifications().add(createRandomCustomerPTDGSpecification(10, 100, 100));//MODIFY
	    customPTDGSpecification.getDayEndSpecifications().add(dayEndSpecification);
		
		return customPTDGSpecification;
	}
	
	private static AccountsPTDGSpecification createAccountsPTDGSpecification(int numberOfAccounts, EntityType entityType, 
			String productCode, double closingDayBalance, String accountOpenDate, String regionCode) {
		AccountsPTDGSpecification accountsPTDGSpecification = new AccountsPTDGSpecification();
		accountsPTDGSpecification.setNumberOfAccounts(numberOfAccounts);
		accountsPTDGSpecification.setAccountCohort("");
		accountsPTDGSpecification.setAccountOpenDate(accountOpenDate);
		accountsPTDGSpecification.setAverageBalance(0.0);
		accountsPTDGSpecification.setClosingDayBalance(closingDayBalance);
		accountsPTDGSpecification.setProductCode(productCode);
		accountsPTDGSpecification.setEntityType(entityType);
		accountsPTDGSpecification.setPayrollAccount("N");
		accountsPTDGSpecification.setCycleEndDate("");
		accountsPTDGSpecification.setCustomCategory("");
		accountsPTDGSpecification.setRegionCode(regionCode);
		return accountsPTDGSpecification;
	}
	
	private static AccountsPTDGSpecification createAccountsPTDGSpecification(int numberOfAccounts, EntityType entityType, 
			String productCode, double closingDayBalance, String regionCode) {
		return createAccountsPTDGSpecification(numberOfAccounts, entityType, productCode, closingDayBalance, "20180101", regionCode);
	}
	
	private static CustomerPTDGSpecification createCustomerPTDGSpecification(long numberOfCustomers, List<String> customerSegments) {
		CustomerPTDGSpecification customerPTDGSpecification = new CustomerPTDGSpecification();
		customerPTDGSpecification.setCustomerSegmentList(customerSegments);
		customerPTDGSpecification.setCustomerOpenDate("20170101");
		customerPTDGSpecification.setBranchCode("");
		customerPTDGSpecification.setCustomerCohortCategoryCode("");
		customerPTDGSpecification.setCustomerCohortCode("");
		customerPTDGSpecification.setCustomerStatus("ACTIVE");
		customerPTDGSpecification.setCustomerBirthDate("19820829");
		customerPTDGSpecification.setCustomerEmployeeStatus("");
		customerPTDGSpecification.setNumberOfCustomers(numberOfCustomers);
		return customerPTDGSpecification;
	}
	
	private static TransactionsPTDGSpecification createTransactionsPTDGSpecification(EntityType entityType, int numberOfTransactions, String productCode, 
			String transactionCode, String transactionChannel, String merchantCategoryCode, String merchantCode, double transactionValue, String merchantRegionCode) {
		TransactionsPTDGSpecification transactionsPTDGSpecification = new TransactionsPTDGSpecification();
		transactionsPTDGSpecification.setEntityType(entityType);
		transactionsPTDGSpecification.setNumberOfTransactions(numberOfTransactions);
		transactionsPTDGSpecification.setProductCode(productCode);
		transactionsPTDGSpecification.setTransactionCode(transactionCode);
		transactionsPTDGSpecification.setTransactionChannel(transactionChannel);
		transactionsPTDGSpecification.setMerchantCategoryCode(merchantCategoryCode);
		transactionsPTDGSpecification.setMerchantCode(merchantCode);
		transactionsPTDGSpecification.setMerchantRegionCode(merchantRegionCode);
		transactionsPTDGSpecification.setTransactionValue(transactionValue);
		return transactionsPTDGSpecification;
	}
	
	private static FeePTDGSpecification createFeePTDGSpecification(String feeCode, String feeName, 
			double feeAmount, double benefitAmount, String benefitCode, int numberOfFees) {
		FeePTDGSpecification feePTDGSpecification = new FeePTDGSpecification();
		feePTDGSpecification.setFeeCode(feeCode);
		feePTDGSpecification.setFeeName(feeName);
		feePTDGSpecification.setFeeAmount(feeAmount);
		feePTDGSpecification.setBenefitAmount(benefitAmount);
		feePTDGSpecification.setBenefitCode(benefitCode);
		feePTDGSpecification.setNumberOfFees(numberOfFees);
		return feePTDGSpecification;
	}
	
	
	/**
	 * Customer Profile 6: 105K of Customers get no waiver
        * Accounts (24)
            * Deposit Account -  5 [ Product Code : DEP ]
            * IPS GIC - 14 [ Product Code : GIC ]
            * SPA - 1 [ Product Code : SPA ]
            * Mutual Funds : 4 [ Product Code : MF ]
        * No permanent waivers, not wood gundy customers
        * Each of the accounts have 1K balance each
	 */
	private static CustomerPTDGSpecification createRandomCustomerPTDGSpecification(long numberOfCustomers, int numberOfAccountsPerCustomer, int numberOfTransactionsPerAccount) {
		
		List<String> customerSegments = new ArrayList<>();
		customerSegments.add("PLATINUM");
		customerSegments.add("GOLD");
		customerSegments.add("SILVER");
		
		List<String> regions = new ArrayList<>();
		regions.add("ONTARIO");
		regions.add("QUEBEC");
		regions.add("ALBERTA");
		regions.add("YUKON");
		regions.add("NUNAVUT");
		
		List<String> products = new ArrayList<>();
		products.add("EVERDAY_CHEQUING");
		products.add("SMART_CHEQUING");
		products.add("PREMIER_CHEQUING");
		products.add("BOUNS_SAVING");
		products.add("EADVANTAGE_SAVING");
		products.add("PREMIUM_SAVING");
		
		List<String> channels = new ArrayList<>();
		channels.add("BRANCH");
		channels.add("WEB");
		channels.add("MOBILE");
		
		List<String> transactionCodes = new ArrayList<>();
		transactionCodes.add("POS");
		transactionCodes.add("WIRE");
		transactionCodes.add("CHEQUE");
		transactionCodes.add("INTERAC");
		transactionCodes.add("ATM_WITHDRAWAL");
		transactionCodes.add("BRANCH_WITHDRAWAL");
		
		List<FeeMetadata> feeMetatadaList = new ArrayList<FeeMetadata>();
		feeMetatadaList.add(new FeeMetadata("MONTHLY_MAINTENANCE", "MONTHLY_MAINTENANCE", 9.50, 1, 2));
		feeMetatadaList.add(new FeeMetadata("OVERDRAFT_PROTECTION", "OVERDRAFT_PROTECTION",  5.00, 1, 2));
		feeMetatadaList.add(new FeeMetadata("WIRE", "WIRE", 1.50, 1, 2));
		feeMetatadaList.add(new FeeMetadata("CHEQUE", "CHEQUE", 1.50, 1, 5));
		feeMetatadaList.add(new FeeMetadata("ATM_WITHDRAWAL", "ATM_WITHDRAWAL", 1.50, 2,6));
		feeMetatadaList.add(new FeeMetadata("NSF", "NSF", 30.00, 0, 1));
		
		CustomerPTDGSpecification customerPTDGSpecification = createCustomerPTDGSpecification(numberOfCustomers, customerSegments);
		
		//5 accounts
		for (int i = 0; i < numberOfAccountsPerCustomer; i++) {
			String regionCode = getRandomValue(regions); 
			String productCode = getRandomValue(products); 
			
			AccountsPTDGSpecification accountsPTDGSpecification = createAccountsPTDGSpecification(1, EntityType.DEPOSIT_ACCOUNT, productCode, 1000, regionCode);
			
			//Transactions
			for (int j = 0; j < numberOfTransactionsPerAccount; j++) {
				String transactionCode = getRandomValue(transactionCodes);
				String merchantRegionCode = getRandomValue(regions); 
				String txnChannel = getRandomValue(channels);
				double txnValue = ThreadLocalRandom.current().nextDouble(1, 10000);
				accountsPTDGSpecification.getTransactionPTDGSpecifications().add(
						createTransactionsPTDGSpecification(EntityType.DEPOSIT_TRANSACTION, 1, productCode, transactionCode, txnChannel, "", "", 
								Double.valueOf(df.format(txnValue)), merchantRegionCode));
			}
			//Fees
			for (FeeMetadata feeMetadata : feeMetatadaList) {
				int numberOfFees = ThreadLocalRandom.current().nextInt(feeMetadata.getMinValue(), feeMetadata.getMaxValue());
				accountsPTDGSpecification.getFeePTDGSpecifications().add(
						createFeePTDGSpecification(feeMetadata.getFeeCode(), feeMetadata.getFeeName(), feeMetadata.getFeeValue(), 0L, "", numberOfFees));
			}
			customerPTDGSpecification.getAccountsPTDGSpecifications().add(accountsPTDGSpecification);
		}
		
	    
		return customerPTDGSpecification;
	}
	
	private static String getRandomValue(List<String> valueSet) {
		Random rand = new Random();
		return valueSet.get(rand.nextInt(valueSet.size()));
	}
	
	
	
}
