package com.zafin.rpe.test.tools.generator;

public class TransactionsPTDGSpecification {

	private EntityType entityType;
	private int numberOfTransactions;
	private String productCode;
	private String transactionCode;
	private String transactionChannel;
	private String merchantCategoryCode;
	private String merchantCode;
	private String merchantRegionCode;
	private String merchantStateCode;
	private double transactionValue;
	
	public int getNumberOfTransactions() {
		return numberOfTransactions;
	}
	public void setNumberOfTransactions(int numberOfTransactions) {
		this.numberOfTransactions = numberOfTransactions;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getTransactionCode() {
		return transactionCode;
	}
	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}
	public String getTransactionChannel() {
		return transactionChannel;
	}
	public void setTransactionChannel(String transactionChannel) {
		this.transactionChannel = transactionChannel;
	}
	public String getMerchantCategoryCode() {
		return merchantCategoryCode;
	}
	public void setMerchantCategoryCode(String merchantCategoryCode) {
		this.merchantCategoryCode = merchantCategoryCode;
	}
	public String getMerchantCode() {
		return merchantCode;
	}
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}
	public double getTransactionValue() {
		return transactionValue;
	}
	public void setTransactionValue(double transactionValue) {
		this.transactionValue = transactionValue;
	}
	public EntityType getEntityType() {
		return entityType;
	}
	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}
	public String getMerchantRegionCode() {
		return merchantRegionCode;
	}
	public void setMerchantRegionCode(String merchantRegionCode) {
		this.merchantRegionCode = merchantRegionCode;
	}

	public String getMerchantStateCode() {
		return merchantStateCode;
	}

	public void setMerchantStateCode(String merchantStateCode) {
		this.merchantStateCode = merchantStateCode;
	}

}
