package com.zafin.rpe.test.tools.generator.input.model;

import com.zafin.rpe.test.tools.generator.EntityType;

public class AccountProfile {

	private String profileCode;
	private String productCode;
	private EntityType entityType;
	private Boolean accountPayroll = new Boolean(true);
	private String accountCohortCode;
	private DateRange accountOpenDate;
	private int transactionCount;
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public EntityType getEntityType() {
		return entityType;
	}
	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}
	public Boolean getAccountPayroll() {
		return accountPayroll;
	}
	public void setAccountPayroll(Boolean accountPayroll) {
		this.accountPayroll = accountPayroll;
	}
	public String getAccountCohortCode() {
		return accountCohortCode;
	}
	public void setAccountCohortCode(String accountCohortCode) {
		this.accountCohortCode = accountCohortCode;
	}
	public DateRange getAccountOpenDate() {
		return accountOpenDate;
	}
	public void setAccountOpenDate(DateRange accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}
	public int getTransactionCount() {
		return transactionCount;
	}
	public void setTransactionCount(int transactionCount) {
		this.transactionCount = transactionCount;
	}
	public String getProfileCode() {
		return profileCode;
	}
	public void setProfileCode(String profileCode) {
		this.profileCode = profileCode;
	}
	
	
}
