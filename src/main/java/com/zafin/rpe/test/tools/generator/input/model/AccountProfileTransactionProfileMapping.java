package com.zafin.rpe.test.tools.generator.input.model;

/**
 * One account profile can have zero or more transaction profiles. For each account created with this account profile and for each
 * transaction profile, transactions are generated based on the weighted number of transactions configuration
 * 
 * E.g. The weighted configuration is: 1(50%),2(25%),5(25%), the system will do the following:
 * 50% of the accounts will get 1 transaction, 25% of the accounts will get 2 transactions & 25% of the accounts will get 5 transactions
 * 
 */
public class AccountProfileTransactionProfileMapping {

	private String accountProfileCode;
	private String transactionProfileCode;
	private WeightedCounts numberOfTransactions;
	
	public String getAccountProfileCode() {
		return accountProfileCode;
	}
	public void setAccountProfileCode(String accountProfileCode) {
		this.accountProfileCode = accountProfileCode;
	}
	public String getTransactionProfileCode() {
		return transactionProfileCode;
	}
	public void setTransactionProfileCode(String transactionProfileCode) {
		this.transactionProfileCode = transactionProfileCode;
	}
	public WeightedCounts getNumberOfTransactions() {
		return numberOfTransactions;
	}
	public void setNumberOfTransactions(WeightedCounts numberOfTransactions) {
		this.numberOfTransactions = numberOfTransactions;
	}
	
}
