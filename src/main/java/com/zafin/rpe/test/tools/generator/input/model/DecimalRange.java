package com.zafin.rpe.test.tools.generator.input.model;

public class DecimalRange {

	private double rangeFrom;
	private double rangeTo;
	
	public double getRangeFrom() {
		return rangeFrom;
	}
	public void setRangeFrom(double rangeFrom) {
		this.rangeFrom = rangeFrom;
	}
	public double getRangeTo() {
		return rangeTo;
	}
	public void setRangeTo(double rangeTo) {
		this.rangeTo = rangeTo;
	}
	
	
}
