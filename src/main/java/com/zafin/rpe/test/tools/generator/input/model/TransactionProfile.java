package com.zafin.rpe.test.tools.generator.input.model;

import java.util.List;

/**
 * For each transaction that is generated using this profile, the system will generate any list values in a round robin mode.
 * For transaction values, the system will generate a random value between the range inclusive of the range values.  
 *
 */
public class TransactionProfile {

	private String profileCode;
	private String productCode;
	private List<String> transactionCodes;
	private List<String> transactionChannels;
	private List<String> merchantCategoryCodes;
	private List<String> merchantCodes;
	private DecimalRange transactionValues;
	
	public String getProfileCode() {
		return profileCode;
	}
	public void setProfileCode(String profileCode) {
		this.profileCode = profileCode;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public List<String> getTransactionCodes() {
		return transactionCodes;
	}
	public void setTransactionCodes(List<String> transactionCodes) {
		this.transactionCodes = transactionCodes;
	}
	public List<String> getTransactionChannels() {
		return transactionChannels;
	}
	public void setTransactionChannels(List<String> transactionChannels) {
		this.transactionChannels = transactionChannels;
	}
	public List<String> getMerchantCategoryCodes() {
		return merchantCategoryCodes;
	}
	public void setMerchantCategoryCodes(List<String> merchantCategoryCodes) {
		this.merchantCategoryCodes = merchantCategoryCodes;
	}
	public List<String> getMerchantCodes() {
		return merchantCodes;
	}
	public void setMerchantCodes(List<String> merchantCodes) {
		this.merchantCodes = merchantCodes;
	}
	public DecimalRange getTransactionValues() {
		return transactionValues;
	}
	public void setTransactionValues(DecimalRange transactionValues) {
		this.transactionValues = transactionValues;
	}
	
	
	
}

