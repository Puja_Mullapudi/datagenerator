package com.zafin.rpe.test.tools.generator.input.model;

import java.util.ArrayList;
import java.util.List;

public class WeightedCounts {

	List<WeightedCount> weightedCounts = new ArrayList<>();

	public List<WeightedCount> getWeightedCounts() {
		return weightedCounts;
	}
	
	public void addWeightedCount(int count, double percentage) {
		weightedCounts.add(new WeightedCount(count,percentage));
	}
}

class WeightedCount {
	private int count;
	private double percentage;
	
	WeightedCount(int count, double percentage) {
		this.count = count;
		this.percentage = percentage;
	}
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public double getPercentage() {
		return percentage;
	}
	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}
}